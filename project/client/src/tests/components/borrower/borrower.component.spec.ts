import { PaymentDTO } from './../../../app/common/models/payment-models/payment.dto';
import { BaseInvestmentDTO } from './../../../app/common/models/investment-models/base-investment.dto';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { BorrowerComponent } from '../../../app/features/borrower/borrower-component/borrower.component';
import { of } from 'rxjs';
import { SharedModule } from '../../../app/shared/shared.module';
import { LoanViewComponent } from '../../../app/features/borrower/views/loan-view/loan-view.component';
import { DebtViewComponent } from '../../../app/features/borrower/views/debt-view/debt-view.component';
import { InvesmentViewComponent } from '../../../app/features/borrower/views/invesment-view/invesment-view.component';
import { AuthService } from '../../../app/core/services/auth.service';
import { NotificatorService } from '../../../app/core/services/notificator.service';
import { PaymentsService } from '../../../app/core/services/payments.service';
import { LoansService } from '../../../app/core/services/loans.service';
import { InvestmentsService } from '../../../app/core/services/investments.service';
import { ActivatedRoute } from '@angular/router';
import { DebtOnUserDTO } from '../../../app/common/models/debt-models/debt-on-user.dto';
import { BaseLoanDTO } from '../../../app/common/models/loan-models/base-loan.dto';
import { BaseUserDTO } from '../../../app/common/models/user-models/base-user.dto';
import { UserDTO } from '../../../app/common/models/user-models/user.dto';
import { PaymentToPaidDTO } from '../../../app/common/models/payment-models/payment-to-paid.dto';
import { CreatePaymentDTO } from '../../../app/common/models/payment-models/create-payment.dto';
import { LoanDTO } from '../../../app/common/models/loan-models/loan.dto';
import { StatusLoan } from '../../../app/common/enums/status-loan.enum';
import { PaymentsModalComponent } from '../../../app/features/borrower/modals/payments-modal/payments-modal.component';
import { DebtRequestModalComponent } from '../../../app/features/borrower/modals/debt-request-modal/debt-request-modal.component';
import { DebtInfoModalComponent } from '../../../app/features/borrower/modals/debt-info-modal/debt-info-modal.component';
// tslint:disable-next-line: max-line-length
import { UpdateDebtRequestModalComponent } from '../../../app/features/borrower/modals/update-debt-request-modal/update-debt-request-modal.component';
import { AgreeModalComponent } from '../../../app/shared/components/agree-modal/agree-modal.component';
import { MatDialog } from '@angular/material';
import { DebtsService } from '../../../app/core/services/debts.service';
import { BaseDebtDTO } from '../../../app/common/models/debt-models/base-debt.dto';
import { StatusInvestment } from '../../../app/common/enums/status-investment.enum';
import { InvestmentDTO } from '../../../app/common/models/investment-models/investment.dto';
import { UtilsService } from '../../../app/core/services/utils.service';

describe('BorrowerComponent', () => {
    let dialog;
    let debtsService;
    let notificator;
    let authService;
    let paymentsService;
    let route;
    let loansService;
    let investmentsService;
    let utils;

    let fixture: ComponentFixture<BorrowerComponent>;
    let component: BorrowerComponent;

    beforeEach(async(() => {
        jest.clearAllMocks();

        dialog = {
            open() { },
        };

        debtsService = {
            createDebt() { },
            updateDebt() { },
            deleteDebt() { },
        };

        notificator = {
            success() { },
            warn() { },
        };

        authService = {
            loggedUser$: of(),
            loggedUserInfo$: of(),
        };

        paymentsService = {
            createPayments() { },
        };

        route = {
            data: of({
                debts: [],
                loans: [],
            }),
        };

        loansService = {
            createLoans() { },
        };

        investmentsService = {
            updateInvestmentStatus() { },
        };

        utils = {
            calculateRemainingAmount() { },
        };

        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                BrowserAnimationsModule,
                SharedModule,
            ],
            declarations: [
                BorrowerComponent,
                LoanViewComponent,
                DebtViewComponent,
                InvesmentViewComponent,

                DebtRequestModalComponent,
                DebtInfoModalComponent,
                UpdateDebtRequestModalComponent,
                PaymentsModalComponent,
            ],
            providers: [
                DebtsService,
                NotificatorService,
                AuthService,
                PaymentsService,
                LoansService,
                InvestmentsService,
                UtilsService,
            ],

        })
            .overrideProvider(MatDialog, { useValue: dialog })
            .overrideProvider(DebtsService, { useValue: debtsService })
            .overrideProvider(NotificatorService, { useValue: notificator })
            .overrideProvider(AuthService, { useValue: authService })
            .overrideProvider(ActivatedRoute, { useValue: route })
            .overrideProvider(PaymentsService, { useValue: paymentsService })
            .overrideProvider(LoansService, { useValue: loansService })
            .overrideProvider(InvestmentsService, { useValue: investmentsService })
            .overrideProvider(UtilsService, { useValue: utils })
            .compileComponents();

        fixture = TestBed.createComponent(BorrowerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should be defined', () => {
        expect(component).toBeDefined();
    });

    describe('ngOnInit should', () => {

        it('initialize correctly with the data passed from the observable', done => {
            // Arrange
            const user = new UserDTO();
            user.id = 'testUser';

            authService.loggedUser$ = of(user);

            // Act
            component.ngOnInit();

            // Assert
            expect(component.userId).toBe(user.id);

            done();
        });

        it('initialize correctly with the data passed from the resolver', () => {
            // Arrange
            const debt = new DebtOnUserDTO();
            debt.investments = [];

            const loan = new BaseLoanDTO();
            loan.payments = [];
            loan.investment = new BaseInvestmentDTO();
            loan.investment.user = new BaseUserDTO();

            const debts = [debt];
            const loans = [loan];
            route.data = of({ debts, loans });

            // Act
            component.ngOnInit();

            // Assert
            expect(component.debts).toBe(debts);
            expect(component.loans).toBe(loans);
        });
    });

    describe('ngOnDestroy should', () => {
        it('stop receving data from authService.loggedUser$ observable', () => {
            // Arrange
            const user = new UserDTO();

            authService.loggedUser$ = of(user);

            // Act
            component.ngOnInit();
            component.ngOnDestroy();

            // Assert
            expect((component as any).userSubscription.closed).toBe(true);
        });

        it('stop receving data from debtDataDialog.componentInstance.investmentIdForAccept observable', () => {
            // Arrange
            const debt = new DebtOnUserDTO();
            debt.id = '5';

            const investment = new BaseInvestmentDTO();
            investment.status = StatusInvestment.Pending;
            investment.id = 'investmentId';
            investment.user = new BaseUserDTO();
            investment.user.balance = 2;
            investment.amount = 1;

            debt.investments = [investment];

            jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    componentInstance: {
                        investmentIdForAccept: of('investmentId'),
                        investmentIdForReject: of('investmentId'),
                    }
                }));

            (component.debtViews as any) = [{ debt: { id: '5' } }];

            jest.spyOn(loansService, 'createLoans')
                .mockImplementation(() => of(new LoanDTO()));

            jest.spyOn(investmentsService, 'updateInvestmentStatus')
                .mockImplementation(() => of(new InvestmentDTO()));

            // Act
            component.openDebtDataModal(debt);
            component.ngOnDestroy();

            // Assert
            expect((component as any).investmentIdForAccept.closed).toBe(true);
        });

        it('stop receving data from debtDataDialog.componentInstance.investmentIdForReject observable', () => {
            // Arrange
            const debt = new DebtOnUserDTO();
            debt.id = '5';

            const investment = new BaseInvestmentDTO();
            investment.status = StatusInvestment.Pending;
            investment.id = 'investmentId';
            investment.user = new BaseUserDTO();
            investment.user.balance = 2;
            investment.amount = 1;

            debt.investments = [investment];

            jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    componentInstance: {
                        investmentIdForAccept: of('investmentId'),
                        investmentIdForReject: of('investmentId'),
                    }
                }));

            (component.debtViews as any) = [{ debt: { id: '5' } }];

            jest.spyOn(loansService, 'createLoans')
                .mockImplementation(() => of(new LoanDTO()));

            jest.spyOn(investmentsService, 'updateInvestmentStatus')
                .mockImplementation(() => of(new InvestmentDTO()));

            // Act
            component.openDebtDataModal(debt);
            component.ngOnDestroy();

            // Assert
            expect((component as any).investmentIdForReject.closed).toBe(true);
        });
    });

    describe('makePaymentOnLoan should', () => {
        it('call paymentsService.createPayments() once with correct parameters', () => {
            // Arrange
            const paymentData = new PaymentToPaidDTO();
            paymentData.loanId = 'testLoanId';
            paymentData.newPayment = new CreatePaymentDTO();

            const loan = new LoanDTO();

            component.userId = 'testUserId';

            jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    afterClosed() { return of(true); }
                }));

            const spy = jest.spyOn(paymentsService, 'createPayments')
                .mockImplementation(() => of(loan));

            // Act
            component.makePaymentOnLoan(paymentData);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith('testUserId', 'testLoanId', paymentData.newPayment);
        });

        it('remove loan from array if its completed', done => {
            // Arrange
            const paymentData = new PaymentToPaidDTO();
            const loan = new LoanDTO();
            loan.id = '1';
            loan.status = StatusLoan.Completed;

            jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    afterClosed() { return of(true); }
                }));

            jest.spyOn(paymentsService, 'createPayments')
                .mockImplementation(() => of(loan));

            component.loans = [loan];
            // Act
            component.makePaymentOnLoan(paymentData);

            // Assert
            expect(component.loans).toEqual([]);

            done();
        });

        it('call notificator.success() if loan status is completed', done => {
            // Arrange
            const paymentData = new PaymentToPaidDTO();
            const loan = new LoanDTO();
            loan.status = StatusLoan.Completed;

            jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    afterClosed() { return of(true); }
                }));

            jest.spyOn(paymentsService, 'createPayments')
                .mockImplementation(() => of(loan));

            const spy = jest.spyOn(notificator, 'success');

            // Act
            component.makePaymentOnLoan(paymentData);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(`Your loan has been successfully paid!`);

            done();
        });

        it('correctly find loan in array if its not completed', done => {
            // Arrange
            const paymentData = new PaymentToPaidDTO();
            paymentData.loanId = '5';

            const loan = new LoanDTO();
            loan.id = '5';

            component.loans = [loan];

            jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    afterClosed() { return of(true); }
                }));

            jest.spyOn(paymentsService, 'createPayments')
                .mockImplementation(() => of(loan));

            // Act
            component.makePaymentOnLoan(paymentData);

            // Assert
            expect(component.loans[0]).toEqual(loan);
            // Ask about this test !
            done();
        });

        it('correctly change loan from array with loan from response if its not completed', done => {
            // Arrange
            const paymentData = new PaymentToPaidDTO();
            paymentData.loanId = '5';

            const oldLoan = new LoanDTO();
            oldLoan.id = '5';

            const newLoan = new LoanDTO();

            component.loans = [oldLoan];

            jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    afterClosed() { return of(true); }
                }));

            jest.spyOn(paymentsService, 'createPayments')
                .mockImplementation(() => of(newLoan));

            // Act
            component.makePaymentOnLoan(paymentData);

            // Assert
            expect(component.loans).toEqual([newLoan]);

            done();
        });
    });

    describe('openCurrentLoanPayments should', () => {
        it('correctly check if loan have payments to open PaymentsModalComponent', () => {
            // Arrange
            const loan = new BaseLoanDTO();
            loan.payments = [new PaymentDTO()];

            const modalConfigs = {
                maxHeight: '90vh',
                maxWidth: '95vh',
                data: loan.payments,
            };

            const spy = jest.spyOn(dialog, 'open');

            // Act
            component.openCurrentLoanPayments(loan);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(PaymentsModalComponent, modalConfigs);
        });

        it('correctly check if loan don\'t have payments and call notificator.warn() once with correct parameters', () => {
            // Arrange
            const loan = new BaseLoanDTO();
            loan.payments = [];

            const spy = jest.spyOn(notificator, 'warn');
            // Act
            component.openCurrentLoanPayments(loan);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(`Still no payments yet!`);
        });
    });

    describe('openDebtDataModal should', () => {
        it('call notificator.warn() if debt didn\'t have pending invesments ', () => {
            // Arrange
            const debt = new DebtOnUserDTO();
            debt.id = '5';

            const investment = new BaseInvestmentDTO();
            investment.status = StatusInvestment.Pending;
            investment.id = 'investmentId';
            investment.user = new BaseUserDTO();
            investment.user.balance = 2;
            investment.amount = 3;

            debt.investments = [investment];

            const spy = jest.spyOn(notificator, 'warn');

            // Act
            component.openDebtDataModal(debt);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(`Still no invesments yet!`);
        });

        it('open DebtInfoModalComponent correctly', done => {
            // Arrange
            const debt = new DebtOnUserDTO();
            debt.id = '5';

            const investment = new BaseInvestmentDTO();
            investment.status = StatusInvestment.Pending;
            investment.id = 'investmentId';
            investment.user = new BaseUserDTO();
            investment.user.balance = 2;
            investment.amount = 2;

            debt.investments = [investment];

            const spy = jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    componentInstance: {
                        investmentIdForAccept: of('investmentId'),
                        investmentIdForReject: of('investmentId'),
                    }
                }));

            (component.debtViews as any) = [{ debt: { id: '5' } }];

            jest.spyOn(loansService, 'createLoans')
                .mockImplementation(() => of(new LoanDTO()));

            jest.spyOn(investmentsService, 'updateInvestmentStatus')
                .mockImplementation(() => of(new InvestmentDTO()));

            const modalConfigs = {
                maxHeight: '90vh',
                maxWidth: '95vh',
                data: debt,
            };

            // Act
            component.openDebtDataModal(debt);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(DebtInfoModalComponent, modalConfigs);

            done();
        });
    });

    describe('openDebtUpdateModal should', () => {
        it('open UpdateDebtRequestModalComponent correctly', done => {
            // Arrange
            const debt = new DebtOnUserDTO();

            const modalConfigs = {
                maxHeight: '90vh',
                maxWidth: '95vh',
                data: debt,
            };

            const spy = jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    afterClosed() { return of(undefined); }
                }));

            // Act
            component.openDebtUpdateModal(debt);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(UpdateDebtRequestModalComponent, modalConfigs);

            done();
        });

        it('not call debtsService.updateDebt() after dialog is closed and return undefined', done => {
            // Arrange
            const debt = new DebtOnUserDTO();

            jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    afterClosed() { return of(undefined); }
                }));

            const spy = jest.spyOn(debtsService, 'updateDebt')
                .mockImplementation(() => of(new BaseDebtDTO()));

            // Act
            component.openDebtUpdateModal(debt);

            // Assert
            expect(spy).toBeCalledTimes(0);

            done();
        });

        it('call debtsService.updateDebt() once with correct parameters after dialog is closed and return correct value', done => {
            // Arrange
            const debt = new DebtOnUserDTO();
            debt.id = 'debtId';
            debt.investments = [];

            component.userId = 'userId';

            jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    afterClosed() { return of({ value: { amount: 1000 } }); }
                }));

            const spy = jest.spyOn(debtsService, 'updateDebt')
                .mockImplementation(() => of(debt));

            (component.debtViews as any) = [{ debt: { id: 'debtId', investments: [] }, remainingAmount: 50 }];

            // Act
            component.openDebtUpdateModal(debt);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith('userId', { amount: 1000 }, 'debtId');

            done();
        });

        it('correctly change debt.amount in children component', done => {
            // Arrange
            const debt = new DebtOnUserDTO();
            debt.id = 'debtId';
            debt.investments = [];
            debt.amount = 30;

            jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    afterClosed() { return of({ value: { amount: 1000 } }); }
                }));

            jest.spyOn(debtsService, 'updateDebt')
                .mockImplementation(() => of(debt));

            (component.debtViews as any) = [{ debt: { id: 'debtId', amount: 0, investments: [] }, remainingAmount: 50 }];

            // Act
            component.openDebtUpdateModal(debt);

            // Assert
            expect(component.debtViews[0].debt.amount).toBe(30);

            done();
        });

        it('correctly change remainingAmount in children component', done => {
            // Arrange
            const debt = new DebtOnUserDTO();
            debt.id = 'debtId';
            debt.investments = [];

            jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    afterClosed() { return of({ value: { amount: 1000 } }); }
                }));

            jest.spyOn(debtsService, 'updateDebt')
                .mockImplementation(() => of(debt));

            (component.debtViews as any) = [{ debt: { id: 'debtId', investments: [] }, remainingAmount: 50 }];

            jest.spyOn(utils, 'calculateRemainingAmount')
                .mockImplementation(() => 50);
            // Act
            component.openDebtUpdateModal(debt);

            // Assert
            expect(component.debtViews[0].remainingAmount).toBe(50);

            done();
        });

        it('correctly update amount on debt with new amount from response', done => {
            // Arrange
            const debt = new DebtOnUserDTO();
            debt.investments = [];
            debt.amount = 0;

            const updatedDebt = new BaseDebtDTO();
            updatedDebt.amount = 50;
            updatedDebt.id = 'updatedDebtId';

            jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    afterClosed() { return of({ value: { amount: 1000 } }); }
                }));

            jest.spyOn(debtsService, 'updateDebt')
                .mockImplementation(() => of(updatedDebt));

            (component.debtViews as any) = [{ debt: { id: 'updatedDebtId', investments: [], remainingAmount: 50 }, }];

            // Act
            component.openDebtUpdateModal(debt);

            // Assert
            expect(debt.amount).toBe(50);

            done();
        });
    });

    describe('openDebtCancelModal should', () => {
        it('open AgreeModalComponent correctly', done => {
            // Arrange
            const debt = new DebtOnUserDTO();

            const modalConfigs = {
                maxHeight: '90vh',
                maxWidth: '95vh',
                data: 'cancel this loan request',
            };

            const spy = jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    afterClosed() { return of('isAgree'); }
                }));

            jest.spyOn(debtsService, 'deleteDebt')
                .mockImplementation(() => of(new BaseDebtDTO()));

            // Act
            component.openDebtCancelModal(debt);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(AgreeModalComponent, modalConfigs);

            done();
        });

        it('not call debtsService.createDebt() after dialog closed return undefined', done => {
            // Arrange
            const debt = new DebtOnUserDTO();

            jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    afterClosed() { return of(undefined); }
                }));

            const spy = jest.spyOn(debtsService, 'deleteDebt')
                .mockImplementation(() => of(new BaseDebtDTO()));

            // Act
            component.openDebtCancelModal(debt);

            // Assert
            expect(spy).toBeCalledTimes(0);

            done();
        });

        it('call debtsService.createDebt() once with correct parameters after dialog closed with correct value', done => {
            // Arrange
            const debt = new DebtOnUserDTO();

            jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    afterClosed() { return of('isAgree'); }
                }));

            const spy = jest.spyOn(debtsService, 'deleteDebt')
                .mockImplementation(() => of(new BaseDebtDTO()));

            // Act
            component.openDebtCancelModal(debt);

            // Assert
            expect(spy).toBeCalledTimes(1);

            done();
        });

        it('filter debts array without debtForDelete from debtsService.deleteDebt() response', done => {
            // Arrange
            const debt = new DebtOnUserDTO();
            const debtForDelete = new BaseDebtDTO();
            debtForDelete.id = 'test';

            jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    afterClosed() { return of('isAgree'); }
                }));

            jest.spyOn(debtsService, 'deleteDebt')
                .mockImplementation(() => of(debtForDelete));

            component.debts = [(debtForDelete as any)];

            // Act
            component.openDebtCancelModal(debt);

            // Assert
            expect(component.debts).toEqual([]);

            done();
        });
    });

    describe('openDebtRequest should', () => {
        it('open DebtRequestModalComponent correctly', () => {
            // Arrange
            const debt = new DebtOnUserDTO();

            const modalConfigs = {
                maxHeight: '90vh',
                maxWidth: '95vh',
            };

            const spy = jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    afterClosed() { return of('test'); }
                }));

            jest.spyOn(debtsService, 'createDebt').mockImplementation(() => of(debt));

            // Act
            component.openDebtRequest();

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(DebtRequestModalComponent, modalConfigs);
        });

        it('call debtsService.createDebt() once with correct parameters after dialog closed to', done => {
            // Arrange
            const debt = new DebtOnUserDTO();
            component.userId = 'testUserId';

            jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    afterClosed() { return of({ value: 'mockedValue' }); }
                }));

            const spy = jest.spyOn(debtsService, 'createDebt').mockImplementation(() => of(debt));

            // Act
            component.openDebtRequest();

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith('testUserId', 'mockedValue');

            done();
        });

        it('notificator.success() after debtsService.createDebt() with successful respons once with correct parameters', done => {
            // Arrange
            const debt = new DebtOnUserDTO();
            component.userId = 'testUserId';

            jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    afterClosed() { return of({ value: 'mockedValue' }); }
                }));

            jest.spyOn(debtsService, 'createDebt').mockImplementation(() => of(debt));

            const spy = jest.spyOn(notificator, 'success');

            // Act
            component.openDebtRequest();

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(`Your debt request is created successful!`);

            done();
        });

        it('add createdDebt in debts array from response', done => {
            // Arrange
            const debt = new DebtOnUserDTO();
            component.userId = 'testUserId';
            component.debts = [];

            jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    afterClosed() { return of({ value: 'mockedValue' }); }
                }));

            jest.spyOn(debtsService, 'createDebt').mockImplementation(() => of(debt));

            // Act
            component.openDebtRequest();

            // Assert
            expect(component.debts).toEqual([debt]);

            done();
        });

        it('not call debtsService.createDebt() if DebtRequestModalComponent is closed with falsy value', done => {
            // Arrange
            const debt = new DebtOnUserDTO();
            component.userId = 'testUserId';

            jest.spyOn(dialog, 'open')
                .mockImplementation(() => ({
                    afterClosed() { return of(undefined); }
                }));

            const spy = jest.spyOn(debtsService, 'createDebt').mockImplementation(() => of(debt));

            // Act
            component.openDebtRequest();

            // Assert
            expect(spy).toBeCalledTimes(0);

            done();
        });
    });
});
