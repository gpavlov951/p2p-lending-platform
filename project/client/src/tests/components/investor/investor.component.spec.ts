import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { InvestorComponent } from '../../../app/features/investor/investor-component/investor.component';
import { AuthService } from '../../../app/core/services/auth.service';
import { NotificatorService } from '../../../app/core/services/notificator.service';
import { InvestmentsService } from '../../../app/core/services/investments.service';
import { SharedModule } from '../../../app/shared/shared.module';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { SocketService } from '../../../app/core/services/socket.service';
import { MatDialog } from '@angular/material';
import { DeptViewComponent } from '../../../app/features/investor/views/dept-view/dept-view.component';
import { InvestPendingViewComponent } from '../../../app/features/investor/views/invest-pending-view/invest-pending-view.component';
import { InvestorDetailsViewComponent } from '../../../app/features/investor/views/investor-details-view/investor-details-view.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoggedUserInfoDTO } from '../../../app/common/models/user-models/logged-user-info.dto';
import { InvestmentDTO } from '../../../app/common/models/investment-models/investment.dto';
import { AllDebtDTO } from '../../../app/common/models/debt-models/all-debt-data.dto';
import { StatusInvestment } from '../../../app/common/enums/status-investment.enum';
import { StatusLoan } from '../../../app/common/enums/status-loan.enum';
import { BaseInvestmentWithoutUserDTO } from '../../../app/common/models/investment-models/base-investment-without-user.dto';
import { LoanDTO } from '../../../app/common/models/loan-models/loan.dto';
import { DebtDTO } from '../../../app/common/models/debt-models/debt.dto';
import { BaseUserDTO } from '../../../app/common/models/user-models/base-user.dto';
import { DebtViewModalComponent } from '../../../app/features/investor/modals/debt-modal/debt-modal.component';
import { UtilsService } from '../../../app/core/services/utils.service';

describe('InvestorComponent', () => {
    let dialog;
    let investmentService;
    let authService;
    let notificator;
    let route;
    let socketService;
    let utils;

    let fixture: ComponentFixture<InvestorComponent>;
    let component: InvestorComponent;

    beforeEach(async(() => {

        jest.clearAllMocks();

        route = {
            data: of({
                userInvestments: [],
                allDebts: []
            })
        };
        authService = { loggedUserInfo$: of() };
        socketService = {
            newDebt$: of(),
            acceptedInvest$: of(),
        };

        investmentService = {
            createInvestment() { },
            updateInvestmentStatus() { }
        };

        utils = {
            calculateLeftAmountToPay() { },
            calculateNextDueDate() { },
            calculateOverDueDays() { },
            calculatePenaltyAmount() { },
            calculateRemainingAmount() { },
        };

        notificator = {
            success() { },
            warn() { },
        };

        dialog = {
            open() { },
        };

        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                SharedModule,
                BrowserAnimationsModule
            ],
            declarations: [
                InvestorComponent,
                DeptViewComponent,
                InvestPendingViewComponent,
                InvestorDetailsViewComponent,
                DebtViewModalComponent,
            ],
            providers: [
                AuthService,
                NotificatorService,
                InvestmentsService,
                SocketService
            ]
        })
            .overrideProvider(MatDialog, { useValue: dialog })
            .overrideProvider(NotificatorService, { useValue: notificator })
            .overrideProvider(AuthService, { useValue: authService })
            .overrideProvider(ActivatedRoute, { useValue: route })
            .overrideProvider(InvestmentsService, { useValue: investmentService })
            .overrideProvider(SocketService, { useValue: socketService })
            .overrideProvider(UtilsService, { useValue: utils })
            .compileComponents();

        fixture = TestBed.createComponent(InvestorComponent);
        component = fixture.componentInstance;

    }));

    it('should be defined', () => {
        expect(component).toBeDefined();
    });

    describe('ngOnInit should', () => {

        it(`should call authService.loggedUserInfo$ once with correct parameters and return correct values`, done => {
            // Arrange
            const mockedUser = new LoggedUserInfoDTO();
            authService.loggedUserInfo$ = of(mockedUser);

            // Act
            component.ngOnInit();

            // Assert
            expect(component.investorBalance).toEqual(mockedUser.balance);
            expect(component.userId).toEqual(mockedUser.id);

            done();
        });

        it(`initialize investments correctly with the data passed from the resolver`, () => {
            // Arrange
            const investment = new InvestmentDTO();

            investment.status = StatusInvestment.Accepted;
            investment.loans = [];
            investment.loans.push(new LoanDTO());
            investment.loans[0].status = StatusLoan.InProcess;

            const userInvestments = [{ status: StatusInvestment.Accepted, loans: [{ status: StatusLoan.InProcess }] }];
            const allDebts = [];
            route.data = of({ userInvestments, allDebts });

            // Act
            component.ngOnInit();

            // Assert
            expect(component.investments).toEqual([investment]);
        });

        it(`initialize investments pending correctly with the data passed from the resolver`, () => {
            // Arrange
            const investment = new InvestmentDTO();
            investment.status = StatusInvestment.Pending;

            const userInvestments = [investment];
            const allDebts = [];
            route.data = of({ userInvestments, allDebts });

            // Act
            component.ngOnInit();

            // Arrange
            expect(component.investPending).toEqual([investment]);
        });

        it(`initialize debt correctly with the data passed from the resolver`, () => {
            // Arrange
            const debt = new AllDebtDTO();
            debt.user = new BaseUserDTO();
            debt.user.id = '1';

            component.userId = '5';

            const userInvestments = [];
            const allDebts = [debt];

            route.data = of({ userInvestments, allDebts });

            // Act
            component.ngOnInit();

            // Assert
            expect(component.debts).toContain(debt);
        });
    });
    describe('ngOnDestroy should', () => {

        it('stop receiving data from socketSubscription', () => {
            // Arrange & Act
            component.ngOnInit();
            component.ngOnDestroy();

            // Assert
            expect((component as any).loggedUserInfoSummarySubscription.closed).toBe(true);
        });
    });

    describe('create invest should work correctly', () => {

        it('should cannot invest if investor have not enough money', () => {
            // Arrange
            component.investorBalance = 1;
            const debt = new DebtDTO();
            debt.amount = 5;
            debt.isPartial = false;

            const spy = jest.spyOn(notificator, 'warn');

            // Act
            component.createInvest(debt);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(`You don't have enough money for invest`);
        });

        it('should open correctly dialog with debt and investor balance', () => {
            // Arrange
            const debt = new DebtDTO();
            const balance = component.investorBalance;


            const modalConfigs = {
                width: '300px',
                data: {
                    debt,
                    balance
                }
            };

            const spy = jest.spyOn(dialog, 'open').mockImplementation(() => ({
                afterClosed() { return of(undefined); }
            }));

            // Act
            component.createInvest(debt);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(DebtViewModalComponent, modalConfigs);
        });

        it('should create investment correctly with correct parameters', done => {
            // Arrange
            const debt = new DebtDTO();
            const investment = new InvestmentDTO();
            const investPending = [investment];
            debt.id = '1';
            debt.amount = 1;
            debt.isPartial = false;
            component.investorBalance = 1;

            component.userId = 'testUserId';

            const spy = jest.spyOn(investmentService, 'createInvestment').mockImplementation(() => of(investment));
            const spyAfterClosed = jest.spyOn(dialog, 'open').mockImplementation(() => ({
                afterClosed() { return of({ getRawValue() { return 'data'; }, }); }
            }));

            // Act
            component.createInvest(debt);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith('testUserId', '1', 'data');

            done();
        });

        it('should create investment correctly and push investment to array', done => {
            // Arrange
            const debt = new DebtDTO();
            const investment = new InvestmentDTO();
            const investPending = [investment];
            debt.id = '1';
            debt.amount = 1;
            debt.isPartial = false;
            component.investorBalance = 1;

            component.userId = 'testUserId';

            const spy = jest.spyOn(investmentService, 'createInvestment').mockImplementation(() => of(investment));
            const spyAfterClosed = jest.spyOn(dialog, 'open').mockImplementation(() => ({
                afterClosed() { return of({ getRawValue() { return 'data'; }, }); }
            }));
            const spyNotficator = jest.spyOn(notificator, 'success');

            // Act
            component.createInvest(debt);

            // Assert
            expect(component.investPending).toEqual([investment]);
            expect(spyNotficator).toHaveBeenCalledWith('Your invest request is created successfuly');

            done();
        });

        it('should create investment correctly and notificate user with success message', done => {
            // Arrange
            const debt = new DebtDTO();
            const investment = new InvestmentDTO();
            const investPending = [investment];
            debt.id = '1';
            debt.amount = 1;
            debt.isPartial = false;
            component.investorBalance = 1;

            component.userId = 'testUserId';

            const spy = jest.spyOn(investmentService, 'createInvestment').mockImplementation(() => of(investment));
            const spyAfterClosed = jest.spyOn(dialog, 'open').mockImplementation(() => ({
                afterClosed() { return of({ getRawValue() { return 'data'; }, }); }
            }));
            const spyNotficator = jest.spyOn(notificator, 'success');

            // Act
            component.createInvest(debt);

            // Assert
            expect(spyNotficator).toBeCalledTimes(1);
            expect(spyNotficator).toHaveBeenCalledWith('Your invest request is created successfuly');

            done();
        });
    });

    describe('Cancel investment shout work correctly', () => {

        it('should cancel investment correctly with correct parameters', done => {
            // Arrange
            const investment = new BaseInvestmentWithoutUserDTO();
            investment.id = '1';
            investment.status = StatusInvestment.Rejected;
            const spy = jest.spyOn(investmentService, 'updateInvestmentStatus').mockImplementation(() => of(investment));

            // Act
            component.cancelInvestment(investment);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith('1', { status: 'Rejected' });
            done();
        });

        it('should cancel investment correctly', done => {
            // Arrange
            const investment = new BaseInvestmentWithoutUserDTO();
            investment.id = '1';
            investment.status = StatusInvestment.Rejected;
            const currentInvestment = new BaseInvestmentWithoutUserDTO();
            currentInvestment.id = '2';
            component.investPending = [investment];
            const spy = jest.spyOn(investmentService, 'updateInvestmentStatus').mockImplementation(() => of(currentInvestment));

            // Act
            component.cancelInvestment(investment);

            // Assert
            expect(component.investPending).toEqual([investment]);

            done();
        });
    });
});
