import { NotificatorService } from '../../../app/core/services/notificator.service';
import { TestBed } from '@angular/core/testing';
import { ToastrModule, ToastrService } from 'ngx-toastr';

describe('NotificatorService', () => {

    const toastr = {
        success() { },
        warning() { },
        error() { },
    };

    let service: NotificatorService;

    beforeEach(() => {
        jest.clearAllMocks();

        TestBed.configureTestingModule({
            imports: [ToastrModule],
            providers: [NotificatorService]
        })
            .overrideProvider(ToastrService, { useValue: toastr });

        service = TestBed.get(NotificatorService);
    });

    it('success should call success', () => {

        const spy = jest.spyOn(toastr, 'success').mockImplementation(() => { });

        service.success('test');

        expect(toastr.success).toHaveBeenCalledWith('test');

    });

    it('warn should call warn', () => {

        const spy = jest.spyOn(toastr, 'warning').mockImplementation(() => { });

        service.warn('test');

        expect(toastr.warning).toHaveBeenCalledWith('test');

    });

    it('error should call error', () => {

        const spy = jest.spyOn(toastr, 'error').mockImplementation(() => { });

        service.error('test');

        expect(toastr.error).toHaveBeenCalledWith('test');

    });

});
