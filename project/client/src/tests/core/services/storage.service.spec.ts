import { TestBed } from '@angular/core/testing';
import { StorageService } from '../../../app/core/services/storage.service';

describe('StorageService', () => {

  let service: StorageService;

  beforeEach(() => {
    jest.clearAllMocks();

    TestBed.configureTestingModule({
      providers: [StorageService],
    });

    service = TestBed.get(StorageService);
  });

  it('save should call localStorage.setItem', () => {

    const data = {
      mock: true,
    };

    const spy = jest.spyOn(localStorage, 'setItem').mockImplementation(() => {});

    service.save('test', data);

    expect(localStorage.setItem).toHaveBeenCalledWith('test', String(data));

  });

  it('read should call localStorage.getItem', () => {

    const spy = jest.spyOn(localStorage, 'getItem').mockImplementation(() => '');

    service.read('test');

    expect(localStorage.getItem).toHaveBeenCalledWith('test');

  });

  it('read should return null if there is no data stored', () => {

    const spy = jest.spyOn(localStorage, 'getItem').mockImplementation(() => '');

    const result1 = service.read('test');

    spy.mockImplementation(() => 'undefined');

    const result2 = service.read('test');

    expect(result1).toBe(null);
    expect(result2).toBe(null);

  });

  it('delete should call localStorage.removeItem', () => {

    const spy = jest.spyOn(localStorage, 'removeItem').mockImplementation(() => {});

    service.delete('test');

    expect(localStorage.removeItem).toHaveBeenCalledWith('test');

  });

  it('clear should call localStorage.clear', () => {

    const spy = jest.spyOn(localStorage, 'clear').mockImplementation(() => {});

    service.clear();

    expect(localStorage.clear).toHaveBeenCalledTimes(1);
  });
});
