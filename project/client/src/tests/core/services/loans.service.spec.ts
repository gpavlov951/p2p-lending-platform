import { LoansService } from '../../../app/core/services/loans.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TestBed, async } from '@angular/core/testing';
import { AuthService } from '../../../app/core/services/auth.service';
import { JwtModule } from '@auth0/angular-jwt';
import { CONFIG } from '../../../app/config/config';
import { of } from 'rxjs';
import { LoggedUserInfoDTO } from '../../../app/common/models/user-models/logged-user-info.dto';
import { LoanDTO } from '../../../app/common/models/loan-models/loan.dto';
import { BaseInvestmentDTO } from '../../../app/common/models/investment-models/base-investment.dto';

describe('LoanService', () => {
    let httpClient;
    let authService;

    let service: LoansService;

    beforeEach(async(() => {
        jest.clearAllMocks();

        httpClient = {
            get() { },
            post() { },
        };

        authService = {
            nextLoggedUserInfo$() { },
            loggedUserInfoValue$() { }
        };

        TestBed.configureTestingModule({
            imports: [HttpClientModule,
                JwtModule.forRoot({ config: {} })],
            providers: [
                LoansService,
                AuthService
            ]
        })
            .overrideProvider(HttpClient, { useValue: httpClient })
            .overrideProvider(AuthService, { useValue: authService });

        service = TestBed.get(LoansService);
    }));

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    describe('LoanService', () => {
        it('it should get all loans per user', () => {
            // Arrange
            const userId = '1';
            const url = `${CONFIG.MAIN_URL}/users/${userId}/loans`;
            const returnValue = of('return value');

            const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

            const result = service.getAllLoansPerUser(userId);

            // Act & Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(url);
        });

        it('it should return all loans per user', done => {
            // Arrange
            const userId = '1';
            const url = `${CONFIG.MAIN_URL}/users/${userId}/loans`;
            const returnValue = of('return value');

            const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

            // Act
            const result = service.getAllLoansPerUser(userId);

            // Assert
            result.subscribe(data => {
                expect(data).toEqual('return value');

                done();
            });
        });
    });

    describe('Create loan', () => {
        it('it should call create loan to valid url', () => {
            // Arrange
            const userId = '1';
            const debtId = '1';
            const investmentId = '1';
            const url = `${CONFIG.MAIN_URL}/users/${userId}/debts/${debtId}/investments/${investmentId}/loans`;
            const returnValue = of('return value');

            const spy = jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);

            const result = service.createLoans(userId, debtId, investmentId);

            // Act & Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(url, {});
        });

        it('it shoud call authService.loggedUserInfoValue$() once with correct parameters', done => {
            const userId = '1';
            const debtId = '1';
            const investmentId = '1';
            const mockedLoan = new LoanDTO();
            mockedLoan.investment = new BaseInvestmentDTO();
            const returnValue = of(mockedLoan);
            const mockedUser = new LoggedUserInfoDTO();
            mockedUser.loans = [];

            const spy = jest.spyOn(authService, 'loggedUserInfoValue$').mockImplementation(() => mockedUser);
            jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);


            service.createLoans(userId, debtId, investmentId).subscribe(
                () => {
                    expect(spy).toBeCalledTimes(1);

                    done();
                });
        });

        it('it should push data to array', done => {
            const userId = '1';
            const debtId = '1';
            const investmentId = '1';
            const mockedLoan = new LoanDTO();
            mockedLoan.investment = new BaseInvestmentDTO();
            const returnValue = of(mockedLoan);
            const mockedUser = new LoggedUserInfoDTO();
            mockedUser.loans = [];

            const spy = jest.spyOn(authService, 'loggedUserInfoValue$').mockImplementation(() => mockedUser);
            jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);


            service.createLoans(userId, debtId, investmentId).subscribe(
                () => {
                    expect(mockedUser.loans).toEqual([mockedLoan]);

                    done();
                });
        });

        it('it should change user balance', done => {
            const userId = '1';
            const debtId = '1';
            const investmentId = '1';
            const mockedUser = new LoggedUserInfoDTO();
            const mockedLoan = new LoanDTO();
            mockedLoan.investment = new BaseInvestmentDTO();
            mockedUser.loans = [];
            mockedLoan.investment.amount = 5;
            mockedUser.balance = 5;
            const returnValue = of(mockedLoan);

            const spy = jest.spyOn(authService, 'loggedUserInfoValue$').mockImplementation(() => mockedUser);
            jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);

            service.createLoans(userId, debtId, investmentId).subscribe(
                () => {
                    expect(mockedUser.loans).toEqual([mockedLoan]);
                    expect(mockedUser.balance).toEqual(10);
                    done();
                });
        });

        it('it shoud call authService.nextLoggedUserInfo$() correctly', done => {
            const userId = '1';
            const debtId = '1';
            const investmentId = '1';
            const mockedUser = new LoggedUserInfoDTO();
            const mockedLoan = new LoanDTO();
            mockedLoan.investment = new BaseInvestmentDTO();
            mockedUser.loans = [];
            mockedLoan.investment.amount = 5;
            mockedUser.balance = 5;
            const returnValue = of(mockedLoan);

            const spy = jest.spyOn(authService, 'loggedUserInfoValue$').mockImplementation(() => mockedUser);
            jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);
            const spyOne = jest.spyOn(authService, 'nextLoggedUserInfo$').mockImplementation(() => mockedUser);

            service.createLoans(userId, debtId, investmentId).subscribe(
                () => {
                    expect(spyOne).toBeCalledTimes(1);
                    expect(spyOne).toHaveBeenCalledWith(mockedUser);

                    done();
                });
        });
    });
});
