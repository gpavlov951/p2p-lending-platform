import { PaymentsService } from '../../../app/core/services/payments.service';
import { TestBed, async } from '@angular/core/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { AuthService } from '../../../app/core/services/auth.service';
import { CONFIG } from '../../../app/config/config';
import { of } from 'rxjs';
import { CreatePaymentDTO } from '../../../app/common/models/payment-models/create-payment.dto';
import { LoanDTO } from '../../../app/common/models/loan-models/loan.dto';
import { LoggedUserInfoDTO } from '../../../app/common/models/user-models/logged-user-info.dto';

describe('PaymentsService', () => {
    let http;
    let authService;

    let service: PaymentsService;

    beforeEach(async(() => {
        jest.clearAllMocks();

        http = {
            get() { },
            post() { },
        };

        authService = {
            nextLoggedUserInfo$() { },
            loggedUserInfoValue$() { },
        };

        TestBed.configureTestingModule({
            imports: [
                HttpClientModule,
                JwtModule.forRoot({ config: {} }),
            ],
            providers: [
                AuthService,
                PaymentsService,
                HttpClient,
            ],
        })
            .overrideProvider(HttpClient, { useValue: http })
            .overrideProvider(AuthService, { useValue: authService });

        service = TestBed.get(PaymentsService);
    }));

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    describe('createPayments should', () => {
        it('call http.post() once with correct parameters', () => {
            // Arrange
            const userId = '5';
            const loanId = '5';
            const payment = new CreatePaymentDTO();
            const url = `${CONFIG.MAIN_URL}/users/${userId}/loans/${loanId}/payments`;
            const loan = new LoanDTO();

            const spy = jest.spyOn(http, 'post').mockReturnValue(of(loan));

            // Act
            service.createPayments(userId, loanId, payment);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(url, payment);
        });

        it('http.post() return correct result', done => {
            // Arrange
            const userId = '5';
            const loanId = '5';
            const payment = new CreatePaymentDTO();
            const loan = new LoanDTO();
            const user = new LoggedUserInfoDTO();
            user.loans = [];

            jest.spyOn(http, 'post').mockReturnValue(of(loan));
            jest.spyOn(authService, 'loggedUserInfoValue$').mockReturnValue(user);

            // Act & Assert
            service.createPayments(userId, loanId, payment).subscribe(
                (result) => {
                    expect(result).toEqual(loan);

                    done();
                });
        });

        it('call authService.loggedUserInfoValue() once', done => {
            // Arrange
            const userId = '5';
            const loanId = '5';
            const payment = new CreatePaymentDTO();
            const loan = new LoanDTO();
            const user = new LoggedUserInfoDTO();
            user.loans = [];

            jest.spyOn(http, 'post').mockReturnValue(of(loan));
            const spy = jest.spyOn(authService, 'loggedUserInfoValue$').mockReturnValue(user);

            // Act & Assert
            service.createPayments(userId, loanId, payment).subscribe(
                () => {
                    expect(spy).toBeCalledTimes(1);
                    expect(spy).toReturnWith(user);

                    done();
                });
        });

        it('reduce balance on logged user with newLoan.monthlyInstalmet', done => {
            // Arrange
            const userId = '5';
            const loanId = '5';
            const payment = new CreatePaymentDTO();
            payment.penaltyAmount = 10;
            payment.amount = 40;

            const user = new LoggedUserInfoDTO();
            user.loans = [];
            user.balance = 100;

            const loan = new LoanDTO();
            loan.monthlyInstalmet = 50;

            jest.spyOn(http, 'post').mockReturnValue(of(loan));
            jest.spyOn(authService, 'loggedUserInfoValue$').mockReturnValue(user);

            // Act & Assert
            service.createPayments(userId, loanId, payment).subscribe(
                () => {
                    expect(user.balance).toEqual(50);

                    done();
                });
        });

        it('call authService.nextLoggedUserInfo$() once with correct parameters', done => {
            // Arrange
            const userId = '5';
            const loanId = '5';
            const payment = new CreatePaymentDTO();
            const loan = new LoanDTO();
            const user = new LoggedUserInfoDTO();
            user.loans = [];

            jest.spyOn(http, 'post').mockReturnValue(of(loan));
            jest.spyOn(authService, 'loggedUserInfoValue$').mockReturnValue(user);
            const spy = jest.spyOn(authService, 'nextLoggedUserInfo$');

            // Act & Assert
            service.createPayments(userId, loanId, payment).subscribe(
                () => {
                    expect(spy).toBeCalledTimes(1);
                    expect(spy).toHaveBeenCalledWith(user);

                    done();
                });
        });
    });
});
