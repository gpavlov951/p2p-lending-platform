import { UserProfilePictureDTO } from './../../../app/common/models/user-models/user-profile-picture.dto';
import { BaseUserDTO } from './../../../app/common/models/user-models/base-user.dto';
import { UpdateUserBalanceDTO } from './../../../app/common/models/user-models/update-user-balance.dto';
import { UsersService } from '../../../app/core/services/users.service';
import { async, TestBed } from '@angular/core/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { AuthService } from '../../../app/core/services/auth.service';
import { CreateUserDTO } from '../../../app/common/models/user-models/create-user.dto';
import { CONFIG } from '../../../app/config/config';
import { of } from 'rxjs';
import { UserDTO } from '../../../app/common/models/user-models/user.dto';

describe('UsersService', () => {
    let http;
    let authService;

    let service: UsersService;

    beforeEach(async(() => {
        jest.clearAllMocks();

        http = {
            get() { },
            post() { },
            put() { },
            patch() { },
        };

        authService = {
            nextLoggedUserInfo$() { },
            loggedUserInfoValue$() { },
        };

        TestBed.configureTestingModule({
            imports: [
                HttpClientModule,
                JwtModule.forRoot({ config: {} }),
            ],
            providers: [
                AuthService,
                UsersService,
                HttpClient,
            ],
        })
            .overrideProvider(HttpClient, { useValue: http })
            .overrideProvider(AuthService, { useValue: authService });

        service = TestBed.get(UsersService);
    }));

    it('should be defined', () => {
        expect(service).toBeDefined();
    });


    describe('createUser should', () => {
        it('call http.post() once with correct parameters', () => {
            // Arrange
            const user = new CreateUserDTO();
            const url = `${CONFIG.MAIN_URL}/users`;
            const spy = jest.spyOn(http, 'post');

            // Act
            service.createUser(user);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenLastCalledWith(url, user);
        });

        it('http.post() return correct value', done => {
            // Arrange
            const user = new CreateUserDTO();
            const mockReturn = 'fake';

            jest.spyOn(http, 'post').mockReturnValue(of(mockReturn));

            // Act & Assert
            service.createUser(user).subscribe(
                (result) => {
                    expect(result).toEqual(mockReturn);

                    done();
                });
        });
    });

    describe('updateUserBalance should', () => {
        it('call http.patch() once with correct parameters', () => {
            // Arrange
            const newBalance = new UpdateUserBalanceDTO();
            const userId = '5';
            const mockedUser = new BaseUserDTO();
            const url = `${CONFIG.MAIN_URL}/users/${userId}`;

            const spy = jest.spyOn(http, 'patch').mockReturnValue(of(mockedUser));

            // Act
            service.updateUserBalance(newBalance, userId);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(url, newBalance);
        });

        it('http.patch() return correct value', done => {
            // Arrange
            const newBalance = new UpdateUserBalanceDTO();
            const userId = '5';
            const mockedUser = new BaseUserDTO();

            jest.spyOn(http, 'patch').mockReturnValue(of(mockedUser));

            // Act & Assert
            service.updateUserBalance(newBalance, userId).subscribe(
                (result) => {
                    expect(result).toEqual(mockedUser);

                    done();
                });
        });

        it('call authService.nextLoggedUserInfo$() once with correct parameters', done => {
            // Arrange
            const newBalance = new UpdateUserBalanceDTO();
            const userId = '5';
            const mockedUser = new BaseUserDTO();

            jest.spyOn(http, 'patch').mockReturnValue(of(mockedUser));
            const spy = jest.spyOn(authService, 'nextLoggedUserInfo$');

            // Act & Assert
            service.updateUserBalance(newBalance, userId).subscribe(
                () => {
                    expect(spy).toBeCalledTimes(1);
                    expect(spy).toHaveBeenCalledWith({});

                    done();
                });
        });

        it('call authService.loggedUserInfoValue$() once with correct parameters', done => {
            // Arrange
            const newBalance = new UpdateUserBalanceDTO();
            const userId = '5';
            const mockedUser = new BaseUserDTO();

            jest.spyOn(http, 'patch').mockReturnValue(of(mockedUser));
            const spy = jest.spyOn(authService, 'loggedUserInfoValue$');

            // Act & Assert
            service.updateUserBalance(newBalance, userId).subscribe(
                () => {
                    expect(spy).toBeCalledTimes(1);

                    done();
                });

        });
    });

    describe('getAllUsers should', () => {
        it('call http.get() once with correct parameters', () => {
            // Arrange
            const url = `${CONFIG.MAIN_URL}/users/`;
            const spy = jest.spyOn(http, 'get');

            // Act
            service.getAllUsers();

            // Arrange
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(url);
        });

        it('http.get() return correct value', done => {
            // Arrange
            const mockedUsers = [new UserDTO()];
            jest.spyOn(http, 'get').mockReturnValue(of(mockedUsers));

            // Act & Arrange
            service.getAllUsers().subscribe(
                result => {
                    expect(result).toEqual(mockedUsers);

                    done();
                });

        });
    });

    describe('updateUserProfilePicture should', () => {
        it('call http.put() once with correct parameters', () => {
            // Arrange
            const userId = '5';
            const url = `${CONFIG.MAIN_URL}/users/${userId}`;
            const img = new UserProfilePictureDTO();
            const spy = jest.spyOn(http, 'put');

            // Act
            service.updateUserProfilePicture(img, userId);

            // Arrange
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(url, img);
        });

        it('http.put() return correct value', done => {
            // Arrange
            const userId = '5';
            const img = new UserProfilePictureDTO();
            const baseUser = new BaseUserDTO();

            jest.spyOn(http, 'put').mockReturnValue(of(baseUser));

            // Act & Assert
            service.updateUserProfilePicture(img, userId).subscribe(
                result => {
                    expect(result).toEqual(baseUser);

                    done();
                });
        });
    });
});
