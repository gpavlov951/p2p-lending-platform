import { DebtsService } from '../../../app/core/services/debts.service';
import { async, TestBed } from '@angular/core/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CONFIG } from '../../../app/config/config';
import { of } from 'rxjs';
import { CreateDebtDTO } from '../../../app/common/models/debt-models/create-debt.dto';
import { UpdateDebtDTO } from '../../../app/common/models/debt-models/update-debt.dto';

describe('DebtsService', () => {
    let http;

    let service: DebtsService;

    beforeEach(async(() => {
        jest.clearAllMocks();

        http = {
            get() { },
            post() { },
            put() { },
            delete() { },
        };

        TestBed.configureTestingModule({
            imports: [HttpClientModule],
            providers: [DebtsService],
        })
            .overrideProvider(HttpClient, { useValue: http });

        service = TestBed.get(DebtsService);
    }));

    it('should be define', () => {
        expect(service).toBeDefined();
    });

    describe('getAllDebts should', () => {
        it('call http.get() once with correct parameters', () => {
            // Arrange
            const url = `${CONFIG.MAIN_URL}/debts`;

            const spy = jest.spyOn(http, 'get');

            // Act
            service.getAllDebts();

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(url);
        });

        it('http.get() return correct value', done => {
            // Arrange
            const mockReturn = 'fake';

            jest.spyOn(http, 'get').mockReturnValue(of(mockReturn));

            // Act & Assert
            service.getAllDebts().subscribe(
                (result) => {
                    expect(result).toEqual(mockReturn);

                    done();
                });
        });
    });

    describe('getAllDebtsPerUser', () => {
        it('call http.get() once with correct parameters', () => {
            // Arrange
            const userId = '5';
            const url = `${CONFIG.MAIN_URL}/users/${userId}/debts`;

            const spy = jest.spyOn(http, 'get');

            // Act
            service.getAllDebtsPerUser(userId);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(url);
        });

        it('http.get() return correct value', done => {
            // Arrange
            const userId = '5';
            const mockReturn = 'fake';

            jest.spyOn(http, 'get').mockReturnValue(of(mockReturn));

            // Act & Assert
            service.getAllDebtsPerUser(userId).subscribe(
                (result) => {
                    expect(result).toEqual(mockReturn);

                    done();
                });
        });
    });

    describe('createDebt', () => {
        it('call http.post() once with correct parameters', () => {
            // Arrange
            const userId = '5';
            const debt = new CreateDebtDTO();
            const url = `${CONFIG.MAIN_URL}/users/${userId}/debts`;

            const spy = jest.spyOn(http, 'post');

            // Act
            service.createDebt(userId, debt);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(url, debt);
        });

        it('http.post() return correct value', done => {
            // Arrange
            const userId = '5';
            const mockReturn = 'fake';
            const debt = new CreateDebtDTO();

            jest.spyOn(http, 'post').mockReturnValue((of(mockReturn)));

            // Act & Assert
            service.createDebt(userId, debt).subscribe(
                (result) => {
                    expect(result).toEqual(mockReturn);

                    done();
                });
        });
    });

    describe('updateDebt', () => {
        it('call http.put() once with correct parameters', () => {
            // Arrange
            const userId = '5';
            const updatedDebt = new UpdateDebtDTO();
            const debtId = '5';
            const url = `${CONFIG.MAIN_URL}/users/${userId}/debts/${debtId}`;

            const spy = jest.spyOn(http, 'put');
            // Act
            service.updateDebt(userId, updatedDebt, debtId);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(url, updatedDebt);
        });

        it('http.put() return correct value', done => {
            // Arrange
            const userId = '5';
            const updatedDebt = new UpdateDebtDTO();
            const debtId = '5';
            const mockReturn = 'fake';

            jest.spyOn(http, 'put').mockReturnValue(of(mockReturn));

            // Act & Assert
            service.updateDebt(userId, updatedDebt, debtId).subscribe(
                (result) => {
                    expect(result).toEqual(mockReturn);

                    done();
                });

        });
    });

    describe('deleteDebt', () => {
        it('call http.delete() once with correct parameters', () => {
            // Arrange
            const userId = '5';
            const debtId = '5';
            const url = `${CONFIG.MAIN_URL}/users/${userId}/debts/${debtId}`;

            const spy = jest.spyOn(http, 'delete');

            // Act
            service.deleteDebt(userId, debtId);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(url);
        });

        it('http.delete() return correct value', done => {
            // Arrange
            const userId = '5';
            const debtId = '5';
            const mockReturn = 'fake';

            jest.spyOn(http, 'delete').mockReturnValue(of(mockReturn));

            // Act & Assert
            service.deleteDebt(userId, debtId).subscribe(
                result => {
                    expect(result).toEqual(mockReturn);

                    done();
                });
        });
    });
});
