import { InvestmentsService } from '../../../app/core/services/investments.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TestBed, async } from '@angular/core/testing';
import { CONFIG } from '../../../app/config/config';
import { of } from 'rxjs';
import { UpdateInvestmentStatusDTO } from '../../../app/common/models/investment-models/update-investment-status.dto';

describe('InvstmentService', () => {
    let httpClient;

    let service: InvestmentsService;

    beforeEach(async(() => {
        jest.clearAllMocks();

        httpClient = {
            get() { },
            post() { },
            put() { }
        };

        TestBed.configureTestingModule({
            imports: [HttpClientModule],
            providers: [
                InvestmentsService,
            ]
        })
            .overrideProvider(HttpClient, { useValue: httpClient });

        service = TestBed.get(InvestmentsService);
    }));

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
    describe('InvestmentService', () => {
        it('it should get all investments per user', () => {
            // Arrange
            const userId = '1';
            const url = `${CONFIG.MAIN_URL}/users/${userId}/investments`;
            const returnValue = of('return value');

            const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

            const result = service.getAllInvestmentsPerUser(userId);

            // Act & Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(url);
        });

        it('it should be get all investments per user with valid data', done => {
            // Arrange
            const userId = '1';
            const url = `${CONFIG.MAIN_URL}/users/${userId}/investments`;
            const returnValue = of('return value');

            const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

            // Act
            const result = service.getAllInvestmentsPerUser(userId);

            // Assert
            result.subscribe(data => {
                expect(data).toEqual('return value');

                done();
            });
        });

        it('it should call create investment to valid url', () => {
            // Arrange
            const userId = '1';
            const debtId = '1';
            const investment = {
                amount: 1000,
                period: 10,
                interest: 10,
                penaltyInterest: 10
            };
            const url = `${CONFIG.MAIN_URL}/users/${userId}/debts/${debtId}/investments`;
            const returnValue = of(investment);

            const spy = jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);

            // Act
            const result = service.createInvestment(userId, debtId, investment);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(url, investment);
        });

        it('it should create investment', done => {
            // Arrange
            const userId = '1';
            const debtId = '1';
            const investment = {
                amount: 1000,
                period: 10,
                interest: 10,
                penaltyInterest: 10
            };
            const url = `${CONFIG.MAIN_URL}/users/${userId}/debts/${debtId}/investments`;
            const returnValue = of('return value');

            const spy = jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);

            // Act
            const result = service.createInvestment(userId, debtId, investment);

            // Assert
            result.subscribe(data => {
                expect(data).toEqual('return value');

                done();
            });
        });

        it('it should call update investment with correct url', () => {
            // Arrange
            const userId = '1';
            const investmentId = '1';
            const status = new UpdateInvestmentStatusDTO();
            const url = `${CONFIG.MAIN_URL}/investments/${investmentId}`;
            const returnValue = of('return value');

            const spy = jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);

            // Act
            const result = service.updateInvestmentStatus(userId, status);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(url, status);
        });


        it('it should call update investment to valid url', done => {
            // Arrange
            const userId = '1';
            const investmentId = '1';
            const status = new UpdateInvestmentStatusDTO();
            const url = `${CONFIG.MAIN_URL}/investments/${investmentId}`;
            const returnValue = of('return value');

            const spy = jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);

            // Act
            const result = service.updateInvestmentStatus(userId, status);

            // Assert
            result.subscribe(data => {
                expect(data).toEqual('return value');

                done();
            });
        });
    });
});
