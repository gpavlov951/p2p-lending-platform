import { ERROR_INTERCEPTOR, UNAUTHORIZE_INTERCEPTOR } from './core/interceptors/index';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { HomeComponent } from './components/home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { RegisterComponent } from './components/register/register.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { LoginComponent } from './components/login/login.component';
import { TOKEN_INTERCEPTOR } from './core/interceptors';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ServerErrorComponent } from './components/server-error/server-error.component';
import { JwtModule } from '@auth0/angular-jwt';
import { UserSummaryComponent } from './shared/components/user-summary-component/user-summary.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    RegisterComponent,
    LoginComponent,
    NotFoundComponent,
    ServerErrorComponent,
    UserSummaryComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    AppRoutingModule,
    SharedModule,
    JwtModule.forRoot({ config: {} }),
  ],
  providers: [
    TOKEN_INTERCEPTOR,
    ERROR_INTERCEPTOR,
    UNAUTHORIZE_INTERCEPTOR,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
