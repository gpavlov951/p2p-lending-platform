import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PaymentToPaidDTO } from '../../../../common/models/payment-models/payment-to-paid.dto';
import { BaseLoanDTO } from '../../../../../app/common/models/loan-models/base-loan.dto';
import { UtilsService } from '../../../../core/services/utils.service';
import moment from 'moment';

@Component({
  selector: 'app-loan-view',
  templateUrl: './loan-view.component.html',
  styleUrls: ['./loan-view.component.css']
})
export class LoanViewComponent implements OnInit {

  @Input() loan: BaseLoanDTO;
  @Input() userBalance: number;

  canPaid: boolean;

  totalAmount: number;
  installmentAmount: number;
  paymentsCount: number;
  createdOnDate: string;
  interestRate: number;

  currentDate: string = moment(new Date()).format('YYYY-MM-DD');

  leftToPaid: number;
  nextDueDate: string;
  overDueDays: number;
  penaltyAmount: number;

  @Output() paymentData: EventEmitter<PaymentToPaidDTO> = new EventEmitter();
  @Output() currentLoan: EventEmitter<BaseLoanDTO> = new EventEmitter();

  constructor(
    private readonly utils: UtilsService,
  ) { }

  ngOnInit() {
    this.totalAmount = this.loan.totalAmount;
    this.installmentAmount = this.loan.monthlyInstalmet;
    this.paymentsCount = this.loan.payments.length;
    this.createdOnDate = this.loan.createdOn;
    this.interestRate = this.loan.investment.interest;

    this.leftToPaid = this.utils.calculateLeftAmountToPay(this.totalAmount, this.installmentAmount, this.paymentsCount);
    this.nextDueDate = this.utils.calculateNextDueDate(this.createdOnDate, this.paymentsCount);

    if (this.utils.calculateOverDueDays(this.nextDueDate, this.currentDate) > 0) {
      this.overDueDays = this.utils.calculateOverDueDays(this.nextDueDate, this.currentDate.toString());
      this.penaltyAmount = this.utils
        .calculatePenaltyAmount(this.overDueDays, this.loan.investment.amount, this.loan.investment.penaltyInterest);
    } else {
      this.overDueDays = 0;
      this.penaltyAmount = 0;
    }

    if (this.userBalance > (this.installmentAmount + this.penaltyAmount)) {
      this.canPaid = true;
    }
  }

  onClickOpenPayments(): void {
    this.currentLoan.emit(this.loan);
  }

  onClickMakePayment(): void {
    this.paymentData.emit({
      loanId: this.loan.id,
      newPayment: {
        amount: this.installmentAmount,
        overDue: this.overDueDays,
        penaltyAmount: this.penaltyAmount,
      }
    });
  }
}
