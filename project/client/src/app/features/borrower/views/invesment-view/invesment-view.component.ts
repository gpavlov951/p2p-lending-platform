import { Component, Input, Output, EventEmitter } from '@angular/core';
import { BaseInvestmentDTO } from '../../../../common/models/investment-models/base-investment.dto';
import { MatDialog } from '@angular/material';
import { AgreeModalComponent } from '../../../../shared/components/agree-modal/agree-modal.component';

@Component({
  selector: 'app-invesment-view',
  templateUrl: './invesment-view.component.html',
  styleUrls: ['./invesment-view.component.css']
})
export class InvesmentViewComponent {

  @Input() invesmentOnDebt: BaseInvestmentDTO;

  @Output() investmentIdForAccept: EventEmitter<string> = new EventEmitter();
  @Output() investmentIdForReject: EventEmitter<string> = new EventEmitter();

  constructor(
    private readonly dialog: MatDialog,
  ) { }

  onClickSendAcceptInvestment(): void {
    const debtCancelModal = this.dialog.open(AgreeModalComponent, {
      maxHeight: '90vh',
      maxWidth: '95vh',
      data: 'accept this investment'
    });

    debtCancelModal.afterClosed().subscribe(
      (isAgree) => {
        if (isAgree) {
          this.investmentIdForAccept.emit(this.invesmentOnDebt.id);
        }
      });
  }

  onClickSencRejectInvestment(): void {
    const debtCancelModal = this.dialog.open(AgreeModalComponent, {
      maxHeight: '90vh',
      maxWidth: '95vh',
      data: 'reject this investment'
    });

    debtCancelModal.afterClosed().subscribe(
      (isAgree) => {
        if (isAgree) {
          this.investmentIdForReject.emit(this.invesmentOnDebt.id);
        }
      });
  }
}
