import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { BaseDebtDTO } from '../../../../common/models/debt-models/base-debt.dto';
import { DebtOnUserDTO } from '../../../../../app/common/models/debt-models/debt-on-user.dto';
import { UtilsService } from '../../../../core/services/utils.service';
import { Subscription } from 'rxjs';
import { SocketService } from '../../../../core/services/socket.service';
import { BaseInvestmentDTO } from '../../../../common/models/investment-models/base-investment.dto';
import { StatusInvestment } from '../../../../common/enums/status-investment.enum';

@Component({
  selector: 'app-debt-view',
  templateUrl: './debt-view.component.html',
  styleUrls: ['./debt-view.component.css'],
})
export class DebtViewComponent implements OnInit, OnDestroy {

  private socketSubscription: Subscription;
  private socketSubscriptionSecond: Subscription;

  @Input() debt: DebtOnUserDTO;

  investments: number;
  remainingAmount: number;

  @Output() debtEmit: EventEmitter<BaseDebtDTO> = new EventEmitter();
  @Output() debtForUpdate: EventEmitter<BaseDebtDTO> = new EventEmitter();
  @Output() debtForCancel: EventEmitter<BaseDebtDTO> = new EventEmitter();

  constructor(
    private readonly utils: UtilsService,
    private readonly socketService: SocketService,
  ) { }

  ngOnInit(): void {
    this.remainingAmount = this.utils.calculateRemainingAmount(this.debt);
    this.investments = this.debt.investments
      .filter((investment: BaseInvestmentDTO) => investment.status === StatusInvestment.Pending).length;

    this.socketSubscription = this.socketService.newInvest$.subscribe(
      (emittedInvest) => {
        if (emittedInvest && emittedInvest.debtId === this.debt.id) {
          this.debt.investments.unshift(emittedInvest);
          this.investments++;
        }
      });

    this.socketSubscriptionSecond = this.socketService.declinedInvestments$.subscribe(
      (declinedInvestments) => {
        if (declinedInvestments) {
          if (this.debt.investments.filter(i => declinedInvestments.includes(i.id))) {
            this.debt.investments = this.debt.investments
              .filter((i) => !declinedInvestments.includes(i.id));
            this.investments--;
          }
        }
      }
    );
  }

  ngOnDestroy(): void {
    if (this.socketSubscription) {
      this.socketSubscription.unsubscribe();
    }

    if (this.socketSubscriptionSecond) {
      this.socketSubscriptionSecond.unsubscribe();
    }
  }

  onClickViewDebtInvestment(): void {
    this.debtEmit.emit(this.debt);
  }

  onClickUpdateDebt(): void {
    this.debtForUpdate.emit(this.debt);
  }

  onClickCancelDebt(): void {
    this.debtForCancel.emit(this.debt);
  }

}
