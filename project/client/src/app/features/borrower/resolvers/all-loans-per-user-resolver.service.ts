import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { switchMap, first } from 'rxjs/operators';
import { AuthService } from '../../../../app/core/services/auth.service';
import { BaseLoanDTO } from '../../../../app/common/models/loan-models/base-loan.dto';
import { LoansService } from '../../../../app/core/services/loans.service';
import { UserDTO } from 'src/app/common/models/user-models/user.dto';
import { Observable } from 'rxjs';

@Injectable()
export class AllLoansPerUserResolverService implements Resolve<BaseLoanDTO[]> {

    constructor(
        private readonly authService: AuthService,
        private readonly loansService: LoansService,
    ) { }

    resolve(): Observable<BaseLoanDTO[]> {
        return this.authService.loggedUser$
            .pipe(
                switchMap((user: UserDTO) => this.loansService.getAllLoansPerUser(user.id)),
                first()
            );
    }
}
