import { Injectable } from '@angular/core';
import { Resolve} from '@angular/router';
import { switchMap, first } from 'rxjs/operators';
import { AuthService } from '../../../../app/core/services/auth.service';
import { DebtsService } from 'src/app/core/services/debts.service';
import { UserDTO } from 'src/app/common/models/user-models/user.dto';
import { Observable } from 'rxjs';
import { DebtOnUserDTO } from 'src/app/common/models/debt-models/debt-on-user.dto';

@Injectable()
export class AllDebtsPerUserResolverService implements Resolve<DebtOnUserDTO[]> {

    constructor(
        private readonly authService: AuthService,
        private readonly debtsService: DebtsService,
    ) { }

    resolve(): Observable<DebtOnUserDTO[]> {
        return this.authService.loggedUser$
            .pipe(
                switchMap((user: UserDTO) => this.debtsService.getAllDebtsPerUser(user.id)),
                first()
            );
    }
}
