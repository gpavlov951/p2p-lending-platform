import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { borrowerRoutes } from '../../../app/config/routes';

@NgModule({
    imports: [RouterModule.forChild(borrowerRoutes)],
    exports: [RouterModule]
})
export class BorrowerRoutingModule { }
