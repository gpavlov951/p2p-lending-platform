import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DebtOnUserDTO } from '../../../../common/models/debt-models/debt-on-user.dto';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilsService } from '../../../../core/services/utils.service';

@Component({
  selector: 'app-update-debt-request-modal',
  templateUrl: './update-debt-request-modal.component.html',
  styleUrls: ['./update-debt-request-modal.component.css']
})
export class UpdateDebtRequestModalComponent {

  updateLoan: FormGroup;
  minAmountForUpdate: number;

  constructor(
    private readonly utils: UtilsService,
    private readonly formBuilder: FormBuilder,
    private readonly debtDialog: MatDialogRef<UpdateDebtRequestModalComponent>,
    @Inject(MAT_DIALOG_DATA) public debtInfo: DebtOnUserDTO,
  ) {
    this.minAmountForUpdate = debtInfo.amount - this.utils.calculateRemainingAmount(debtInfo);

    this.updateLoan = this.formBuilder.group({
      amount: [this.minAmountForUpdate, Validators.compose([Validators.required, Validators.min(this.minAmountForUpdate)])],
    });
  }

  onClickCloseModal(): void {
    this.debtDialog.close();
  }


}
