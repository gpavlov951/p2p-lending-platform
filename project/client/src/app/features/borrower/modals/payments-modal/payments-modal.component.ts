import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { PaymentDTO } from '../../../../common/models/payment-models/payment.dto';

@Component({
  selector: 'app-payments-modal',
  templateUrl: './payments-modal.component.html',
  styleUrls: ['./payments-modal.component.css']
})
export class PaymentsModalComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) public loanPayments: PaymentDTO[],
  ) { }

}
