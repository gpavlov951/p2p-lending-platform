import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-debt-request-modal',
  templateUrl: './debt-request-modal.component.html',
  styleUrls: ['./debt-request-modal.component.css']
})
export class DebtRequestModalComponent {

  createDebt: FormGroup;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly dialogRef: MatDialogRef<DebtRequestModalComponent>,
  ) {
    this.createDebt = this.formBuilder.group({
      amount: [100, Validators.compose([Validators.required, Validators.min(100)])],
      period: [1, Validators.compose([Validators.required, Validators.min(1)])],
      isPartial: [false],
    });
  }

  onClickCloseModal(): void {
    this.dialogRef.close();
  }

}
