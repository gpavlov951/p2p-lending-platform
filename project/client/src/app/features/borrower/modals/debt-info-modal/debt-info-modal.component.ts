import { Component, OnInit, Inject, Output, EventEmitter, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DebtOnUserDTO } from '../../../../../app/common/models/debt-models/debt-on-user.dto';
import { BaseInvestmentDTO } from '../../../../common/models/investment-models/base-investment.dto';
import { StatusInvestment } from '../../../../common/enums/status-investment.enum';
import { UtilsService } from '../../../../core/services/utils.service';
import { Subscription } from 'rxjs';
import { SocketService } from '../../../../core/services/socket.service';

@Component({
  selector: 'app-debt-info-modal',
  templateUrl: './debt-info-modal.component.html',
  styleUrls: ['./debt-info-modal.component.css']
})
export class DebtInfoModalComponent implements OnInit, OnDestroy {

  private socketSubscription: Subscription;
  private socketSubscriptionSecond: Subscription;

  @Output() investmentIdForAccept: EventEmitter<string> = new EventEmitter();
  @Output() investmentIdForReject: EventEmitter<string> = new EventEmitter();

  investmentsOnDebt: BaseInvestmentDTO[] = [];
  remainingAmount: number;

  constructor(
    private readonly utils: UtilsService,
    private readonly debtDialog: MatDialogRef<DebtInfoModalComponent>,
    @Inject(MAT_DIALOG_DATA) public debtInfo: DebtOnUserDTO,
    private readonly socketService: SocketService,
  ) {
    this.remainingAmount = this.utils.calculateRemainingAmount(debtInfo);
    this.investmentsOnDebt = debtInfo.investments
      .filter((investment: BaseInvestmentDTO) => investment.status === StatusInvestment.Pending);
  }

  ngOnInit() {
    this.socketSubscription = this.socketService.newInvest$.subscribe(
      (emittedInvest) => {
        if (emittedInvest && !this.investmentsOnDebt.find((investment) => investment.id === emittedInvest.id)) {
          this.investmentsOnDebt.unshift(emittedInvest);
        }
      });

    this.socketSubscription = this.socketService.declinedInvestments$.subscribe(
      (declinedInvestments) => {
        if (declinedInvestments) {
          this.investmentsOnDebt = this.investmentsOnDebt
            .filter((i) => !declinedInvestments.includes(i.id));

          if (!this.investmentsOnDebt.length) {
            this.debtDialog.close();
          }
        }
      }
    );
  }

  ngOnDestroy() {
    if (this.socketSubscription) {
      this.socketSubscription.unsubscribe();
    }

    if (this.socketSubscriptionSecond) {
      this.socketSubscriptionSecond.unsubscribe();
    }
  }

  onClickSendRejectInvestment(investmentId: string): void {
    this.investmentIdForReject.emit(investmentId);

    this.investmentsOnDebt = this.investmentsOnDebt.filter((investment) => investment.id !== investmentId);

    if (this.investmentsOnDebt.length === 0) {
      this.debtDialog.close();
    }
  }

  onClickSendAcceptInvestment(investmentId: string): void {
    const clickedInvestment = this.investmentsOnDebt.find((i: BaseInvestmentDTO) => i.id === investmentId);
    this.remainingAmount -= clickedInvestment.amount;

    this.investmentsOnDebt = this.investmentsOnDebt
      .filter((i: BaseInvestmentDTO) => i.amount <= this.remainingAmount && i.id !== clickedInvestment.id);

    this.investmentIdForAccept.emit(investmentId);
    if (this.investmentsOnDebt.length === 0) {
      this.debtDialog.close();
    }
  }
}
