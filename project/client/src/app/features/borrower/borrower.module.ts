import { NgModule } from '@angular/core';
import { BorrowerComponent } from './borrower-component/borrower.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { BorrowerRoutingModule } from './borrower-routing.module';
import { LoanViewComponent } from './views/loan-view/loan-view.component';
import { DebtViewComponent } from './views/debt-view/debt-view.component';
import { DebtRequestModalComponent } from './modals/debt-request-modal/debt-request-modal.component';
import { DebtInfoModalComponent } from './modals/debt-info-modal/debt-info-modal.component';
import { AllLoansPerUserResolverService } from './resolvers/all-loans-per-user-resolver.service';
import { AllDebtsPerUserResolverService } from './resolvers/all-debts-per-user-resolver.service';
import { InvesmentViewComponent } from './views/invesment-view/invesment-view.component';
import { AgreeModalComponent } from '../../shared/components/agree-modal/agree-modal.component';
import { UpdateDebtRequestModalComponent } from './modals/update-debt-request-modal/update-debt-request-modal.component';
import { PaymentsModalComponent } from './modals/payments-modal/payments-modal.component';

@NgModule({
  declarations: [
    BorrowerComponent,
    LoanViewComponent,
    DebtViewComponent,
    InvesmentViewComponent,

    DebtRequestModalComponent,
    DebtInfoModalComponent,
    UpdateDebtRequestModalComponent,
    PaymentsModalComponent,
  ],
  imports: [
    SharedModule,
    BorrowerRoutingModule,
  ],
  providers: [
    AllLoansPerUserResolverService,
    AllDebtsPerUserResolverService,
  ],
  entryComponents: [
    DebtRequestModalComponent,
    DebtInfoModalComponent,
    UpdateDebtRequestModalComponent,
    PaymentsModalComponent,

    AgreeModalComponent,
  ],
})
export class BorrowerModule { }
