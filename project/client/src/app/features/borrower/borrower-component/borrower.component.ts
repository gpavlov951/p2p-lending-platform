import { DebtInfoModalComponent } from './../modals/debt-info-modal/debt-info-modal.component';
import { Component, OnInit, OnDestroy, ViewChildren, QueryList } from '@angular/core';
import { MatDialog } from '@angular/material';
import { NotificatorService } from '../../../../app/core/services/notificator.service';
import { LoanDTO } from '../../../../app/common/models/loan-models/loan.dto';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../../app/core/services/auth.service';
import { DebtsService } from '../../../core/services/debts.service';
import { PaymentsService } from '../../../core/services/payments.service';
import { PaymentToPaidDTO } from '../../../common/models/payment-models/payment-to-paid.dto';
import { ActivatedRoute } from '@angular/router';
import { DebtOnUserDTO } from '../../../../app/common/models/debt-models/debt-on-user.dto';
import { DebtRequestModalComponent } from '../modals/debt-request-modal/debt-request-modal.component';
import { BaseLoanDTO } from '../../../../app/common/models/loan-models/base-loan.dto';
import { LoansService } from '../../../../app/core/services/loans.service';
import { StatusLoan } from '../../../common/enums/status-loan.enum';
import { InvestmentsService } from '../../../core/services/investments.service';
import { StatusInvestment } from '../../../common/enums/status-investment.enum';
import { InvestmentDTO } from '../../../common/models/investment-models/investment.dto';
import { BaseInvestmentDTO } from '../../../common/models/investment-models/base-investment.dto';
import { DebtViewComponent } from '../views/debt-view/debt-view.component';
import { AgreeModalComponent } from '../../../shared/components/agree-modal/agree-modal.component';
import { UpdateDebtRequestModalComponent } from '../modals/update-debt-request-modal/update-debt-request-modal.component';
import { BaseDebtDTO } from '../../../common/models/debt-models/base-debt.dto';
import { PaymentsModalComponent } from '../modals/payments-modal/payments-modal.component';
import { UtilsService } from '../../../core/services/utils.service';

@Component({
  selector: 'app-borrower',
  templateUrl: './borrower.component.html',
  styleUrls: ['./borrower.component.css'],
  viewProviders: [DebtViewComponent],
})
export class BorrowerComponent implements OnInit, OnDestroy {
  private userSubscription: Subscription;
  private loggedUserInfo: Subscription;
  private investmentIdForAccept: Subscription;
  private investmentIdForReject: Subscription;

  userBalance: number;
  userId: string;
  loans: BaseLoanDTO[] = [];
  debts: DebtOnUserDTO[] = [];

  @ViewChildren(DebtViewComponent) debtViews: QueryList<DebtViewComponent>;

  constructor(
    private readonly dialog: MatDialog,
    private readonly debtsService: DebtsService,
    private readonly notificator: NotificatorService,
    private readonly authService: AuthService,
    private readonly paymentsService: PaymentsService,
    private readonly route: ActivatedRoute,
    private readonly loansService: LoansService,
    private readonly investmentsService: InvestmentsService,
    private readonly utils: UtilsService,
  ) { }

  ngOnInit() {
    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => {
        if (user) {
          this.userId = user.id;
        }
      }
    );

    this.loggedUserInfo = this.authService.loggedUserInfo$.subscribe(
      loggedUserInfo => {
        if (loggedUserInfo) {
          this.userBalance = loggedUserInfo.balance;
        }
      }
    );

    this.route.data.subscribe(
      ({ debts, loans }) => {
        this.debts = debts;
        this.loans = loans;
      }
    );
  }

  ngOnDestroy() {
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }

    if (this.loggedUserInfo) {
      this.loggedUserInfo.unsubscribe();
    }

    if (this.investmentIdForAccept) {
      this.investmentIdForAccept.unsubscribe();
    }

    if (this.investmentIdForReject) {
      this.investmentIdForReject.unsubscribe();
    }
  }

  makePaymentOnLoan(paymentData: PaymentToPaidDTO): void {
    const debtCancelModal = this.dialog.open(AgreeModalComponent, {
      maxHeight: '90vh',
      maxWidth: '95vh',
      data: 'make payment on this debt'
    });

    debtCancelModal.afterClosed().subscribe(
      (isAgree) => {
        if (isAgree) {
          this.paymentsService.createPayments(this.userId, paymentData.loanId, paymentData.newPayment).subscribe(
            (newLoan: LoanDTO) => {

              if (newLoan.status === StatusLoan.Completed) {

                this.loans = this.loans.filter((loan: BaseLoanDTO) => loan.id !== newLoan.id);
                this.notificator.success(`Your loan has been successfully paid!`);

              } else {
                const index: number = this.loans.findIndex((loan: BaseLoanDTO) => loan.id === paymentData.loanId);
                this.loans[index] = newLoan;
              }
            }
          );

        }
      });
  }

  openCurrentLoanPayments(currentLoan: BaseLoanDTO): void {
    if (currentLoan.payments.length !== 0) {
      this.dialog.open(PaymentsModalComponent, {
        maxHeight: '90vh',
        maxWidth: '95vh',
        data: currentLoan.payments,
      });
    } else {
      this.notificator.warn(`Still no payments yet!`);
    }
  }

  openDebtDataModal(debt: DebtOnUserDTO): void {
    if (debt.investments.find((invest: BaseInvestmentDTO) => invest.status === StatusInvestment.Pending &&
      invest.user.balance >= invest.amount)) {
      const debtDataDialog = this.dialog.open(DebtInfoModalComponent, {
        maxHeight: '90vh',
        maxWidth: '95vh',
        data: debt,
      });

      this.investmentIdForAccept = debtDataDialog.componentInstance.investmentIdForAccept.subscribe(
        (investmentId: string) => {
          const remainingAmount: number = debtDataDialog.componentInstance.remainingAmount;

          debt.investments.find((investment: BaseInvestmentDTO) => investment.id === investmentId)
            .status = StatusInvestment.Accepted;

          debt.investments
            .filter((investment: BaseInvestmentDTO) => investment.amount > remainingAmount)
            .map((investment: BaseInvestmentDTO) => investment.status = StatusInvestment.Rejected);

          this.debtViews.find((debtChildren: DebtViewComponent) => +debtChildren.debt.id === +debt.id)
            .remainingAmount = remainingAmount;

          this.loansService.createLoans(this.userId, debt.id, investmentId).subscribe(
            (newLoan: LoanDTO) => {
              this.loans.unshift(newLoan);
            },
          );

          this.debtViews.find((debtChildren: DebtViewComponent) => +debtChildren.debt.id === +debt.id)
            .investments = debt.investments.filter((i) => i.status === StatusInvestment.Pending).length;

          if (remainingAmount === 0) {
            this.debts = this.debts.filter((debtForFilter: DebtOnUserDTO) => debtForFilter.id !== debt.id);
          }
        }
      );

      this.investmentIdForReject = debtDataDialog.componentInstance.investmentIdForReject.subscribe(
        (investmentId: string) => {
          this.investmentsService.updateInvestmentStatus(investmentId, { status: StatusInvestment.Rejected }).subscribe(
            (investment: InvestmentDTO) => {

              const foundInvestment: BaseInvestmentDTO = debt.investments
                .find((investmentToFind: BaseInvestmentDTO) => investmentToFind.id === investmentToFind.id);
              foundInvestment.status = investment.status;
            },
          );
        });

    } else {
      this.notificator.warn(`Still no invesments yet!`);
    }
  }

  openDebtUpdateModal(debt: DebtOnUserDTO) {
    const debtUpdateModal = this.dialog.open(UpdateDebtRequestModalComponent, {
      maxHeight: '90vh',
      maxWidth: '95vh',
      data: debt,
    });

    debtUpdateModal.afterClosed().subscribe(
      (updatedAmount) => {
        if (updatedAmount) {

          this.debtsService.updateDebt(this.userId, { amount: updatedAmount.value.amount }, debt.id).subscribe(
            (updatedDebt: BaseDebtDTO) => {
              const debtComponent = this.debtViews.find((debtChildren: DebtViewComponent) => debtChildren.debt.id === updatedDebt.id);

              debtComponent.debt.amount = updatedDebt.amount;
              debtComponent.remainingAmount = this.utils.calculateRemainingAmount(debtComponent.debt);

              let counter = 0;
              debt.amount = updatedDebt.amount;
              debt.investments
                .map((investment: BaseInvestmentDTO) => {
                  if (investment.amount > debtComponent.remainingAmount) {
                    return investment.status = StatusInvestment.Rejected;
                  } else {
                    if (investment.status === StatusInvestment.Pending) {
                      counter++;
                      return investment;
                    }
                  }
                }
                );

              debtComponent.investments = counter;

              if (debtComponent.remainingAmount === 0) {
                this.debts = this.debts.filter((debtToFind: DebtOnUserDTO) => debtToFind.id !== debt.id);
              }
            });
        }
      });
  }

  openDebtCancelModal(debt: DebtOnUserDTO) {
    const debtCancelModal = this.dialog.open(AgreeModalComponent, {
      maxHeight: '90vh',
      maxWidth: '95vh',
      data: 'cancel this loan request'
    });

    debtCancelModal.afterClosed().subscribe(
      (isAgree) => {
        if (isAgree) {
          this.debtsService.deleteDebt(this.userId, debt.id).subscribe(
            ((debtForDelete: BaseDebtDTO) =>
              this.debts = this.debts.filter((debtForFilter: DebtOnUserDTO) => debtForFilter.id !== debtForDelete.id)),
          );
        }
      });
  }

  openDebtRequest(): void {
    const debtRequestDialog = this.dialog.open(DebtRequestModalComponent, {
      maxHeight: '90vh',
      maxWidth: '95vh',
    });

    debtRequestDialog.afterClosed().subscribe(
      (newDebt) => {
        if (newDebt) {
          this.debtsService.createDebt(this.userId, newDebt.value).subscribe(
            (createdDebt: DebtOnUserDTO) => {
              this.notificator.success(`Your debt request is created successful!`);
              this.debts.unshift(createdDebt);
            },
          );
        }
      });
  }
}
