import { Component, OnInit, Input } from '@angular/core';
import { InvestmentDTO } from '../../../../common/models/investment-models/investment.dto';
import { LoanDTO } from '../../../../common/models/loan-models/loan.dto';
import { PaymentDTO } from '../../../../common/models/payment-models/payment.dto';
import { UtilsService } from '../../../../core/services/utils.service';

@Component({
  selector: 'app-investor-details-view',
  templateUrl: './investor-details-view.component.html',
  styleUrls: ['./investor-details-view.component.css']
})
export class InvestorDetailsViewComponent implements OnInit {

  @Input() investment: InvestmentDTO;

  investmentLoan: LoanDTO;
  instalmentsLeft: number;
  nextDueDate: string;
  nextInstalment: number;
  overDue: number;
  totalPenalties: number;
  totalPenaltyAmount: number;
  amountLeft: number;

  constructor(
    private readonly utils: UtilsService,
  ) { }

  ngOnInit() {
    const totalAmount = this.investment.loans[0].totalAmount;
    const monthlyInstalment = this.investment.loans[0].monthlyInstalmet;
    const loanCreated = this.investment.loans[0].createdOn;
    const paymentsCount = this.investment.loans[0].payments;

    this.amountLeft = this.utils.calculateLeftAmountToPay(totalAmount, monthlyInstalment, paymentsCount.length);
    this.investmentLoan = this.investment.loans[0];
    this.instalmentsLeft = this.investment.period - paymentsCount.length;
    this.nextDueDate = this.utils.calculateNextDueDate(loanCreated, paymentsCount.length);
    this.nextInstalment = monthlyInstalment;
    this.overDue = paymentsCount.filter((payment: PaymentDTO) => payment.overDue !== 0).length;
    this.totalPenaltyAmount = paymentsCount
      .reduce((acc: number, payment: PaymentDTO) => acc += payment.penaltyAmount, 0);
  }
}
