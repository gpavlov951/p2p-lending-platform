import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BaseInvestmentWithoutUserDTO } from '../../../../common/models/investment-models/base-investment-without-user.dto';
import { MatDialog } from '@angular/material';
import { AgreeModalComponent } from '../../../../shared/components/agree-modal/agree-modal.component';

@Component({
  selector: 'app-invest-pending-view',
  templateUrl: './invest-pending-view.component.html',
  styleUrls: ['./invest-pending-view.component.css']
})
export class InvestPendingViewComponent implements OnInit {

  @Input() investPending: BaseInvestmentWithoutUserDTO;
  @Output() cancelInvest = new EventEmitter();

  debtUsername: string;
  investAmount: number;
  investPeriod: number;
  investPartial: boolean;

  constructor(
    private readonly dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.investAmount = this.investPending.amount;
    this.debtUsername = this.investPending.debt.user.username;
    this.investPeriod = this.investPending.period;
    this.investPartial = this.investPending.debt.isPartial;
  }

  cancelInvestment() {
    const debtCancelModal = this.dialog.open(AgreeModalComponent, {
      maxHeight: '90vh',
      maxWidth: '95vh',
      data: 'cancel this investment'
    });

    debtCancelModal.afterClosed().subscribe(
      (isAgree) => {
        if (isAgree) {
          this.cancelInvest.emit(this.investPending);
        }
      });
  }
}
