import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AllDebtDTO } from '../../../../common/models/debt-models/all-debt-data.dto';
import { UtilsService } from '../../../../core/services/utils.service';

@Component({
  selector: 'app-dept-view',
  templateUrl: './dept-view.component.html',
  styleUrls: ['./dept-view.component.css']
})
export class DeptViewComponent implements OnInit {

  @Input() debt: AllDebtDTO;
  @Output() sendDebt: EventEmitter<AllDebtDTO> = new EventEmitter();

  remainingAmount: number;

  constructor(
    private readonly utils: UtilsService,
  ) { }

  ngOnInit() {
    this.remainingAmount = this.utils.calculateRemainingAmount(this.debt);

  }
  sendDebtId(): void {
    this.sendDebt.emit(this.debt);
  }
}
