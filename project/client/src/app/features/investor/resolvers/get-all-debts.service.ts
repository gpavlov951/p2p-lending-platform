import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { DebtsService } from '../../../core/services/debts.service';
import { AllDebtDTO } from '../../../common/models/debt-models/all-debt-data.dto';

@Injectable()
export class GetAllDebtsResolverService implements Resolve<AllDebtDTO[]> {
    constructor(
        private readonly debtsService: DebtsService,
    ) { }

    resolve(): Observable<AllDebtDTO[]> {
        return this.debtsService.getAllDebts();
    }
}
