import { Resolve } from '@angular/router';
import { AuthService } from '../../../core/services/auth.service';
import { Observable } from 'rxjs';
import { switchMap, first } from 'rxjs/operators';
import { UserDTO } from '../../../common/models/user-models/user.dto';
import { Injectable } from '@angular/core';
import { InvestmentsService } from '../../../core/services/investments.service';
import { InvestmentDTO } from '../../../common/models/investment-models/investment.dto';

@Injectable()
export class GetUserAllInvestmentsResolverService implements Resolve<InvestmentDTO[]> {
    constructor(
        private readonly authService: AuthService,
        private readonly investmentsService: InvestmentsService,
    ) { }

    resolve(): Observable<InvestmentDTO[]> {
        return this.authService.loggedUser$
            .pipe(
                switchMap((user: UserDTO) => this.investmentsService.getAllInvestmentsPerUser(user.id)),
                first()
            );
    }
}
