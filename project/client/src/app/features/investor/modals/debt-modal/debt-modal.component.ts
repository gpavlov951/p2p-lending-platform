import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilsService } from '../../../../core/services/utils.service';
import { DebtOnUserDTO } from '../../../../common/models/debt-models/debt-on-user.dto';

@Component({
  selector: 'app-debt-view-modal',
  templateUrl: './debt-modal.component.html',
  styleUrls: ['./debt-modal.component.css']
})
export class DebtViewModalComponent {

  createInvest: FormGroup;
  remainingAmount: number;

  constructor(
    private readonly utils: UtilsService,
    private readonly formBuilder: FormBuilder,
    private readonly dialogRef: MatDialogRef<DebtViewModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { debt: DebtOnUserDTO, investorBalance: number },
  ) {
    this.remainingAmount = this.utils.calculateRemainingAmount(this.data.debt);
    if (this.data.investorBalance < this.remainingAmount) {
      this.remainingAmount = this.data.investorBalance;
    }

    if (this.data.debt.isPartial) {
      this.createInvest = this.formBuilder.group({
        amount: [this.remainingAmount, Validators.compose([Validators.required, Validators.min(1), Validators.max(this.remainingAmount)])],
        period: [1, Validators.compose([Validators.required, Validators.min(1)])],
        interest: [1, Validators.compose([Validators.required, Validators.min(1)])],
        penaltyInterest: [1, Validators.compose([Validators.required, Validators.min(1)])],
      });
    } else {
      this.createInvest = this.formBuilder.group({
        amount: [{ value: data.debt.amount, disabled: true }],
        period: [{ value: data.debt.period, disabled: true }],
        interest: [1, Validators.compose([Validators.required, Validators.min(1)])],
        penaltyInterest: [1, Validators.compose([Validators.required, Validators.min(1)])],
      });
    }
  }

  onClickCloseModal(): void {
    this.dialogRef.close();
  }

}
