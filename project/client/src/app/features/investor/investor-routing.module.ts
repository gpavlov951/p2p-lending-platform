import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { investorRoutes } from '../../config/routes';

@NgModule({
    imports: [RouterModule.forChild(investorRoutes)],
    exports: [RouterModule]
})
export class InvestorRoutingModule { }
