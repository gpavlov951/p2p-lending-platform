import { SocketService } from './../../../core/services/socket.service';
import { Component, OnInit, OnDestroy, ViewChildren, QueryList } from '@angular/core';
import { InvestmentDTO } from '../../../common/models/investment-models/investment.dto';
import { InvestmentsService } from '../../../core/services/investments.service';
import { AuthService } from '../../../core/services/auth.service';
import { Subscription } from 'rxjs';
import { StatusInvestment } from '../../../common/enums/status-investment.enum';
import { DebtViewModalComponent } from '../modals/debt-modal/debt-modal.component';
import { MatDialog } from '@angular/material';
import { NotificatorService } from '../../../core/services/notificator.service';
import { DebtDTO } from '../../../common/models/debt-models/debt.dto';
import { BaseInvestmentWithoutUserDTO } from '../../../common/models/investment-models/base-investment-without-user.dto';
import { AllDebtDTO } from '../../../common/models/debt-models/all-debt-data.dto';
import { ActivatedRoute } from '@angular/router';
import { LoggedUserInfoDTO } from '../../../common/models/user-models/logged-user-info.dto';
import { StatusLoan } from '../../../common/enums/status-loan.enum';
import { InvestorDetailsViewComponent } from '../views/investor-details-view/investor-details-view.component';
import { CreatePaymentDTO } from '../../../common/models/payment-models/create-payment.dto';

@Component({
  selector: 'app-investor',
  templateUrl: './investor.component.html',
  styleUrls: ['./investor.component.css']
})
export class InvestorComponent implements OnInit, OnDestroy {

  debts: AllDebtDTO[] = [];
  investments: InvestmentDTO[] = [];
  investPending: BaseInvestmentWithoutUserDTO[] = [];

  private socketSubscription: Subscription;
  private socketSubscriptionSecond: Subscription;
  private socketSubscriptionThird: Subscription;
  private socketSubscriptionFourth: Subscription;
  private loggedUserInfoSummarySubscription: Subscription;

  investorBalance: number;
  userId: string;

  constructor(
    private readonly dialog: MatDialog,
    private readonly investmentService: InvestmentsService,
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService,
    private readonly route: ActivatedRoute,
    private readonly socketService: SocketService,
  ) { }

  ngOnInit() {

    this.loggedUserInfoSummarySubscription = this.authService.loggedUserInfo$.subscribe(
      (loggedUserInfo: LoggedUserInfoDTO) => {

        this.investorBalance = loggedUserInfo.balance;
        this.userId = loggedUserInfo.id;
      }
    );

    this.route.data.subscribe(
      ({ userInvestments, allDebts }) => {
        this.investments = userInvestments.filter(
          (investment: InvestmentDTO) => investment.status === StatusInvestment.Accepted
            && investment.loans[0].status === StatusLoan.InProcess
        );

        this.investPending = userInvestments.filter(
          (investPending: BaseInvestmentWithoutUserDTO) => investPending.status === StatusInvestment.Pending);

        this.debts = allDebts.filter(((debt: AllDebtDTO) => debt.user.id !== this.userId));
      }
    );

    this.socketSubscription = this.socketService.newDebt$.subscribe(
      (emittedDebt: AllDebtDTO) => {
        if (emittedDebt) {
          this.debts.unshift(emittedDebt);
        }
      }
    );

    this.socketSubscriptionSecond = this.socketService.acceptedInvest$.subscribe(
      (emmitedAcceptedInvest: InvestmentDTO) => {
        if (emmitedAcceptedInvest) {
          this.investments.unshift(emmitedAcceptedInvest);
        }
      }
    );

    this.socketSubscriptionThird = this.socketService.newPayment$.subscribe(
      (newPayment) => {
        if (newPayment) {
          const indexOfCurrentInvestment = this.investments.findIndex((i) => +i.loans[0].id === +newPayment.loanId);
          const investmentToSwap = { ...this.investments[indexOfCurrentInvestment] };

          investmentToSwap.loans[0].payments.push(({ overDue: newPayment.overDue, penaltyAmount: newPayment.penaltyAmount } as any));

          this.investments = this.investments.filter((i) => i.id !== this.investments[indexOfCurrentInvestment].id);

          if (investmentToSwap.loans[0].payments.length !== investmentToSwap.period) {
            this.investments.unshift(investmentToSwap);
          }
        }
      }
    );

    this.socketSubscriptionFourth = this.socketService.declinedInvestments$.subscribe(
      (declinedInvestments) => {
        if (declinedInvestments) {
          this.investPending = this.investPending.filter((i) => !declinedInvestments.includes(i.id));
        }
      }
    );
  }

  ngOnDestroy() {

    if (this.socketSubscription) {
      this.socketSubscription.unsubscribe();
    }

    if (this.socketSubscriptionSecond) {
      this.socketSubscriptionSecond.unsubscribe();
    }

    if (this.loggedUserInfoSummarySubscription) {
      this.loggedUserInfoSummarySubscription.unsubscribe();
    }

    if (this.socketSubscriptionThird) {
      this.socketSubscriptionThird.unsubscribe();
    }

    if (this.socketSubscriptionFourth) {
      this.socketSubscriptionFourth.unsubscribe();
    }
  }

  createInvest(debt: DebtDTO) {
    if ((this.investorBalance < debt.amount) && (!debt.isPartial)) {
      this.notificator.warn(`You don't have enough money for invest`);
      return;
    }
    const dialogRef = this.dialog.open(DebtViewModalComponent, {
      width: '300px',
      data: {
        debt,
        investorBalance: this.investorBalance,
      }
    });

    dialogRef.afterClosed().subscribe((sendDebt) => {

      if (sendDebt) {
        this.investmentService.createInvestment(this.userId, debt.id, sendDebt.getRawValue()).subscribe(
          (newInvestment: InvestmentDTO) => {
            this.investPending.unshift(newInvestment);
            this.notificator.success(`Your invest request is created successfuly`);
          });
      }
    });
  }

  cancelInvestment(invest: BaseInvestmentWithoutUserDTO) {
    this.investmentService.updateInvestmentStatus(invest.id, { status: StatusInvestment.Rejected }).subscribe(
      (currentInvest: BaseInvestmentWithoutUserDTO) => {
        this.investPending = this.investPending.filter(
          (investment) => investment.id !== currentInvest.id);
      });
  }
}


