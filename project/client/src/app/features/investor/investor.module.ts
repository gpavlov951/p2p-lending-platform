import { NgModule } from '@angular/core';
import { InvestorComponent } from './investor-component/investor.component';
import { SharedModule } from '../../shared/shared.module';
import { InvestorRoutingModule } from './investor-routing.module';
import { DeptViewComponent } from './views/dept-view/dept-view.component';
import { DebtViewModalComponent } from './modals/debt-modal/debt-modal.component';
import { InvestPendingViewComponent } from './views/invest-pending-view/invest-pending-view.component';
import { InvestorDetailsViewComponent } from './views/investor-details-view/investor-details-view.component';
import { GetUserAllInvestmentsResolverService } from './resolvers/get-user-all-investments.service';
import { GetAllDebtsResolverService } from './resolvers/get-all-debts.service';
import { AgreeModalComponent } from '../../shared/components/agree-modal/agree-modal.component';

@NgModule({
  declarations: [
    InvestorComponent,
    InvestorDetailsViewComponent,
    DeptViewComponent,
    DebtViewModalComponent,
    InvestPendingViewComponent
  ],
  imports: [
    SharedModule,
    InvestorRoutingModule
  ],
  providers: [
    GetUserAllInvestmentsResolverService,
    GetAllDebtsResolverService
  ],
  entryComponents: [
    DebtViewModalComponent,
    AgreeModalComponent
  ],
})
export class InvestorModule { }
