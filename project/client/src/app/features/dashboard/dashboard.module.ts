import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard-component/dashboard.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DepositModalComponent } from './modals/deposit-modal/deposit-modal.component';
import { WithdrawModalComponent } from './modals/withdraw-modal/withdraw-modal.component';

@NgModule({
  declarations: [
    DashboardComponent,
    DepositModalComponent,
    WithdrawModalComponent
  ],
  imports: [
    SharedModule,
    DashboardRoutingModule,
  ],
  providers: [],
  entryComponents: [
    DepositModalComponent,
    WithdrawModalComponent
  ],

})
export class DashboardModule { }
