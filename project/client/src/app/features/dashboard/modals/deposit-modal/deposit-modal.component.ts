import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-deposit-modal',
  templateUrl: './deposit-modal.component.html',
  styleUrls: ['./deposit-modal.component.css']
})
export class DepositModalComponent {

  createDeposit: FormGroup;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly dialogRef: MatDialogRef<DepositModalComponent>,
  ) {
    this.createDeposit = this.formBuilder.group({
      deposit: ['', Validators.compose([Validators.required, Validators.min(1)])],
    });
  }

  onClickCloseModal(): void {
    this.dialogRef.close();
  }

}
