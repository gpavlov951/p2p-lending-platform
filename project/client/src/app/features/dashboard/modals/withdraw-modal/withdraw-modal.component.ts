import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-withdraw-modal',
  templateUrl: './withdraw-modal.component.html',
  styleUrls: ['./withdraw-modal.component.css']
})
export class WithdrawModalComponent {

  createWithdraw: FormGroup;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly dialogRef: MatDialogRef<WithdrawModalComponent>,
    @Inject(MAT_DIALOG_DATA) public loggedUserBalance: number) {
    this.createWithdraw = this.formBuilder.group({
      withdraw: ['', Validators.compose([Validators.required, Validators.min(1), Validators.max(this.loggedUserBalance)])],
    });
  }

  onClickCloseModal(): void {
    this.dialogRef.close();
  }
}
