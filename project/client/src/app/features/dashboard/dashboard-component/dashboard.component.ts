import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material';
import { DepositModalComponent } from '../modals/deposit-modal/deposit-modal.component';
import { NotificatorService } from '../../../core/services/notificator.service';
import { WithdrawModalComponent } from '../modals/withdraw-modal/withdraw-modal.component';
import { AuthService } from '../../../core/services/auth.service';
import { UsersService } from '../../../core/services/users.service';
import { LoggedUserInfoDTO } from '../../../common/models/user-models/logged-user-info.dto';
import { InvestmentDTO } from '../../../common/models/investment-models/investment.dto';
import { StatusInvestment } from '../../../common/enums/status-investment.enum';
import { StatusLoan } from '../../../common/enums/status-loan.enum';
import { LoanDTO } from '../../../common/models/loan-models/loan.dto';
import { InvestorUserDTO } from '../../../common/models/user-models/investor-user.dto';
import { BorrowerUserDTO } from '../../../common/models/user-models/borrower-user.dto';
import { UpdateUserBalanceDTO } from '../../../common/models/user-models/update-user-balance.dto';
import { UpdateBalanceStatus } from '../../../common/enums/user-balance-status.enum';
import { UtilsService } from '../../../core/services/utils.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
  private loggedUserInfoSummarySubscription: Subscription;

  loggedUserInfo: LoggedUserInfoDTO;

  userAsInvestor: InvestorUserDTO = new InvestorUserDTO();
  userAsBorrower: BorrowerUserDTO = new BorrowerUserDTO();

  constructor(
    private readonly utils: UtilsService,
    private readonly dialog: MatDialog,
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
    private readonly notificator: NotificatorService,
  ) { }

  public ngOnInit(): void {

    this.loggedUserInfoSummarySubscription = this.authService.loggedUserInfo$.subscribe(
      (loggedUserInfo: LoggedUserInfoDTO) => {
        this.loggedUserInfo = loggedUserInfo;

        if (loggedUserInfo.balance) {
          this.userAsInvestor = this.convertUserToInvestor(loggedUserInfo);
          this.userAsBorrower = this.convertUserToBorrower(loggedUserInfo);
        }
      }
    );
  }

  public ngOnDestroy(): void {
    if (this.loggedUserInfoSummarySubscription) {
      this.loggedUserInfoSummarySubscription.unsubscribe();
    }
  }

  openDialogDeposit(): void {
    const depositModal = this.dialog.open(DepositModalComponent, {
      width: '280px',
    });

    depositModal.afterClosed().subscribe(
      (deposit) => {
        if (deposit) {
          const newDeposit: UpdateUserBalanceDTO = { action: UpdateBalanceStatus.Deposit, amount: deposit.value.deposit };
          this.usersService.updateUserBalance(newDeposit, this.loggedUserInfo.id).subscribe(
            () => {
              this.notificator.success(`Your deposit request is created successful!`);
            },
          );
        }
      }
    );
  }

  openDialogWithdraw(): void {
    const withdrawModal = this.dialog.open(WithdrawModalComponent, {
      width: '260px',
      data: this.loggedUserInfo.balance
    });

    withdrawModal.afterClosed().subscribe(
      (withdraw) => {
        if (withdraw) {
          const newWithdraw: UpdateUserBalanceDTO = { action: UpdateBalanceStatus.Withdraw, amount: withdraw.value.withdraw };
          this.usersService.updateUserBalance(newWithdraw, this.loggedUserInfo.id).subscribe(
            () => {
              this.notificator.success(`Your withdraw request is created successful!`);
            },
          );
        }
      }
    );
  }

  private convertUserToBorrower(loggedUserInfo: LoggedUserInfoDTO): BorrowerUserDTO {
    const userToReturn = new BorrowerUserDTO();

    userToReturn.totalPeriodDebts = loggedUserInfo.loans
      .map((loan: LoanDTO) => loan.investment.period)
      .sort()
      .pop();

    userToReturn.investors = new Set(loggedUserInfo.loans
      .map((loan: LoanDTO) => loan.investment.user.username)).size;

    const nextLoanForBorrower = loggedUserInfo.loans
      .map((loan: LoanDTO) =>
        ({
          dueDate: this.utils.calculateNextDueDate(loan.createdOn, loan.payments.length), monthlyInstallment: loan.monthlyInstalmet
        }))
      .sort((a, b) => a.dueDate.localeCompare(b.dueDate))
      .shift();

    if (nextLoanForBorrower) {
      userToReturn.nextDueDateToPaid = nextLoanForBorrower.dueDate;
      userToReturn.moneyPaid = nextLoanForBorrower.monthlyInstallment;
    }

    return userToReturn;
  }

  private convertUserToInvestor(loggedUserInfo: LoggedUserInfoDTO): InvestorUserDTO {
    const userToReturn = new InvestorUserDTO();

    userToReturn.totalPeriodInvestments = loggedUserInfo.investments
      .filter((investment: InvestmentDTO) =>
        investment.status === StatusInvestment.Accepted && investment.loans[0].status === StatusLoan.InProcess)
      .map((investment: InvestmentDTO) => investment.period)
      .sort((a, b) => b - a)
      .shift();

    userToReturn.borrowers = new Set(loggedUserInfo.investments
      .filter((investment: InvestmentDTO) =>
        investment.status === StatusInvestment.Accepted && investment.loans[0].status === StatusLoan.InProcess)
      .map((investment: InvestmentDTO) => investment.debt.user.username)).size;

    const nextLoanForInvestor = loggedUserInfo.investments
      .filter((investment: InvestmentDTO) =>
        investment.status === StatusInvestment.Accepted && investment.loans[0].status === StatusLoan.InProcess)
      .map((investment: InvestmentDTO) =>
        ({
          dueDate: this.utils.calculateNextDueDate(investment.loans[0].createdOn, investment.loans[0].payments.length),
          monthlyInstallment: investment.loans[0].monthlyInstalmet
        }))
      .sort((a, b) => a.dueDate.localeCompare(b.dueDate))
      .shift();

    if (nextLoanForInvestor) {
      userToReturn.nextDueDateToGetMoney = nextLoanForInvestor.dueDate;
      userToReturn.moneyToGet = nextLoanForInvestor.monthlyInstallment;
    }

    return userToReturn;
  }
}
