import { Routes } from '@angular/router';
import { BorrowerComponent } from '../features/borrower/borrower-component/borrower.component';
import { AllLoansPerUserResolverService } from '../features/borrower/resolvers/all-loans-per-user-resolver.service';
import { AllDebtsPerUserResolverService } from '../features/borrower/resolvers/all-debts-per-user-resolver.service';
import { InvestorComponent } from '../features/investor/investor-component/investor.component';
import { GetUserAllInvestmentsResolverService } from '../features/investor/resolvers/get-user-all-investments.service';
import { GetAllDebtsResolverService } from '../features/investor/resolvers/get-all-debts.service';
import { HomeComponent } from '../components/home/home.component';
import { AnonymousGuard } from '../core/guards/anonymous.guard';
import { RegisterComponent } from '../components/register/register.component';
import { LoginComponent } from '../components/login/login.component';
import { AuthGuard } from '../core/guards/auth.guard';
import { NotFoundComponent } from '../components/not-found/not-found.component';
import { ServerErrorComponent } from '../components/server-error/server-error.component';
import { DashboardComponent } from '../features/dashboard/dashboard-component/dashboard.component';

export const appRoutes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent, canActivate: [AnonymousGuard] },
    { path: 'register', component: RegisterComponent, canActivate: [AnonymousGuard] },
    { path: 'login', component: LoginComponent, canActivate: [AnonymousGuard] },

    {
        path: 'dashboard', loadChildren: () => import('../features/dashboard/dashboard.module').then(m => m.DashboardModule),
        canActivate: [AuthGuard]
    },

    {
        path: 'borrower', loadChildren: () => import('../features/borrower/borrower.module').then(m => m.BorrowerModule),
        canActivate: [AuthGuard]
    },

    {
        path: 'investor', loadChildren: () => import('../features/investor/investor.module').then(m => m.InvestorModule),
        canActivate: [AuthGuard]
    },

    { path: 'not-found', component: NotFoundComponent },
    { path: 'server-error', component: ServerErrorComponent },

    { path: '**', redirectTo: '/not-found' }
];

export const dashboardRoutes: Routes = [
    {
        path: '', component: DashboardComponent, pathMatch: 'full',
    }
];

export const borrowerRoutes: Routes = [
    {
        path: '', component: BorrowerComponent,
        resolve: {
            debts: AllDebtsPerUserResolverService,
            loans: AllLoansPerUserResolverService
        },
        pathMatch: 'full',
    },
];

export const investorRoutes: Routes = [
    {
        path: '', component: InvestorComponent,
        pathMatch: 'full', resolve: {
            userInvestments: GetUserAllInvestmentsResolverService,
            allDebts: GetAllDebtsResolverService
        }
    }
];
