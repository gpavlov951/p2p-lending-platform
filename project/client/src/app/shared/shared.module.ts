import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AgreeModalComponent } from './components/agree-modal/agree-modal.component';
import { ZeroPipe } from './pipes/zero.pipe';
import { YesNoPipe } from './pipes/yes-no.pipe';
import { FooterComponent } from './components/footer/footer.component';
import { CustomLoaderComponent } from './components/custom-loader/custom-loader.component';
import { OverdueDirective } from './directives/overdue.directive';

@NgModule({
  declarations: [
    AgreeModalComponent,
    ZeroPipe,
    YesNoPipe,
    FooterComponent,
    CustomLoaderComponent,
    OverdueDirective,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    NgxSpinnerModule,
  ],
  exports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    CommonModule,
    MaterialModule,
    NgxSpinnerModule,

    AgreeModalComponent,
    ZeroPipe,
    YesNoPipe,
    FooterComponent,
    CustomLoaderComponent,
    OverdueDirective
  ],
})
export class SharedModule { }
