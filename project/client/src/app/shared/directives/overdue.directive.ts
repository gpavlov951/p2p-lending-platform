import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
    selector: '[appOverdue]'
})
export class OverdueDirective {

    @Input('appOverdue')

    set isOverdue(isOverdue: boolean) {
        if (isOverdue) {
            this.renderer.setStyle(
                this.el.nativeElement,
                'boxShadow',
                '0px 0px 3px 1px red'
            );

        }
    }

    constructor(
        private el: ElementRef,
        private readonly renderer: Renderer2,
    ) { }
}
