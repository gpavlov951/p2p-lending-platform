import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-agree-modal',
  templateUrl: './agree-modal.component.html',
  styleUrls: ['./agree-modal.component.css']
})
export class AgreeModalComponent {

  textToShow: string;
  isAgree: boolean;

  constructor(
    private readonly debtDialog: MatDialogRef<AgreeModalComponent>,
    @Inject(MAT_DIALOG_DATA) public text: string,
  ) {
    this.textToShow = `Are you sure you want to ${text}?`;
  }

  cancel(): void {
    this.debtDialog.close(this.isAgree);
  }

  agree(): void {
    this.isAgree = true;
    this.debtDialog.close(this.isAgree);
  }
}
