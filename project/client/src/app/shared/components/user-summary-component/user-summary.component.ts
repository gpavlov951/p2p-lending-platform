import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserDTO } from '../../../common/models/user-models/user.dto';
import { AuthService } from '../../../core/services/auth.service';
import { Router } from '@angular/router';
import { LoggedUserInfoDTO } from '../../../common/models/user-models/logged-user-info.dto';
import { UsersService } from '../../../core/services/users.service';
import { NotificatorService } from '../../../core/services/notificator.service';
import { InvestmentDTO } from '../../../common/models/investment-models/investment.dto';
import { StatusInvestment } from '../../../common/enums/status-investment.enum';
import { StatusLoan } from '../../../common/enums/status-loan.enum';
import { LoanDTO } from '../../../common/models/loan-models/loan.dto';
import { UserSummaryDTO } from '../../../common/models/user-models/user-summary.dto';
import { UserSummaryService } from '../../../core/services/user-summary.service';
import { UtilsService } from '../../../core/services/utils.service';
import { SocketService } from '../../../core/services/socket.service';
import { CreatePaymentDTO } from '../../../common/models/payment-models/create-payment.dto';

@Component({
  selector: 'app-user-summary',
  templateUrl: './user-summary.component.html',
  styleUrls: ['./user-summary.component.css']
})
export class UserSummaryComponent implements OnInit, OnDestroy {

  private loggedUserInfoSummarySubscription: Subscription;
  private socketSubscription: Subscription;
  private socketSubscriptionTwo: Subscription;

  @Input() user: UserDTO;

  loggedUserInfo: LoggedUserInfoDTO;
  userSummary: UserSummaryDTO = new UserSummaryDTO();

  constructor(
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly authService: AuthService,
    private readonly userSummaryService: UserSummaryService,
    private readonly usersService: UsersService,
    private readonly utils: UtilsService,
    private readonly socketService: SocketService,
  ) { }

  ngOnInit(): void {
    this.loggedUserInfoSummarySubscription = this.authService.loggedUserInfo$.subscribe(
      (loggedUserInfo: LoggedUserInfoDTO) => {
        this.loggedUserInfo = loggedUserInfo;

        if (loggedUserInfo.balance) {
          this.userSummary = this.convertToUserSummaryDTO(loggedUserInfo);
        }
      }
    );

    this.socketSubscription = this.socketService.acceptedInvest$.subscribe(
      (emmitedAcceptedInvest: InvestmentDTO) => {
        if (emmitedAcceptedInvest) {
          this.loggedUserInfo.balance -= emmitedAcceptedInvest.amount / 2;
          this.userSummary.totalInvestedAmount += emmitedAcceptedInvest.loans[0].totalAmount;
        }
      }
    );

    this.socketSubscriptionTwo = this.socketService.newPayment$.subscribe(
      (payment: CreatePaymentDTO) => {
        if (payment) {
          this.loggedUserInfo.balance += (payment.amount + payment.penaltyAmount);
          this.userSummary.totalInvestedAmount -= payment.amount;
        }
      }
    );

  }

  ngOnDestroy(): void {
    if (this.loggedUserInfoSummarySubscription) {
      this.loggedUserInfoSummarySubscription.unsubscribe();
    }

    if (this.socketSubscription) {
      this.socketSubscription.unsubscribe();
    }

    if (this.socketSubscriptionTwo) {
      this.socketSubscriptionTwo.unsubscribe();
    }
  }

  logout(): void {
    this.authService.logout()
      .subscribe(
        () => {
          this.notificator.success(`Successful logout!`);
          this.router.navigate(['/']);
        },
        () => {
          this.notificator.error(`Logout failed!`);
        }
      );
  }

  editUserImg(file: File): void {
    this.userSummaryService.uploadImg(file)
      .toPromise()
      .then((res: any) => {
        this.loggedUserInfo.userProfileImgUrl = res.data.link;
        this.usersService.updateUserProfilePicture({ img: res.data.link }, this.loggedUserInfo.id).subscribe();
      });
  }

  private convertToUserSummaryDTO(loggedUserInfo: LoggedUserInfoDTO): UserSummaryDTO {
    const summaryToReturn = new UserSummaryDTO();

    summaryToReturn.totalDebtsAmount = loggedUserInfo.loans
      .reduce((acc, curr) => acc +
        this.utils.calculateLeftAmountToPay(curr.totalAmount, curr.monthlyInstalmet, curr.payments.length), 0);

    summaryToReturn.totalInvestedAmount = loggedUserInfo.investments
      .filter((investment: InvestmentDTO) => investment.status === StatusInvestment.Accepted)
      .reduce((acc, curr) =>
        acc +
        this.utils
          .calculateLeftAmountToPay(curr.loans[0].totalAmount, curr.loans[0].monthlyInstalmet, curr.loans[0].payments.length), 0);

    summaryToReturn.nextDueDate = loggedUserInfo.loans
      .filter((loan: LoanDTO) => loan.status === StatusLoan.InProcess)
      .map((loan: LoanDTO) => this.utils.calculateNextDueDate(loan.createdOn, loan.payments.length))
      .sort()
      .shift();

    return summaryToReturn;
  }
}
