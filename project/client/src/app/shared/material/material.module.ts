import { NgModule } from '@angular/core';

import {
  MatButtonModule,
  MatInputModule,
  MatFormFieldModule,
  MatIconModule,
  MatCardModule,
  MatToolbarModule,
  MatDividerModule,
  MatTabsModule,
  MatDialogModule,
  MatGridListModule,
  MatCheckboxModule,
  MatSidenavModule,
  MatExpansionModule,
  MatListModule,
  MatBadgeModule,
} from '@angular/material';

@NgModule({
  imports: [
  ],
  declarations: [],
  exports: [
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatCardModule,
    MatToolbarModule,
    MatTabsModule,
    MatDialogModule,
    MatGridListModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatExpansionModule,
    MatDividerModule,
    MatListModule,
    MatBadgeModule,
  ],
})
export class MaterialModule { }
