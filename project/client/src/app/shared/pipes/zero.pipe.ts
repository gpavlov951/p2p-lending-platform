import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'zero'
})
export class ZeroPipe implements PipeTransform {

    transform(zero, type: string): string {
        if (type === 'amount' || type === 'date') {
            if (zero === null) {
                return 'Currently not yet!';
            } else {
                return zero;
            }
        }

        if (zero === undefined || zero === 0) {
            return `0 ${type}s`;
        } else if (zero === 1) {
            return `1 ${type}`;
        } else {
            return `${zero} ${type}s`;
        }
    }
}
