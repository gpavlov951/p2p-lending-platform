import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from './core/services/auth.service';
import { UserDTO } from './common/models/user-models/user.dto';
import { CustomLoaderService } from './core/services/custom-loader.service';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {

  private loggedInSubscription: Subscription;
  private userSubscription: Subscription;

  loggedIn: boolean;
  user: UserDTO;

  constructor(
    private readonly authService: AuthService,
    private readonly customLoaderService: CustomLoaderService,
    private readonly router: Router,
  ) {
    this.router.events.pipe(
      filter(e => e instanceof NavigationEnd)).subscribe(e => this.customLoaderService.hide(500));
    this.router.events.pipe(
      filter(e => e instanceof NavigationStart)).subscribe(e => this.customLoaderService.show());
  }

  public ngOnInit() {
    this.loggedInSubscription = this.authService.isLoggedIn$.subscribe(
      loggedIn => this.loggedIn = loggedIn,
    );
    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => this.user = user,
    );
  }

  public ngOnDestroy() {
    this.loggedInSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
  }
}
