export enum UpdateBalanceStatus {
    Deposit = 'Deposit',
    Withdraw = 'Withdraw',
}
