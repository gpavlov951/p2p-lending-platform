export enum StatusInvestment {
    Pending = 'Pending',
    Accepted = 'Accepted',
    Rejected = 'Rejected',
}
