export enum StatusDebt {
    Pending = 'Pending',
    PartialFunded = 'PartialFunded',
    FullyFunded = 'FullyFunded',
}
