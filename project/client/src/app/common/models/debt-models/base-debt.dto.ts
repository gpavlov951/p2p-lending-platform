import { StatusDebt } from '../../enums/status-debt.enum';

export class BaseDebtDTO {

    id: string;

    createdOn: string;

    amount: number;

    period: number;

    status: StatusDebt;

    isPartial: boolean;
}
