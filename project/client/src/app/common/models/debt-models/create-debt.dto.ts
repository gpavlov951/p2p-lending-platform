export class CreateDebtDTO {

    amount: number;

    period: number;

    isPartial: boolean;
}
