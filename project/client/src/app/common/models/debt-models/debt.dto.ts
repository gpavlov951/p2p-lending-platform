import { StatusDebt } from '../../enums/status-debt.enum';
import { UserDTO } from '../user-models/user.dto';

export class DebtDTO {

    id: string;

    createdOn: string;

    amount: number;

    period: number;

    status: StatusDebt;

    isPartial: boolean;

    user: UserDTO;
}
