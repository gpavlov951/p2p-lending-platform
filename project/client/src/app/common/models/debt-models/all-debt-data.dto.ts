import { StatusDebt } from '../../enums/status-debt.enum';
import { BaseInvestmentDTO } from '../investment-models/base-investment.dto';
import { LoanWithoutPaymentsDTO } from '../loan-models/loan-without-payments.dto';
import { BaseUserDTO } from '../user-models/base-user.dto';

export class AllDebtDTO {

    id: string;

    createdOn: string;

    amount: number;

    period: number;

    status: StatusDebt;

    isPartial: boolean;

    investments: BaseInvestmentDTO[];

    loans: LoanWithoutPaymentsDTO[];

    user: BaseUserDTO;
}
