import { StatusInvestment } from '../../enums/status-investment.enum';
import { UserDTO } from '../user-models/user.dto';
import { DebtDTO } from '../debt-models/debt.dto';
import { BaseUserDTO } from '../user-models/base-user.dto';
import { LoanDTO } from '../loan-models/loan.dto';

export class InvestmentDTO {

    id: string;

    amount: number;

    period: number;

    interest: number;

    penaltyInterest: number;

    status: StatusInvestment;

    user: BaseUserDTO;

    debt: DebtDTO;

    loans: LoanDTO[];
}
