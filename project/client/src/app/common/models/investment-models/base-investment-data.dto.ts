export class BaseInvestmentDataDTO {

    id: string;

    amount: number;

    period: number;

    interest: number;

    penaltyInterest: number;
}
