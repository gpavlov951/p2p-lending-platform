import { StatusInvestment } from '../../enums/status-investment.enum';

export class UpdateInvestmentStatusDTO {

    status: StatusInvestment;
}
