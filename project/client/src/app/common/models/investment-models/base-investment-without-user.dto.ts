import { StatusInvestment } from '../../enums/status-investment.enum';
import { DebtDTO } from '../debt-models/debt.dto';

export class BaseInvestmentWithoutUserDTO {

    id: string;

    amount: number;

    period: number;

    interest: number;

    penaltyInterest: number;

    status: StatusInvestment;

    debt: DebtDTO;
}
