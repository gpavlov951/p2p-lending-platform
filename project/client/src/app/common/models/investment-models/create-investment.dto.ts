export class CreateInvestmentDTO {

    amount: number;

    period: number;

    interest: number;

    penaltyInterest: number;
}
