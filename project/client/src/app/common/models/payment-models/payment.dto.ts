export class PaymentDTO {

    id: string;

    createdOn: Date;

    amount: number;

    overDue: number;

    penaltyAmount: number;
}
