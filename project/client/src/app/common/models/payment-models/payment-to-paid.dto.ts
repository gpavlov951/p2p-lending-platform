import { CreatePaymentDTO } from './create-payment.dto';

export class PaymentToPaidDTO {

    loanId: string;

    newPayment: CreatePaymentDTO;
}
