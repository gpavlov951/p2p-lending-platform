export class CreatePaymentDTO {

    amount: number;

    overDue?: number;

    penaltyAmount?: number;
}
