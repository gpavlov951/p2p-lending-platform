import { BaseUserDTO } from '../user-models/base-user.dto';
import { BaseInvestmentDTO } from '../investment-models/base-investment.dto';
import { PaymentDTO } from '../payment-models/payment.dto';
import { StatusLoan } from '../../enums/status-loan.enum';

export class LoanDTO {

    id: string;

    createdOn: string;

    totalAmount: number;

    monthlyInstalmet: number;

    status: StatusLoan;

    user: BaseUserDTO;

    investment: BaseInvestmentDTO;

    payments: PaymentDTO[];
}
