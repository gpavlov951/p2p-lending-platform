import { BaseInvestmentDataDTO } from '../investment-models/base-investment-data.dto';
import { StatusLoan } from '../../enums/status-loan.enum';

export class LoanWithoutPaymentsDTO {

    id: string;

    createdOn: string;

    totalAmount: number;

    monthlyInstalmet: number;

    status: StatusLoan;

    investment: BaseInvestmentDataDTO;
}
