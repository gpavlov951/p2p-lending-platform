import { BaseInvestmentDTO } from '../investment-models/base-investment.dto';
import { PaymentDTO } from '../payment-models/payment.dto';
import { StatusLoan } from '../../enums/status-loan.enum';

export class BaseLoanDTO {

    id: string;

    createdOn: string;

    totalAmount: number;

    monthlyInstalmet: number;

    status: StatusLoan;

    investment: BaseInvestmentDTO;

    payments: PaymentDTO[];
}
