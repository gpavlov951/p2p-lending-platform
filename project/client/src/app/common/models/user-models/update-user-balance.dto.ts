import { UpdateBalanceStatus } from '../../enums/user-balance-status.enum';

export class UpdateUserBalanceDTO {

    action: UpdateBalanceStatus;

    amount: number;
}
