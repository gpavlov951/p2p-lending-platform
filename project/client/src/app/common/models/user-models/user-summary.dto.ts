export class UserSummaryDTO {

    totalDebtsAmount: number;

    totalInvestedAmount: number;

    nextDueDate: string;
}
