import { InvestmentDTO } from '../investment-models/investment.dto';
import { LoanDTO } from '../loan-models/loan.dto';

export class LoggedUserInfoDTO {

    id: string;

    balance: number;

    userProfileImgUrl: string;

    investments: InvestmentDTO[];

    loans: LoanDTO[];
}
