export class BorrowerUserDTO {

    totalPeriodDebts: number;

    investors: number;

    nextDueDateToPaid: string;

    moneyPaid: number;
}
