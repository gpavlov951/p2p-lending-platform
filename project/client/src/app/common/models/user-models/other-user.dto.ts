import { BaseDebtDTO } from '../debt-models/base-debt.dto';
import { BaseInvestmentWithoutUserDTO } from '../investment-models/base-investment-without-user.dto';

export class OtherUserDTO {

    username: string;

    debts: BaseDebtDTO[];

    investments: BaseInvestmentWithoutUserDTO[];
}
