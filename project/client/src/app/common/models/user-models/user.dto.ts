import { UserRole } from 'src/app/common/enums/user-role.enum';

export class UserDTO {

    id: string;

    username: string;

    role: UserRole;

    isBanned: false;
}
