import { UserRole } from '../../enums/user-role.enum';

export class BaseUserDTO {

    id: string;

    username: string;

    role: UserRole;

    balance: number;

    userProfileImgUrl: string;
}
