export class InvestorUserDTO {

    totalPeriodInvestments: number;

    borrowers: number;

    nextDueDateToGetMoney: string;

    moneyToGet: number;
}
