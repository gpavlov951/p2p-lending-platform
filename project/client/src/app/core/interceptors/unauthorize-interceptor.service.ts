import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Router } from '@angular/router';
import { NotificatorService } from '../services/notificator.service';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { StorageService } from '../services/storage.service';

@Injectable()
export class UnauthorizeInterceptorService implements HttpInterceptor {

    constructor(
        private readonly router: Router,
        private readonly notificator: NotificatorService,
        private readonly storage: StorageService,
    ) { }

    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            catchError((error: any) => {
                if (error.status === 401) {
                    this.storage.clear();
                    this.router.navigate(['/']);
                    this.notificator.error('Please login again!');
                }
                return throwError(error);
            })
        );
    }
}
