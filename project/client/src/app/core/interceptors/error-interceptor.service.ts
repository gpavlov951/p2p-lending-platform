import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NotificatorService } from '../services/notificator.service';

@Injectable()
export class ErrorInterceptorService implements HttpInterceptor {
    public constructor(
        private readonly router: Router,
        private readonly notificator: NotificatorService,
    ) { }

    public intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            catchError((error: any) => {
                if (error.status === 404) {
                    this.router.navigate(['/not-found']);
                    this.notificator.error('Resource not found!');
                } else if (error.status >= 500) {
                    this.router.navigate(['/server-error']);
                    this.notificator.error('Oops.. something went wrong.. :(');
                }

                return throwError(error);
            })
        );
    }
}
