import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptorService } from './token-interceptor.service';
import { ErrorInterceptorService } from './error-interceptor.service';
import { UnauthorizeInterceptorService } from './unauthorize-interceptor.service';

export const TOKEN_INTERCEPTOR = {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true,
};

export const ERROR_INTERCEPTOR = {
    provide: HTTP_INTERCEPTORS,
    useClass: ErrorInterceptorService,
    multi: true,
};

export const UNAUTHORIZE_INTERCEPTOR = {
    provide: HTTP_INTERCEPTORS,
    useClass: UnauthorizeInterceptorService,
    multi: true,
};
