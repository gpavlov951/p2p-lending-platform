import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { StorageService } from 'src/app/core/services/storage.service';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {

    constructor(
        private readonly storageService: StorageService,
    ) { }

    intercept(
        request: HttpRequest<any>,
        next: HttpHandler,
    ) {
        const token = this.storageService.read('token') || '';

        let updatedRequest;

        if (request.url.includes('imgur')) {
            updatedRequest = request.clone({
                headers: request.headers.set('Authorization', `Client-ID 6e230461a6a04bb`)
            });
        } else {
            updatedRequest = request.clone({
                headers: request.headers.set('Authorization', `Bearer ${token}`)
            });
        }

        return next.handle(updatedRequest);
    }
}
