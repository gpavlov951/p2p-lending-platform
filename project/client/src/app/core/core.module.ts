import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { NotificatorService } from './services/notificator.service';
import { UsersService } from './services/users.service';
import { StorageService } from './services/storage.service';
import { AuthService } from './services/auth.service';
import { DebtsService } from './services/debts.service';
import { InvestmentsService } from './services/investments.service';
import { LoansService } from './services/loans.service';
import { PaymentsService } from './services/payments.service';
import { SocketService } from './services/socket.service';
import { UserSummaryService } from './services/user-summary.service';
import { UtilsService } from './services/utils.service';
import { CustomLoaderService } from './services/custom-loader.service';

@NgModule({
  providers: [
    AuthService,
    DebtsService,
    InvestmentsService,
    LoansService,
    NotificatorService,
    PaymentsService,
    StorageService,
    UsersService,
    UserSummaryService,
    UtilsService,
    CustomLoaderService,

    SocketService,
  ],
  imports: [
    HttpClientModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
      countDuplicates: true,
    }),
  ],
  exports: [
    HttpClientModule,
  ]
})
export class CoreModule { }
