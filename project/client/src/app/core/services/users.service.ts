import { CONFIG } from './../../config/config';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CreateUserDTO } from '../../common/models/user-models/create-user.dto';
import { UserDTO } from '../../common/models/user-models/user.dto';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LoggedUserInfoDTO } from 'src/app/common/models/user-models/logged-user-info.dto';
import { BaseUserDTO } from '../../common/models/user-models/base-user.dto';
import { AuthService } from './auth.service';
import { UserProfilePictureDTO } from '../../common/models/user-models/user-profile-picture.dto';
import { UpdateUserBalanceDTO } from '../../common/models/user-models/update-user-balance.dto';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private readonly http: HttpClient,
    private readonly authService: AuthService,
  ) { }

  createUser(user: CreateUserDTO): Observable<LoggedUserInfoDTO> {
    return this.http.post<LoggedUserInfoDTO>(`${CONFIG.MAIN_URL}/users`, user);
  }

  updateUserBalance(newBalance: UpdateUserBalanceDTO, userId: string): Observable<BaseUserDTO> {
    return this.http.patch<BaseUserDTO>(`${CONFIG.MAIN_URL}/users/${userId}`, newBalance)
      .pipe(
        tap((showUserDto: BaseUserDTO) => {
          this.authService.nextLoggedUserInfo$({
            ...this.authService.loggedUserInfoValue$(),
            balance: showUserDto.balance
          });
        }),
      );
  }

  getAllUsers(): Observable<UserDTO[]> {
    return this.http.get<UserDTO[]>(`${CONFIG.MAIN_URL}/users/`);
  }

  updateUserProfilePicture(img: UserProfilePictureDTO, userId: string): Observable<BaseUserDTO> {
    return this.http.put<BaseUserDTO>(`${CONFIG.MAIN_URL}/users/${userId}`, img);
  }
}
