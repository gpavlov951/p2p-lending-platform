import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoanDTO } from '../../common/models/loan-models/loan.dto';
import { CONFIG } from '../../config/config';
import { BaseLoanDTO } from 'src/app/common/models/loan-models/base-loan.dto';
import { tap } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { LoggedUserInfoDTO } from '../../common/models/user-models/logged-user-info.dto';

@Injectable({
  providedIn: 'root'
})
export class LoansService {

  constructor(
    private readonly http: HttpClient,
    private readonly authService: AuthService,
  ) { }

  getAllLoansPerUser(userId: string): Observable<BaseLoanDTO[]> {
    return this.http.get<LoanDTO[]>(`${CONFIG.MAIN_URL}/users/${userId}/loans`);
  }

  createLoans(userId: string, debtId: string, investmentId: string): Observable<LoanDTO> {
    return this.http.post<LoanDTO>(`${CONFIG.MAIN_URL}/users/${userId}/debts/${debtId}/investments/${investmentId}/loans`, {})
      .pipe(
        tap((newLoan: LoanDTO) => {
          const loggedUserInfo: LoggedUserInfoDTO = this.authService.loggedUserInfoValue$();
          loggedUserInfo.loans.push(newLoan);
          loggedUserInfo.balance += newLoan.investment.amount;

          this.authService.nextLoggedUserInfo$(loggedUserInfo);
        })
      );
  }
}
