import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { InvestmentDTO } from '../../common/models/investment-models/investment.dto';
import { CreateInvestmentDTO } from '../../common/models/investment-models/create-investment.dto';
import { CONFIG } from '../../config/config';
import { UpdateInvestmentStatusDTO } from '../../common/models/investment-models/update-investment-status.dto';

@Injectable({
  providedIn: 'root'
})
export class InvestmentsService {

  constructor(
    private readonly http: HttpClient,
  ) { }

  getAllInvestmentsPerUser(userId: string): Observable<InvestmentDTO[]> {
    return this.http.get<InvestmentDTO[]>(`${CONFIG.MAIN_URL}/users/${userId}/investments`);
  }

  createInvestment(userId: string, debtId: string, newInvestment: CreateInvestmentDTO): Observable<InvestmentDTO> {
    return this.http.post<InvestmentDTO>(`${CONFIG.MAIN_URL}/users/${userId}/debts/${debtId}/investments`, newInvestment);
  }

  updateInvestmentStatus(investmentId: string, newStatus: UpdateInvestmentStatusDTO): Observable<InvestmentDTO> {
    return this.http.put<InvestmentDTO>(`${CONFIG.MAIN_URL}/investments/${investmentId}`, newStatus);
  }
}
