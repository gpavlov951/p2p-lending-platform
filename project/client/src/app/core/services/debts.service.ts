import { UpdateDebtDTO } from './../../common/models/debt-models/update-debt.dto';
import { CreateDebtDTO } from './../../common/models/debt-models/create-debt.dto';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from '../../config/config';
import { BaseDebtDTO } from '../../common/models/debt-models/base-debt.dto';
import { DebtOnUserDTO } from '../../common/models/debt-models/debt-on-user.dto';
import { AllDebtDTO } from '../../common/models/debt-models/all-debt-data.dto';

@Injectable({
    providedIn: 'root'
})
export class DebtsService {

    constructor(
        private readonly http: HttpClient,
    ) {}

    getAllDebts(): Observable<AllDebtDTO[]> {
        return this.http.get<AllDebtDTO[]>(`${CONFIG.MAIN_URL}/debts`);
    }

    getAllDebtsPerUser(userId: string): Observable<DebtOnUserDTO[]> {
        return this.http.get<DebtOnUserDTO[]>(`${CONFIG.MAIN_URL}/users/${userId}/debts`);
    }

    createDebt(userId: string, newDebt: CreateDebtDTO): Observable<DebtOnUserDTO> {
        return this.http.post<DebtOnUserDTO>(`${CONFIG.MAIN_URL}/users/${userId}/debts`, newDebt);
    }

    updateDebt(userId: string, updatedDebt: UpdateDebtDTO, debtId: string): Observable<BaseDebtDTO> {
        return this.http.put<BaseDebtDTO>(`${CONFIG.MAIN_URL}/users/${userId}/debts/${debtId}`, updatedDebt);
    }

    deleteDebt(userId: string, debtId: string): Observable<BaseDebtDTO> {
        return this.http.delete<BaseDebtDTO>(`${CONFIG.MAIN_URL}/users/${userId}/debts/${debtId}`);
    }
}
