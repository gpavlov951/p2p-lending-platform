import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from '../../config/config';
import { CreatePaymentDTO } from '../../common/models/payment-models/create-payment.dto';
import { LoanDTO } from 'src/app/common/models/loan-models/loan.dto';
import { tap } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { LoggedUserInfoDTO } from '../../common/models/user-models/logged-user-info.dto';

@Injectable({
  providedIn: 'root'
})
export class PaymentsService {

  constructor(
    private readonly http: HttpClient,
    private readonly authService: AuthService,
  ) { }

  createPayments(userId: string, loanId: string, newPayment: CreatePaymentDTO): Observable<LoanDTO> {
    return this.http.post<LoanDTO>(`${CONFIG.MAIN_URL}/users/${userId}/loans/${loanId}/payments`, newPayment)
      .pipe(
        tap((newLoan: LoanDTO) => {
          const loggedUserInfo: LoggedUserInfoDTO = this.authService.loggedUserInfoValue$();
          const foundLoanIndex: number = loggedUserInfo.loans.findIndex(l => l.id === newLoan.id);

          loggedUserInfo.loans.splice(foundLoanIndex, 1, newLoan);
          loggedUserInfo.balance -= (newPayment.amount + newPayment.penaltyAmount);

          this.authService.nextLoggedUserInfo$(loggedUserInfo);
        })
      );
  }
}
