import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserSummaryService {

  constructor(
    private readonly http: HttpClient,
  ) { }

  uploadImg(img: File) {
    return this.http.post(`https://api.imgur.com/3/image`, img);
  }
}
