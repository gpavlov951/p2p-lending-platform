import { BaseUserDTO } from './../../common/models/user-models/base-user.dto';
import { UserDTO } from './../../common/models/user-models/user.dto';
import { LoginUserDTO } from './../../common/models/user-models/login-user.dto';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, zip } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CONFIG } from './../../config/config';
import { StorageService } from './storage.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LoggedUserInfoDTO } from '../../common/models/user-models/logged-user-info.dto';
import { InvestmentDTO } from '../../common/models/investment-models/investment.dto';
import { LoanDTO } from '../../common/models/loan-models/loan.dto';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly isLoggedInSubject$ = new BehaviorSubject<boolean>(this.isUserLoggedIn());
  private readonly loggedUserSubject$ = new BehaviorSubject<UserDTO>(this.loggedUser());

  private readonly loggedUserInfoSubject$ = new BehaviorSubject<LoggedUserInfoDTO>(new LoggedUserInfoDTO());

  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    private readonly jwtService: JwtHelperService,
  ) { }

  get isLoggedIn$(): Observable<boolean> {
    return this.isLoggedInSubject$.asObservable();
  }

  get loggedUser$(): Observable<UserDTO> {
    return this.loggedUserSubject$.asObservable();
  }

  get loggedUserInfo$(): Observable<Partial<LoggedUserInfoDTO>> {
    return this.loggedUserInfoSubject$.asObservable();
  }

  loggedUserInfoValue$(): LoggedUserInfoDTO {
    return this.loggedUserInfoSubject$.getValue();
  }

  nextLoggedUserInfo$(newSubject: LoggedUserInfoDTO): void {
    this.loggedUserInfoSubject$.next(newSubject);
  }

  login(user: LoginUserDTO): Observable<{ token: string }> {
    return this.http.post<{ token: string }>(`${CONFIG.MAIN_URL}/session`, user)
      .pipe(
        tap(({ token }) => {
          try {
            const loggedUser: UserDTO = this.jwtService.decodeToken(token);

            this.storage.save('token', token);
            this.isLoggedInSubject$.next(true);
            this.loggedUserSubject$.next(loggedUser);

            this.loggedUserInfo(loggedUser.id);
          } catch (error) { }
        }),
      );
  }

  logout(): Observable<{ msg: string }> {
    return this.http.delete<{ msg: string }>(`${CONFIG.MAIN_URL}/session`)
      .pipe(
        tap(() => {
          try {
            this.storage.clear();
            this.isLoggedInSubject$.next(false);
            this.loggedUserSubject$.next(null);
            this.loggedUserInfoSubject$.next(new LoggedUserInfoDTO());
          } catch (error) { }
        }),
      );
  }

  private isUserLoggedIn(): boolean {
    return !!this.storage.read('token');
  }

  private loggedUser(): UserDTO {
    try {
      const user: UserDTO = this.jwtService.decodeToken(this.storage.read('token'));

      this.loggedUserInfo(user.id);

      return user;
    } catch (error) {
      this.isLoggedInSubject$.next(false);

      return null;
    }
  }

  private loggedUserInfo(userId: string): void {
    try {
      zip(
        this.http.get<BaseUserDTO>(`${CONFIG.MAIN_URL}/users/${userId}/full-data`),
        this.http.get<InvestmentDTO[]>(`${CONFIG.MAIN_URL}/users/${userId}/investments`),
        this.http.get<LoanDTO[]>(`${CONFIG.MAIN_URL}/users/${userId}/loans`),
      )
        .pipe(
          tap(([userInfo, investments, loans]) => {
            let loggedUserInfo = new LoggedUserInfoDTO();

            loggedUserInfo = {
              ...userInfo,
              investments,
              loans,
            };

            this.loggedUserInfoSubject$.next(loggedUserInfo);
          })
        )
        .toPromise();
    } catch (error) { }
  }
}
