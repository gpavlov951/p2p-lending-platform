import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { WebSocketSubject } from 'rxjs/observable/dom/WebSocketSubject';
import { AuthService } from './auth.service';
import { AllDebtDTO } from '../../common/models/debt-models/all-debt-data.dto';
import { CreatePaymentDTO } from '../../common/models/payment-models/create-payment.dto';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private socketSubject$: WebSocketSubject<any>;

  private readonly newLoanSubject$ = new BehaviorSubject<AllDebtDTO>(null);
  private readonly newInvestSubject$ = new BehaviorSubject(null);
  private readonly acceptedInvestSubject$ = new BehaviorSubject(null);
  private readonly newPaymentSubject$ = new BehaviorSubject(null);
  private readonly declinedInvestmentsSubject$ = new BehaviorSubject(null);

  constructor(
    private readonly authService: AuthService,
  ) {
    this.authService.loggedUser$.subscribe(
      (user) => {
        !!user ? this.start() : this.stop();
      }
    );
  }

  get newDebt$() {
    return this.newLoanSubject$.asObservable();
  }

  get newInvest$() {
    return this.newInvestSubject$.asObservable();
  }

  get acceptedInvest$() {
    return this.acceptedInvestSubject$.asObservable();
  }

  get newPayment$() {
    return this.newPaymentSubject$.asObservable();
  }

  get declinedInvestments$() {
    return this.declinedInvestmentsSubject$.asObservable();
  }

  private start() {
    this.socketSubject$ = new WebSocketSubject(`ws://localhost:8081/`);

    this.socketSubject$.multiplex(
      () => ({}),
      () => ({}),
      message => message.type === 'debt'
    ).subscribe((newLoan) => {
      this.newLoanSubject$.next(newLoan);
    });

    this.socketSubject$.multiplex(
      () => ({}),
      () => ({}),
      message => message.type === 'investment'
    ).subscribe((newInvest) => {
      this.newInvestSubject$.next(newInvest);
    });

    this.socketSubject$.multiplex(
      () => ({}),
      () => ({}),
      message => message.type === 'acceptedInvest'
    ).subscribe((acceptedInvest) => {
      this.acceptedInvestSubject$.next(acceptedInvest);
    });

    this.socketSubject$.multiplex(
      () => ({}),
      () => ({}),
      message => message.type === 'paidMoney'
    ).subscribe((payment) => {
      this.newPaymentSubject$.next(payment);
    });

    this.socketSubject$.multiplex(
      () => ({}),
      () => ({}),
      message => message.type === 'declinedInvestments'
    ).subscribe((declinedInvestments) => {
      this.declinedInvestmentsSubject$.next(declinedInvestments.investments);
    });

    this.socketSubject$.next({ token: localStorage.getItem('token') || '' });
  }

  private stop() {
    this.socketSubject$.complete();
  }
}
