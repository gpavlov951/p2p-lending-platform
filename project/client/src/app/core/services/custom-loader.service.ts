import { Subject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

interface ILoaderState {
    show: boolean;
}

@Injectable({
    providedIn: 'root'
})
export class CustomLoaderService {

    private loaderSubject = new Subject<ILoaderState>();
    private loaderState = this.loaderSubject.asObservable();

    constructor() { }

    public getLoaderState(): Observable<ILoaderState> {
        return this.loaderState;
    }

    public show() {
        this.loaderSubject.next({ show: true } as ILoaderState);
    }

    public hide(delay?: number) {
        if (delay) {
            setTimeout(() => this.loaderSubject.next({ show: false } as ILoaderState), delay);
        } else {
            this.loaderSubject.next({ show: false } as ILoaderState);
        }
    }

}
