import { DebtOnUserDTO } from '../../common/models/debt-models/debt-on-user.dto';
import { StatusInvestment } from '../../common/enums/status-investment.enum';
import { Injectable } from '@angular/core';
import moment from 'moment';

@Injectable({
    providedIn: 'root'
})
export class UtilsService {

    calculateLeftAmountToPay = (totalAmount: number, installmentAmount: number, paymentsCount: number) => {
        return totalAmount - (installmentAmount * paymentsCount);
    }

    calculateNextDueDate = (createOnDate: string, paymentsCount: number) => {
        return moment(createOnDate).add(paymentsCount + 1, 'M').format('YYYY-MM-DD');
    }

    calculateOverDueDays = (dueDate: string, currDate: string) => {
        return moment(currDate).diff(moment(dueDate), 'days');
    }

    calculatePenaltyAmount = (days: number, totalAmount: number, penaltyInterest: number) => {

        const penalty = Math.pow(1 + (penaltyInterest / 100 / 360), days);
        const penaltyAmountPerDay = (penalty - 1) * totalAmount;

        return days * penaltyAmountPerDay;
    }

    calculateRemainingAmount = (debt: DebtOnUserDTO): number => {
        return debt.amount - debt.investments
            .filter(investment => investment.status === StatusInvestment.Accepted)
            .reduce((acc, curr) => acc += curr.amount, 0);
    }
}
