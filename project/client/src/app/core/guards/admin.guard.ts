import { UserRole } from './../../common/enums/user-role.enum';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { NotificatorService } from '../services/notificator.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserDTO } from '../../common/models/user-models/user.dto';

@Injectable({
    providedIn: 'root'
})
export class AdminGuard implements CanActivate {
    constructor(
        private readonly authService: AuthService,
        private readonly router: Router,
        private readonly notificator: NotificatorService,
    ) { }

    canActivate(): Observable<boolean> {
        return this.authService.loggedUser$
            .pipe(
                map((user: UserDTO) => {
                    if (user.role !== UserRole.Admin) {
                        this.notificator.error(`You can't go on that page!`);
                        this.router.navigate(['/']);
                        return false;
                    } else {
                        return true;
                    }
                })
            );
    }
}
