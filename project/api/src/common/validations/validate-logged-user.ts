import { User } from '../../database/entities/user.entity';
import { PeerToPeerSystemError } from '../../middleware/exceptions/p2p-system.error';

export const validateLoggedUser = (user: User, userId: string): void => {
    if (user.id !== userId) {
        throw new PeerToPeerSystemError(`You are not logged user!`, 400);
    }
};
