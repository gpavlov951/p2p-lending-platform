import { PeerToPeerSystemError } from '../../middleware/exceptions/p2p-system.error';

export const validateEntity = (entity: any, nameEntity: string): void => {
    if (!entity) {
        throw new PeerToPeerSystemError(`${nameEntity} with this id doesn't exist!`, 400);
    }
};
