export enum StatusDebt {
    Cancel = 'Cancel',
    Pending = 'Pending',
    PartialFunded = 'PartialFunded',
    FullyFunded = 'FullyFunded',
}
