import { UpdateBalanceStatus } from '../enums/update-balance-status';

export const UPDATE_USER = {
    [UpdateBalanceStatus.Withdraw]: 'withdrawMoney',
    [UpdateBalanceStatus.Deposit]: 'depositMoney',
};
