import { Injectable } from '@nestjs/common';
import { UsersService } from '../features/users/users.service';
import { JwtService } from '@nestjs/jwt';
import { User } from '../database/entities/user.entity';
import { UserPayloadDTO } from '../features/users/models/user-payload.dto';
import { LoginUserDTO } from '../features/users/models/login-user.dto';
import { PeerToPeerSystemError } from '../middleware/exceptions/p2p-system.error';

@Injectable()
export class AuthService {
    private readonly blacklist: string[] = [];

    constructor(
        private readonly usersService: UsersService,
        private readonly jwtService: JwtService,
    ) { }

    async singIn(user: LoginUserDTO) {
        const foundUser: User = await this.usersService.findUserByUsername(user.username);

        if (!foundUser) {
            throw new PeerToPeerSystemError('User with such username doest not exist!', 400);
        }
        if (!(await this.usersService.validateUserPassword(user))) {
            throw new PeerToPeerSystemError('Invalid password!', 400);
        }

        if (foundUser.isBanned) {
            throw new PeerToPeerSystemError(`Sorry you can't log in, because you are BANNED!`, 400);
        }

        const payload: UserPayloadDTO = {
            id: foundUser.id,
            username: foundUser.username,
            role: foundUser.role,
            isBanned: foundUser.isBanned,
        };

        return {
            token: await this.jwtService.signAsync(payload)
        };
    }

    blacklistToken(token: string): void {
        this.blacklist.push(token);
    }

    isTokenBlacklisted(token: string): boolean {
        return this.blacklist.includes(token);
    }
}
