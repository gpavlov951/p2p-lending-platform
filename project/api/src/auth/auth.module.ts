import { PassportModule } from '@nestjs/passport';
import { ConfigModule } from './../config/config.module';
import { JwtModule } from '@nestjs/jwt';
import { Module } from '@nestjs/common';
import { JwtStrategy } from './strategy/jwt.strategy';
import { ConfigService } from '../config/config.service';
import { UsersModule } from '../features/users/users.module';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';

@Module({
    imports: [
        PassportModule,
        UsersModule,
        JwtModule.registerAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: async (configService: ConfigService) => ({
                secret: configService.jwtSecret,
                signOptions: {
                    expiresIn: configService.jwtExpireTime,
                },
            }),
        }),
    ],
    controllers: [
        AuthController
    ],
    providers: [
        JwtStrategy,
        AuthService
    ],
    exports: [
        AuthService,
        PassportModule
    ],
})
export class AuthModule { }
