import { Module } from '@nestjs/common';
import { CoreModule } from './core/core.module';
import { DatabaseModule } from './database/database.module';
import { UsersModule } from './features/users/users.module';
import { DebtsModule } from './features/debts/debts.module';
import { InvestmentsModule } from './features/investments/investments.module';
import { LoansModule } from './features/loans/loans.module';
import { PaymentsModule } from './features/payments/payments.module';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { SocketGateway } from './core/socket/socket.gateway';

@Module({
  imports: [
    CoreModule,
    DatabaseModule,
    UsersModule,
    DebtsModule,
    InvestmentsModule,
    LoansModule,
    PaymentsModule,

    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.jwtSecret,
        signOptions: {
          expiresIn: configService.jwtExpireTime,
        },
      }),
    }),

  ],
  providers: [SocketGateway],
})
export class AppModule { }
