import { SocketService } from './socket.service';
import { WebSocketGateway, OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect, WebSocketServer } from '@nestjs/websockets';
import { JwtService } from '@nestjs/jwt';
import { UserPayloadDTO } from '../../features/users/models/user-payload.dto';
import { PeerToPeerSystemError } from '../../middleware/exceptions/p2p-system.error';
import { Server } from 'http';

@WebSocketGateway(8081)
export class SocketGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {

  @WebSocketServer()
  private readonly server: Server;

  constructor(
    private readonly jwtService: JwtService,
    private readonly socketService: SocketService,
  ) { }

  async afterInit() { }

  async handleConnection(client: WebSocket) {
    client.onmessage = (message) => this.validateUser(client, message);
  }

  async handleDisconnect(client: WebSocket) {
    this.socketService.removeUser(client);
  }

  private validateUser(client: WebSocket, message: MessageEvent) {
    try {
      const { token } = JSON.parse(message.data) as any;
      const user: UserPayloadDTO = (this.jwtService.verify(token) && this.jwtService.decode(token) as UserPayloadDTO);

      if (!user) {
        throw new PeerToPeerSystemError(`NO USER FOUND?!`);
      }

      this.socketService.addUser(user.id, client);

      client.onmessage = () => { /* empty */ };
    } catch (error) { }
  }
}
