import { Injectable } from '@nestjs/common';
import { User } from '../../database/entities/user.entity';
import { AllDebtDataDTO } from '../../features/debts/models/all-debt-data.dto';

@Injectable()
export class DebtsSocketService {

    sendDebtToAllInvestors(borrower: User, debt: AllDebtDataDTO, poolUsers: Array<[string, WebSocket]>): void {
        poolUsers.forEach(([userId, client]) => {
            if (userId !== borrower.id) {
                client.send(JSON.stringify({ ...debt, type: 'debt' }));
            }
        });
    }
}
