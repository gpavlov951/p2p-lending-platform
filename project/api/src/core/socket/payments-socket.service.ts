import { Injectable } from '@nestjs/common';
import { User } from '../../database/entities/user.entity';
import { CreatePaymentDTO } from '../../features/payments/models/create-payment.dto';

@Injectable()
export class PaymentsSocketService {

    sendPaymentToInvestor(newPayment: CreatePaymentDTO, loanId: string, investor: User, poolUsers: Array<[string, WebSocket]>) {
        const userAsInvestor = poolUsers.find(([userId, _]) => investor.id === userId);

        if (userAsInvestor) {
            userAsInvestor[1].send(JSON.stringify({ ...newPayment, loanId, type: 'paidMoney' }));
        }
    }
}
