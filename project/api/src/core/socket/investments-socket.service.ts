import { Injectable } from '@nestjs/common';
import { User } from '../../database/entities/user.entity';
import { BaseInvestmentDataOnlyUserDTO } from '../../features/investments/models/base-investment-data-only-user.dto';
import { AllInvestmentDataDTO } from '../../features/investments/models/all-investment-data.dto';

@Injectable()
export class InvestmentsSocketService {

    sendInvestToBorrower(investment: BaseInvestmentDataOnlyUserDTO, debtId: string, borrower: User, poolUsers: Array<[string, WebSocket]>): void {
        const userAsBorrower = poolUsers.find(([userId, _]) => borrower.id === userId);

        if (userAsBorrower) {
            userAsBorrower[1].send(JSON.stringify({ ...investment, type: 'investment', debtId }));
        }
    }

    sendAcceptedInvestToInvestor(invest: AllInvestmentDataDTO, investor: User, poolUsers: Array<[string, WebSocket]>): void {
        const userAsInvestor = poolUsers.find(([userId, _]) => investor.id === userId);

        if (userAsInvestor) {
            userAsInvestor[1].send(JSON.stringify({ ...invest, type: 'acceptedInvest' }));
        }
    }

    declinedInvestments(investments: string[], poolUsers: Array<[string, WebSocket]>): void {
        poolUsers.forEach(([_, s]) => s.send(JSON.stringify({ investments, type: 'declinedInvestments' })));
    }
}
