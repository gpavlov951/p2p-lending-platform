import { Injectable } from '@nestjs/common';
import { User } from '../../database/entities/user.entity';
import { DebtsSocketService } from './debts-socket.service';
import { AllDebtDataDTO } from '../../features/debts/models/all-debt-data.dto';
import { InvestmentsSocketService } from './investments-socket.service';
import { BaseInvestmentDataOnlyUserDTO } from '../../features/investments/models/base-investment-data-only-user.dto';
import { AllInvestmentDataDTO } from '../../features/investments/models/all-investment-data.dto';
import { PaymentsSocketService } from './payments-socket.service';
import { CreatePaymentDTO } from '../../features/payments/models/create-payment.dto';

@Injectable()
export class SocketService {

    private pool: Array<[string, WebSocket]> = [];

    constructor(
        private readonly debtsSocketService: DebtsSocketService,
        private readonly investmentsSocketService: InvestmentsSocketService,
        private readonly paymentsService: PaymentsSocketService,
    ) { }

    async addUser(id: string, socket: WebSocket) {
        this.pool.push([id, socket]);
    }

    async removeUser(socket: WebSocket) {
        this.pool = this.pool.filter(([, s]) => s !== socket);
    }

    sendDebtToAllInvestors(borrower: User, debt: AllDebtDataDTO): void {
        this.debtsSocketService.sendDebtToAllInvestors(borrower, debt, this.pool);
    }

    sendInvestToBorrower(investment: BaseInvestmentDataOnlyUserDTO, debtId: string, borrower: User): void {
        this.investmentsSocketService.sendInvestToBorrower(investment, debtId, borrower, this.pool);
    }

    sendAcceptedInvestToInvestor(invest: AllInvestmentDataDTO, investor: User): void {
        this.investmentsSocketService.sendAcceptedInvestToInvestor(invest, investor, this.pool);
    }

    sendPaymentToInvestor(newPayment: CreatePaymentDTO, loanId: string, investor: User): void {
        this.paymentsService.sendPaymentToInvestor(newPayment, loanId, investor, this.pool);
    }

    declinedInvestments(investments: string[]): void {
        this.investmentsSocketService.declinedInvestments(investments, this.pool);
    }
}
