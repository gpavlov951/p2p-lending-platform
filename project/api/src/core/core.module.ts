import { ConfigModule } from '../config/config.module';
import { Module, Global } from '@nestjs/common';
import { AuthModule } from '../auth/auth.module';
import { DebtsSocketService } from './socket/debts-socket.service';
import { SocketService } from './socket/socket.service';
import { InvestmentsSocketService } from './socket/investments-socket.service';
import { PaymentsSocketService } from './socket/payments-socket.service';

@Global()
@Module({
    imports: [
        ConfigModule,
        AuthModule
    ],
    providers: [
        InvestmentsSocketService,
        DebtsSocketService,
        PaymentsSocketService,

        SocketService,
    ],
    exports: [
        ConfigModule,
        AuthModule,

        SocketService,
    ],
})
export class CoreModule { }
