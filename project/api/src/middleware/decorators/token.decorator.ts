// tslint:disable:next-line: variable-name
import { createParamDecorator } from '@nestjs/common';

export const Token = createParamDecorator((_, req) => req.headers.authorization);
