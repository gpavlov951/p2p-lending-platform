// tslint:disable:next-line: variable-name
import { createParamDecorator } from '@nestjs/common';
import { User } from '../../database/entities/user.entity';

export const LoggedUser = createParamDecorator((_, req): User => req.user);
