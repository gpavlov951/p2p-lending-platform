import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';
import { Response } from 'express';
import { PeerToPeerSystemError } from '../exceptions/p2p-system.error';

@Catch(PeerToPeerSystemError)
export class PeerToPeerSystemErrorFilter implements ExceptionFilter {
    public catch(exception: PeerToPeerSystemError, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();

        response.status(exception.code).json({
            status: exception.code,
            error: exception.message,
        });
    }
}
