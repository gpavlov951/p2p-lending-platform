import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { PeerToPeerSystemErrorFilter } from './middleware/filters/p2p-error.filter';
import { WsAdapter } from '@nestjs/platform-ws';
import helmet from 'helmet';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
    .setTitle('P2P Lending Platform')
    .setDescription('Final Team Project')
    .setVersion('1.0')
    .addBearerAuth('Authorization', 'header')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  app.useGlobalFilters(new PeerToPeerSystemErrorFilter());
  app.use(helmet());
  app.enableCors();

  app.useWebSocketAdapter(new WsAdapter());

  await app.listen(3000);
}

bootstrap();
