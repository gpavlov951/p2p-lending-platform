import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Debt } from './debt.entity';
import { Investment } from './investment.entity';
import { Loan } from './loan.entity';
import { UserRole } from '../../common/enums/user-role.enum';

@Entity('users')
export class User {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('nvarchar', { nullable: false, unique: true, length: 16 })
    username: string;

    @Column('nvarchar', { nullable: false })
    password: string;

    @Column('float', { nullable: false, default: 1000 })
    balance: number;

    @Column('enum', { enum: UserRole, nullable: false, default: UserRole.User })
    role: UserRole;

    @Column('nvarchar', { nullable: false, default: 'https://i.imgur.com/lXmfcoi.png' })
    userProfileImgUrl: string;

    @Column('boolean', { default: false })
    isBanned: boolean;

    @Column('boolean', { default: false })
    isDeleted: boolean;

    @OneToMany(() => Debt, debt => debt.user)
    debts: Promise<Debt[]>;

    @OneToMany(() => Investment, investment => investment.user)
    investments: Promise<Investment[]>;

    @OneToMany(() => Loan, loan => loan.user)
    loans: Promise<Loan[]>;
}
