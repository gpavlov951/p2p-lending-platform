import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, CreateDateColumn, OneToMany } from 'typeorm';
import { User } from './user.entity';
import { Investment } from './investment.entity';
import { Payment } from './payment.entity';
import { Debt } from './debt.entity';
import { StatusLoan } from '../../common/enums/status.loan.enum';

@Entity('loans')
export class Loan {
    @PrimaryGeneratedColumn('increment')
    id: string;

    @CreateDateColumn()
    createdOn: Date;

    @Column('float', { nullable: false })
    totalAmount: number;

    @Column('float', { nullable: false })
    monthlyInstalmet: number;

    @Column('enum', { enum: StatusLoan, nullable: false, default: StatusLoan.InProcess })
    status: StatusLoan;

    @ManyToOne(() => User, user => user.loans)
    user: Promise<User>;

    @ManyToOne(() => Investment, investment => investment.loans)
    investment: Promise<Investment>;

    @OneToMany(() => Payment, payments => payments.loan)
    payments: Promise<Payment[]>;

    @ManyToOne(() => Debt, debt => debt.loans)
    debt: Promise<Debt>;
}
