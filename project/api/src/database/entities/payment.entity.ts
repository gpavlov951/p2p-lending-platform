import { Entity, PrimaryGeneratedColumn, CreateDateColumn, Column, ManyToOne } from 'typeorm';
import { Loan } from './loan.entity';

@Entity('payments')
export class Payment {

    @PrimaryGeneratedColumn('increment')
    id: string;

    @CreateDateColumn()
    createdOn: Date;

    @Column('float', { nullable: false })
    amount: number;

    @Column('int', { nullable: false, default: 0 })
    overDue: number;

    @Column('float', { nullable: false, default: 0 })
    penaltyAmount: number;

    @ManyToOne(() => Loan, loan => loan.payments)
    loan: Promise<Loan>;
}
