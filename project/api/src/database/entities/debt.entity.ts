import { Loan } from './loan.entity';
import { Entity, PrimaryGeneratedColumn, ManyToOne, Column, OneToMany, CreateDateColumn } from 'typeorm';
import { User } from './user.entity';
import { Investment } from './investment.entity';
import { StatusDebt } from '../../common/enums/status-debt.enum';

@Entity('debts')
export class Debt {
    @PrimaryGeneratedColumn('increment')
    id: string;

    @CreateDateColumn()
    createdOn: Date;

    @Column('int', { nullable: false })
    amount: number;

    @Column('int', { nullable: false })
    period: number;

    @Column('enum', { enum: StatusDebt, nullable: false, default: StatusDebt.Pending })
    status: StatusDebt;

    @Column('boolean', { nullable: false, default: false })
    isPartial: boolean;

    @Column('boolean', { nullable: false, default: false })
    isDeleted: boolean;

    @ManyToOne(() => User, user => user.debts)
    user: Promise<User>;

    @OneToMany(() => Investment, investment => investment.debt)
    investments: Promise<Investment[]>;

    @OneToMany(() => Loan, loan => loan.debt)
    loans: Promise<Loan[]>;
}
