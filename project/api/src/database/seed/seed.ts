import { createConnection, Repository } from 'typeorm';
import { User } from '../entities/user.entity';
import { UserRole } from '../../common/enums/user-role.enum';
import bcrypt from 'bcrypt';

const seedAdmin = async (connection: any) => {
    const userRepo: Repository<User> = connection.manager.getRepository(User);

    const newAdmin: User = userRepo.create({
        username: 'Admin',
        password: await bcrypt.hash('Admin', 10),
        role: UserRole.Admin,
    });

    await userRepo.save(newAdmin);
    console.log('Seeded admin successfully!');
};

const mainSeed = async () => {
    console.log('Seed started!');
    const connection = await createConnection();

    await seedAdmin(connection);

    await connection.close();
    console.log('Seed completed!');
};

mainSeed().catch(console.error);
