import { Publish } from '../../../middleware/transformer/decorators/publish';
import { Investment } from '../../../database/entities/investment.entity';
import { BaseInvestmentDataDTO } from '../../investments/models/base-investment-data.dto';
import { StatusLoan } from '../../../common/enums/status.loan.enum';

export class BaseLoanDataOnlyInvestmentsDTO {

    @Publish()
    id: string;

    @Publish()
    createdOn: string;

    @Publish()
    totalAmount: number;

    @Publish()
    monthlyInstalmet: number;

    @Publish()
    status: StatusLoan;

    @Publish(BaseInvestmentDataDTO)
    investment: Investment;
}
