import { Publish } from '../../../middleware/transformer/decorators/publish';
import { PaymentDataDTO } from '../../payments/models/payment-data.dto';
import { Payment } from '../../../database/entities/payment.entity';
import { StatusLoan } from '../../../common/enums/status.loan.enum';

export class BaseLoanDataOnlyPaymentsDTO {
    @Publish()
    id: string;

    @Publish()
    createdOn: string;

    @Publish()
    totalAmount: number;

    @Publish()
    monthlyInstalmet: number;

    @Publish()
    status: StatusLoan;

    @Publish(PaymentDataDTO)
    payments: Payment[];
}
