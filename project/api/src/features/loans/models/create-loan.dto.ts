import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateLoanDTO {

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    userId: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    debtId: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    investmentId: string;
}
