import { BaseInvestmentDataOnlyUserDTO } from '../../investments/models/base-investment-data-only-user.dto';
import { Publish } from '../../../middleware/transformer/decorators/publish';
import { User } from '../../../database/entities/user.entity';
import { Investment } from '../../../database/entities/investment.entity';
import { Payment } from '../../../database/entities/payment.entity';
import { PaymentDataDTO } from '../../payments/models/payment-data.dto';
import { BaseUserDataDTO } from '../../users/models/base-user-data.dto';
import { StatusLoan } from '../../../common/enums/status.loan.enum';

export class AllLoanDataDTO {

    @Publish()
    id: string;

    @Publish()
    createdOn: string;

    @Publish()
    totalAmount: number;

    @Publish()
    monthlyInstalmet: number;

    @Publish()
    status: StatusLoan;

    @Publish(BaseUserDataDTO)
    user: User;

    @Publish(BaseInvestmentDataOnlyUserDTO)
    investment: Investment;

    @Publish(PaymentDataDTO)
    payments: Payment[];
}
