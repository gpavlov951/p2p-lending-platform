import { Publish } from '../../../middleware/transformer/decorators/publish';
import { BaseInvestmentDataOnlyUserDTO } from '../../investments/models/base-investment-data-only-user.dto';
import { Investment } from '../../../database/entities/investment.entity';
import { PaymentDataDTO } from '../../payments/models/payment-data.dto';
import { Payment } from '../../../database/entities/payment.entity';
import { StatusLoan } from '../../../common/enums/status.loan.enum';

export class BaseLoanDataWithoutUserDTO {

    @Publish()
    id: string;

    @Publish()
    createdOn: string;

    @Publish()
    totalAmount: number;

    @Publish()
    monthlyInstalmet: number;

    @Publish()
    status: StatusLoan;

    @Publish(BaseInvestmentDataOnlyUserDTO)
    investment: Investment;

    @Publish(PaymentDataDTO)
    payments: Payment[];
}
