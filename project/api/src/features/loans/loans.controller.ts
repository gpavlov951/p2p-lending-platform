import { LoansService } from './loans.service';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { Controller, Post, UseGuards, UseInterceptors, Param, Get, Body } from '@nestjs/common';
import { AuthGuardWithBlacklisting } from '../../middleware/guards/blacklist.guard';
import { TransformInterceptor } from '../../middleware/transformer/interceptors/transform.interceptor';
import { AllLoanDataDTO } from './models/all-loan-data.dto';
import { LoggedUser } from '../../middleware/decorators/user.decorator';
import { User } from '../../database/entities/user.entity';
import { Loan } from '../../database/entities/loan.entity';
import { validateLoggedUser } from '../../common/validations/validate-logged-user';
import { CreateLoanDTO } from './models/create-loan.dto';
import { BaseLoanDataWithoutUserDTO } from './models/base-loan-data-without-user.dto';

@Controller()
@ApiUseTags('Loans Controller')
export class LoansController {

    constructor(
        private readonly loansService: LoansService,
    ) { }

    @Get('users/:userId/loans')
    @UseGuards(AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(BaseLoanDataWithoutUserDTO))
    @ApiBearerAuth()
    async getAllLoansPerUser(
        @Param('userId') userId: string,
        @LoggedUser() user: User,
    ): Promise<Loan[]> {
        validateLoggedUser(user, userId);
        return await this.loansService.getAllLoansPerUser(user);
    }

    @Post('users/:userId/debts/:debtId/investments/:investmentId/loans')
    @UseGuards(AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(AllLoanDataDTO))
    @ApiBearerAuth()
    async createLoans(
        @Param('userId') userId: string,
        @Param('debtId') debtId: string,
        @Param('investmentId') investmentId: string,
        @Body() createLoan: CreateLoanDTO,
        @LoggedUser() user: User,
    ): Promise<Loan> {
        validateLoggedUser(user, userId);
        return await this.loansService.createLoan(debtId, investmentId, createLoan, user);
    }

}
