import { Module } from '@nestjs/common';
import { LoansService } from './loans.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Investment } from '../../database/entities/investment.entity';
import { Loan } from '../../database/entities/loan.entity';
import { LoansController } from './loans.controller';
import { Debt } from '../../database/entities/debt.entity';
import { User } from '../../database/entities/user.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Investment, Loan, Debt, User]),
  ],
  controllers: [LoansController],
  providers: [LoansService],
})
export class LoansModule { }
