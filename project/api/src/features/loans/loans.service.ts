import { Investment } from './../../database/entities/investment.entity';
import { User } from './../../database/entities/user.entity';
import { Injectable } from '@nestjs/common';
import { Loan } from '../../database/entities/loan.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { StatusInvestment } from '../../common/enums/status-investment.enum';
import { CreateLoanDTO } from './models/create-loan.dto';
import { Debt } from '../../database/entities/debt.entity';
import { StatusDebt } from '../../common/enums/status-debt.enum';
import { StatusLoan } from '../../common/enums/status.loan.enum';
import { SocketService } from '../../core/socket/socket.service';

@Injectable()
export class LoansService {

    constructor(
        @InjectRepository(Loan) private readonly loansRepository: Repository<Loan>,
        @InjectRepository(Investment) private readonly investmentsRepository: Repository<Investment>,
        @InjectRepository(Debt) private readonly debtsRepository: Repository<Debt>,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        private readonly socketService: SocketService,
    ) { }

    async getAllLoansPerUser(user: User): Promise<Loan[]> {
        return await this.loansRepository.find({
            where: { user, status: StatusLoan.InProcess },
            relations: [
                'investment',
                'investment.user',
                'payments',
            ],
            order: { id: 'DESC' },
        });
    }

    async createLoan(debtId: string, investmentId: string, createLoan: CreateLoanDTO, user: User): Promise<Loan> {
        const createdLoan: Loan = this.loansRepository.create();
        const foundInvestment: Investment = await this.investmentsRepository.findOne(
            { where: { id: investmentId }, relations: ['user', 'debt', 'debt.user'] });
        const foundDebt: Debt = await this.debtsRepository.findOne(
            {
                where: { id: debtId },
                relations: ['investments', 'loans'],
            }
        );

        const investmentAmount: number = foundInvestment.amount;
        const interest: number = foundInvestment.interest;
        const period: number = foundInvestment.period;

        const overAllAmount: number = investmentAmount * (1 + (((interest / 100) / 12) * period));

        createdLoan.totalAmount = overAllAmount;
        createdLoan.monthlyInstalmet = overAllAmount / period;
        createdLoan.user = Promise.resolve(user);
        createdLoan.investment = Promise.resolve(foundInvestment);
        createdLoan.debt = Promise.resolve(foundDebt);
        createdLoan.payments = Promise.resolve([]);

        foundInvestment.status = StatusInvestment.Accepted;
        await this.investmentsRepository.save(foundInvestment);

        const foundInvestor: User = await this.usersRepository.findOne((await foundInvestment.user).id);

        await this.usersRepository.save({
            ...foundInvestor,
            balance: foundInvestor.balance - foundInvestment.amount
        });

        await this.usersRepository.save({
            ...user,
            balance: user.balance + foundInvestment.amount
        });

        const debtRemainingAmount: number =
            foundDebt.amount -
            (await foundDebt.investments)
                .filter((investment: Investment) => investment.status === StatusInvestment.Accepted)
                .reduce((acc, invest) => acc += invest.amount, 0) -
            foundInvestment.amount;

        if (foundDebt.amount === investmentAmount) {
            foundDebt.status = StatusDebt.FullyFunded;

            foundDebt.investments = Promise.all((await foundDebt.investments)
                .map(async (investment: Investment) => {

                    if (investment.id !== foundInvestment.id) {
                        investment.status = StatusInvestment.Rejected;
                        await this.investmentsRepository.save(investment);
                    }

                    return investment;
                }));
        } else {
            foundDebt.status = StatusDebt.PartialFunded;

            const foundAllInvestOnDebt: Investment[] = await this.investmentsRepository
                .find({
                    where: { debt: foundDebt }
                });

            foundAllInvestOnDebt
                .filter((investment: Investment) => investment.amount > debtRemainingAmount && investment.status !== StatusInvestment.Accepted)
                .map(async (investment: Investment) => {
                    investment.status = StatusInvestment.Rejected;
                    await this.investmentsRepository.save(investment);
                });

            if (debtRemainingAmount === 0) {
                foundDebt.status = StatusDebt.FullyFunded;
            }
        }

        await this.debtsRepository.save(foundDebt);

        const savedLoan = await this.loansRepository.save(createdLoan);

        const usernameOnBorrower = (await (await foundInvestment.debt).user).username;

        const investToEmmit: any = {
            id: foundInvestment.id,
            amount: foundInvestment.amount,
            period: foundInvestment.period,
            interest: foundInvestment.interest,
            penaltyInterest: foundInvestment.penaltyInterest,
            status: foundInvestment.status,
            user: {
                id: foundInvestor.id,
                username: foundInvestor.username,
                balance: foundInvestor.balance,
            },
            debt: {
                user: {
                    username: usernameOnBorrower
                }
            },
            loans: [{
                id: savedLoan.id,
                createdOn: savedLoan.createdOn,
                totalAmount: savedLoan.totalAmount,
                monthlyInstalmet: savedLoan.monthlyInstalmet,
                status: savedLoan.status,
                payments: [],
            }],
        };

        this.socketService.sendAcceptedInvestToInvestor(investToEmmit, foundInvestor);

        return savedLoan;
    }
}
