import { Investment } from './../../database/entities/investment.entity';
import { Module } from '@nestjs/common';
import { DebtsService } from './debts.service';
import { Debt } from '../../database/entities/debt.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DebtsController } from './debts.controller';
import { InvestmentsModule } from '../investments/investments.module';
import { LoansModule } from '../loans/loans.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Debt, Investment]),
    InvestmentsModule,
    LoansModule,
  ],
  controllers: [DebtsController],
  providers: [DebtsService]
})
export class DebtsModule { }
