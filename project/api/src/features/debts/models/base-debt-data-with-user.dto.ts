import { Publish } from '../../../middleware/transformer/decorators/publish';
import { StatusDebt } from '../../../common/enums/status-debt.enum';
import { BaseUserDataDTO } from '../../users/models/base-user-data.dto';
import { User } from '../../../database/entities/user.entity';

export class BaseDebtDataWithUserDTO {

    @Publish()
    id: string;

    @Publish()
    createdOn: string;

    @Publish()
    amount: number;

    @Publish()
    period: number;

    @Publish()
    status: StatusDebt;

    @Publish()
    isPartial: boolean;

    @Publish(BaseUserDataDTO)
    user: User;
}
