import { ApiModelProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional } from 'class-validator';

export class UpdateDebtDTO {

    @ApiModelProperty()
    @IsOptional()
    @IsNumber()
    amount: number;
}
