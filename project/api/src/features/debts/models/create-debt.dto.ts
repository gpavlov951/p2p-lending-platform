import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsOptional, IsBoolean } from 'class-validator';

export class CreateDebtDTO {

    @ApiModelProperty()
    @IsNotEmpty()
    @IsNumber()
    amount: number;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsNumber()
    period: number;

    @ApiModelProperty()
    @IsOptional()
    @IsBoolean()
    isPartial: boolean;
}
