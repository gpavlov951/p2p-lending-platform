import { Publish } from '../../../middleware/transformer/decorators/publish';
import { StatusDebt } from '../../../common/enums/status-debt.enum';

export class BaseDebtDataDTO {

    @Publish()
    id: string;

    @Publish()
    createdOn: string;

    @Publish()
    amount: number;

    @Publish()
    period: number;

    @Publish()
    status: StatusDebt;

    @Publish()
    isPartial: boolean;
}
