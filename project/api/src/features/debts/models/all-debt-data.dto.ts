import { Publish } from '../../../middleware/transformer/decorators/publish';
import { StatusDebt } from '../../../common/enums/status-debt.enum';
import { BaseInvestmentDataOnlyUserDTO } from '../../investments/models/base-investment-data-only-user.dto';
import { Investment } from '../../../database/entities/investment.entity';
import { BaseLoanDataOnlyInvestmentsDTO } from '../../loans/models/base-loan-data-only-investments.dto';
import { Loan } from '../../../database/entities/loan.entity';
import { BaseUserDataDTO } from '../../users/models/base-user-data.dto';
import { User } from '../../../database/entities/user.entity';

export class AllDebtDataDTO {
    @Publish()
    id: string;

    @Publish()
    createdOn: string;

    @Publish()
    amount: number;

    @Publish()
    period: number;

    @Publish()
    status: StatusDebt;

    @Publish()
    isPartial: boolean;

    @Publish(BaseInvestmentDataOnlyUserDTO)
    investments: Investment[];

    @Publish(BaseLoanDataOnlyInvestmentsDTO)
    loans: Loan[];

    @Publish(BaseUserDataDTO)
    user: User;
}
