import { DebtsService } from './debts.service';
import { Controller, Post, UseGuards, UseInterceptors, Body, ValidationPipe, Put, Param, Delete, Get } from '@nestjs/common';
import { AuthGuardWithBlacklisting } from '../../middleware/guards/blacklist.guard';
import { TransformInterceptor } from '../../middleware/transformer/interceptors/transform.interceptor';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { CreateDebtDTO } from './models/create-debt.dto';
import { LoggedUser } from '../../middleware/decorators/user.decorator';
import { User } from '../../database/entities/user.entity';
import { Debt } from '../../database/entities/debt.entity';
import { UpdateDebtDTO } from './models/update-debt.dto';
import { validateLoggedUser } from '../../common/validations/validate-logged-user';
import { BaseDebtDataDTO } from './models/base-debt-data.dto';
import { DebtDataPerUser } from './models/debt-data-per-user';
import { AllDebtDataDTO } from './models/all-debt-data.dto';

@Controller()
@ApiUseTags('Debts Controller')
export class DebtsController {

    constructor(
        private readonly debtsService: DebtsService,
    ) { }

    @Get('/debts')
    @UseGuards(AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(AllDebtDataDTO))
    @ApiBearerAuth()
    async getAllDebts(): Promise<Debt[]> {
        return await this.debtsService.getAllDebts();
    }

    @Get('users/:userId/debts')
    @UseGuards(AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(DebtDataPerUser))
    @ApiBearerAuth()
    async getAllDebtsPerUser(
        @Param('userId') userId: string,
        @LoggedUser() user: User,
    ): Promise<Debt[]> {
        validateLoggedUser(user, userId);
        return await this.debtsService.getAllDebtsPerUser(user);
    }

    @Post('users/:userId/debts')
    @UseGuards(AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(DebtDataPerUser))
    @ApiBearerAuth()
    async createDebt(
        @Param('userId') userId: string,
        @Body(new ValidationPipe({ whitelist: true, transform: true })) newDebt: CreateDebtDTO,
        @LoggedUser() user: User,
    ): Promise<Debt> {
        validateLoggedUser(user, userId);
        return await this.debtsService.createDebt(newDebt, user);
    }

    @Put('users/:userId/debts/:debtId')
    @UseGuards(AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(BaseDebtDataDTO))
    @ApiBearerAuth()
    async updateDebt(
        @Param('userId') userId: string,
        @Body(new ValidationPipe({ whitelist: true, transform: true })) updatedDebt: UpdateDebtDTO,
        @Param('debtId') debtId: string,
        @LoggedUser() user: User,
    ): Promise<Debt> {
        validateLoggedUser(user, userId);
        return await this.debtsService.updateDebt(updatedDebt, debtId);
    }

    @Delete('users/:userId/debts/:debtId')
    @UseGuards(AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(BaseDebtDataDTO))
    @ApiBearerAuth()
    async deleteDebt(
        @Param('userId') userId: string,
        @Param('debtId') debtId: string,
        @LoggedUser() user: User,
    ): Promise<Debt> {
        validateLoggedUser(user, userId);
        return await this.debtsService.deleteDebt(debtId);
    }
}
