import { StatusDebt } from './../../common/enums/status-debt.enum';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Debt } from '../../database/entities/debt.entity';
import { Repository, In } from 'typeorm';
import { CreateDebtDTO } from './models/create-debt.dto';
import { User } from '../../database/entities/user.entity';
import { UpdateDebtDTO } from './models/update-debt.dto';
import { validateEntity } from '../../common/validations/validate-entity';
import { SocketService } from '../../core/socket/socket.service';
import { StatusInvestment } from '../../common/enums/status-investment.enum';
import { Investment } from '../../database/entities/investment.entity';

@Injectable()
export class DebtsService {

    constructor(
        @InjectRepository(Debt) private readonly debtsRepository: Repository<Debt>,
        @InjectRepository(Investment) private readonly investmentRepository: Repository<Investment>,
        private readonly sockeetService: SocketService,
    ) { }

    async getAllDebts(): Promise<Debt[]> {
        return await this.debtsRepository.find({
            where: { isDeleted: false, status: In([StatusDebt.PartialFunded, StatusDebt.Pending]) },
            relations: ['user', 'investments', 'investments.user', 'loans', 'loans.investment'],
            order: { id: 'DESC' },
        });
    }

    async getAllDebtsPerUser(user: User): Promise<Debt[]> {
        const allDebtsPerUser: Debt[] = await this.debtsRepository.find({
            where: { user, isDeleted: false, status: In([StatusDebt.PartialFunded, StatusDebt.Pending]) },
            relations: ['investments', 'investments.user', 'loans', 'loans.investment'],
            order: { id: 'DESC' },
        });

        return allDebtsPerUser;
    }

    async createDebt(debt: CreateDebtDTO, user: User): Promise<Debt> {
        const createdDebt: Debt = this.debtsRepository.create(debt);

        createdDebt.user = Promise.resolve(user);
        createdDebt.investments = Promise.resolve([]);
        createdDebt.loans = Promise.resolve([]);

        const savedDebt = await this.debtsRepository.save(createdDebt);

        const investments = await savedDebt.investments;
        const loans = await savedDebt.loans;

        const debtToEmmit: any = {
            id: savedDebt.id,
            createdOn: savedDebt.createdOn,
            amount: savedDebt.amount,
            period: savedDebt.period,
            status: savedDebt.status,
            isPartial: savedDebt.isPartial,
            user: {
                username: user.username,
                role: user.role,
                balance: user.balance,
            },
            investments,
            loans,
        };

        this.sockeetService.sendDebtToAllInvestors(user, debtToEmmit);

        return savedDebt;
    }

    async updateDebt(debtForUpdate: UpdateDebtDTO, debtId: string): Promise<Debt> {
        const foundDebt: Debt = await this.debtsRepository.findOne({
            where: { id: debtId },
            relations: ['investments'],
        });
        validateEntity(foundDebt, 'Debt');

        foundDebt.amount = debtForUpdate.amount;

        const investedAmount: number = (await foundDebt.investments)
            .filter((investment: Investment) => investment.status === StatusInvestment.Accepted)
            .reduce((acc, curr) => acc + curr.amount, 0);

        (await foundDebt.investments)
            .map(async (investment: Investment) => investment.amount > foundDebt.amount
                ? await this.investmentRepository.update(investment, { status: StatusInvestment.Rejected })
                : investment);

        if (foundDebt.amount - investedAmount === 0) {
            foundDebt.status = StatusDebt.FullyFunded;
        }

        return await this.debtsRepository.save(foundDebt);
    }

    async deleteDebt(debtId: string): Promise<Debt> {
        const foundDebt: Debt = await this.debtsRepository.findOne(debtId);

        validateEntity(foundDebt, 'Debt');

        (await foundDebt.investments)
            .map(async (investment: Investment) => {
                investment.status = StatusInvestment.Rejected;
                await this.investmentRepository.save(investment);
                return investment;
            });

        return await this.debtsRepository.save({
            ...foundDebt,
            status: StatusDebt.Cancel,
        });
    }

}
