import { Module } from '@nestjs/common';
import { PaymentsController } from './payments.controller';
import { PaymentsService } from './payments.service';
import { Payment } from '../../database/entities/payment.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Loan } from '../../database/entities/loan.entity';
import { User } from '../../database/entities/user.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Payment, Loan, User]),
  ],
  controllers: [PaymentsController],
  providers: [PaymentsService]
})
export class PaymentsModule { }
