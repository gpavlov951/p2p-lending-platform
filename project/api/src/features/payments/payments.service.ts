import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { Payment } from '../../database/entities/payment.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../../database/entities/user.entity';
import { Loan } from '../../database/entities/loan.entity';
import { CreatePaymentDTO } from './models/create-payment.dto';
import { StatusLoan } from '../../common/enums/status.loan.enum';
import { SocketService } from '../../core/socket/socket.service';

@Injectable()
export class PaymentsService {

    constructor(
        @InjectRepository(Payment) private readonly paymentsRepository: Repository<Payment>,
        @InjectRepository(Loan) private readonly loansRepository: Repository<Loan>,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        private readonly socketService: SocketService,
    ) { }

    async createPayment(loanId: string, newPayment: CreatePaymentDTO, user: User): Promise<Loan> {
        const createdPayment: Payment = this.paymentsRepository.create(newPayment);
        const foundLoan: Loan = await this.loansRepository.findOne({
            where: { id: loanId },
            relations: ['investment', 'investment.user'],
        });

        createdPayment.loan = Promise.resolve(foundLoan);
        await this.paymentsRepository.save(createdPayment);

        const foundInvestor: User = await this.usersRepository.findOne(await foundLoan.investment.then(i => i.user).then(u => u.id));

        await this.usersRepository.save({
            ...foundInvestor,
            balance: foundInvestor.balance + createdPayment.amount + createdPayment.penaltyAmount,
        });

        await this.usersRepository.save({
            ...user,
            balance: user.balance - createdPayment.amount - createdPayment.penaltyAmount,
        });

        const loanForReturn: Loan = await this.loansRepository.findOne({
            where: { id: loanId },
            relations: ['user', 'investment', 'investment.user', 'payments'],
        });

        if ((await loanForReturn.investment).period === (await loanForReturn.payments).length) {
            loanForReturn.status = StatusLoan.Completed;
            await this.loansRepository.save(loanForReturn);
        }

        this.socketService.sendPaymentToInvestor(newPayment, loanId, foundInvestor);

        return loanForReturn;
    }
}
