import { Publish } from '../../../middleware/transformer/decorators/publish';

export class PaymentDataDTO {

    @Publish()
    id: string;

    @Publish()
    createdOn: Date;

    @Publish()
    amount: number;

    @Publish()
    overDue: number;

    @Publish()
    penaltyAmount: number;
}
