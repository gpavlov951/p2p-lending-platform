import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional, IsNumber, IsNotEmpty } from 'class-validator';

export class CreatePaymentDTO {

    @ApiModelProperty()
    @IsNotEmpty()
    @IsNumber()
    amount: number;

    @ApiModelProperty()
    @IsOptional()
    @IsNumber()
    overDue: number;

    @ApiModelProperty()
    @IsOptional()
    @IsNumber()
    penaltyAmount: number;
}
