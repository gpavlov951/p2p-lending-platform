import { LoggedUser } from './../../middleware/decorators/user.decorator';
import { Controller, Post, UseGuards, UseInterceptors, Param, Body, Get, ValidationPipe } from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuardWithBlacklisting } from '../../middleware/guards/blacklist.guard';
import { TransformInterceptor } from '../../middleware/transformer/interceptors/transform.interceptor';
import { PaymentsService } from './payments.service';
import { User } from '../../database/entities/user.entity';
import { CreatePaymentDTO } from './models/create-payment.dto';
import { validateLoggedUser } from '../../common/validations/validate-logged-user';
import { Loan } from '../../database/entities/loan.entity';
import { AllLoanDataDTO } from '../loans/models/all-loan-data.dto';

@Controller('users/:userId/loans/:loanId/payments')
@ApiUseTags('Payments Controller')
export class PaymentsController {

    constructor(
        private readonly paymentsService: PaymentsService
    ) { }

    @Post()
    @UseGuards(AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(AllLoanDataDTO))
    @ApiBearerAuth()
    async createPayments(
        @Param('userId') userId: string,
        @Param('loanId') loanId: string,
        @Body(new ValidationPipe({ whitelist: true, transform: true })) newPayment: CreatePaymentDTO,
        @LoggedUser() user: User,
    ): Promise<Loan> {
        validateLoggedUser(user, userId);
        return await this.paymentsService.createPayment(loanId, newPayment, user);
    }

}
