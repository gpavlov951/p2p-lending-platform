import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../../database/entities/user.entity';
import { Repository } from 'typeorm';
import { LoginUserDTO } from './models/login-user.dto';
import { CreateUserDTO } from './models/create-user.dto';
import { PeerToPeerSystemError } from '../../middleware/exceptions/p2p-system.error';
import { validateEntity } from '../../common/validations/validate-entity';
import { Investment } from '../../database/entities/investment.entity';
import { Loan } from '../../database/entities/loan.entity';
import bcrypt from 'bcrypt';

@Injectable()
export class UsersService {

    constructor(
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        @InjectRepository(Investment) private readonly investmentsRepository: Repository<Investment>,
        @InjectRepository(Loan) private readonly loansRepository: Repository<Loan>,
    ) { }

    async createUser(user: CreateUserDTO): Promise<User> {
        const uniqueUsername = await this.usersRepository.findOne({
            username: user.username,
        });

        if (uniqueUsername) {
            throw new PeerToPeerSystemError('User with this username already exist!', 400);
        }

        const newUser: User = this.usersRepository.create(user);
        newUser.password = await bcrypt.hash(user.password, 10);

        return await this.usersRepository.save(newUser);
    }

    async getAllUsers(): Promise<User[]> {
        return await this.usersRepository.find();
    }

    async getLoggedUserInfo(user: User): Promise<User> {
        const foundUser: User = await this.usersRepository.findOne(
            {
                where: { id: user.id },
            });

        validateEntity(foundUser, 'User');

        return foundUser;
    }

    async updateUserProfilePicture(img: string, user: User): Promise<User> {
        const foundUser: User = await this.usersRepository.findOne(user.id);

        validateEntity(foundUser, 'User');

        return await this.usersRepository.save({
            ...foundUser,
            userProfileImgUrl: img,
        });
    }

    async withdrawMoney(amount: number, user: User): Promise<User> {
        const foundUser: User = await this.usersRepository.findOne({
            where: { id: user.id },
        });

        validateEntity(foundUser, 'User');

        foundUser.balance -= amount;

        return await this.usersRepository.save(foundUser);
    }

    async depositMoney(amount: number, user: User): Promise<User> {
        const foundUser: User = await this.usersRepository.findOne({
            where: { id: user.id },
        });

        validateEntity(foundUser, 'User');

        foundUser.balance += amount;

        return await this.usersRepository.save(foundUser);
    }

    async banUser(userId: string): Promise<User> {
        const userEntity: User = await this.usersRepository.findOne({
            id: userId
        });

        if (userEntity === undefined) {
            throw new BadRequestException('No such user exists');
        }
        userEntity.isBanned = true;
        await this.usersRepository.save(userEntity);
        return userEntity;
    }

    public async unbanUser(userId: string): Promise<User> {
        const userEntity: User = await this.usersRepository.findOne({
            id: userId
        });

        if (userEntity === undefined) {
            throw new BadRequestException('No such user exists');
        }
        userEntity.isBanned = false;
        await this.usersRepository.save(userEntity);
        return userEntity;
    }

    async findUserByUsername(username: string): Promise<User> {
        const foundUser: User = await this.usersRepository.findOne({
            username,
            isDeleted: false
        });

        return foundUser;
    }

    async validateUserPassword(user: LoginUserDTO): Promise<boolean> {
        const foundUser: User = await this.usersRepository.findOne({
            username: user.username
        });

        return await bcrypt.compare(user.password, foundUser.password);
    }
}
