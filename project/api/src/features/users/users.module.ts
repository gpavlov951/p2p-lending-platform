import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../../database/entities/user.entity';
import { Investment } from '../../database/entities/investment.entity';
import { Loan } from '../../database/entities/loan.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Investment, Loan]),
  ],
  providers: [UsersService],
  controllers: [UsersController],
  exports: [UsersService],
})
export class UsersModule { }
