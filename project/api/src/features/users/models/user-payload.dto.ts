import { UserRole } from '../../../common/enums/user-role.enum';

export class UserPayloadDTO {

    id: string;

    username: string;

    role: UserRole;

    isBanned: boolean;
}
