import { Publish } from '../../../middleware/transformer/decorators/publish';
import { UserRole } from '../../../common/enums/user-role.enum';

export class AllUserDataDTO {

    @Publish()
    id: string;

    @Publish()
    username: string;

    @Publish()
    role: UserRole;

    @Publish()
    balance: number;
}
