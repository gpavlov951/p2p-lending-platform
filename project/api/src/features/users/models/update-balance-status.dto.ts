import { ApiModelProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsNumber } from 'class-validator';
import { UpdateBalanceStatus } from '../../../common/enums/update-balance-status';

export class UpdateBalanceStatusDTO {

    @IsEnum(UpdateBalanceStatus)
    @ApiModelProperty({ enum: ['Deposit', 'Withdraw'] })
    action: UpdateBalanceStatus;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsNumber()
    amount: number;
}
