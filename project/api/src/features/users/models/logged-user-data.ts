import { Publish } from '../../../middleware/transformer/decorators/publish';
import { BaseDebtDataDTO } from '../../debts/models/base-debt-data.dto';
import { Debt } from '../../../database/entities/debt.entity';
import { BaseInvestmentDataOnlyDebtDTO } from '../../investments/models/base-investment-data-only-debt.dto';
import { Investment } from '../../../database/entities/investment.entity';

export class LoggedUserDataDTO {
    @Publish()
    username: string;

    @Publish(BaseDebtDataDTO)
    debts: Debt[];

    @Publish(BaseInvestmentDataOnlyDebtDTO)
    investments: Investment[];
}
