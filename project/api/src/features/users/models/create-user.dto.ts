import { Length, IsNotEmpty, IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class CreateUserDTO {

    @ApiModelProperty()
    @Length(3, 16)
    @IsNotEmpty()
    @IsString()
    username: string;

    @ApiModelProperty()
    @Length(5, 16)
    @IsNotEmpty()
    @IsString()
    password: string;
}
