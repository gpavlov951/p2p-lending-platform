import { Controller, Post, Body, ValidationPipe, UseGuards, UseInterceptors, Get, Patch, Param, Put } from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { UsersService } from './users.service';
import { CreateUserDTO } from './models/create-user.dto';
import { User } from '../../database/entities/user.entity';
import { AuthGuardWithBlacklisting } from '../../middleware/guards/blacklist.guard';
import { LoggedUser } from '../../middleware/decorators/user.decorator';
import { TransformInterceptor } from '../../middleware/transformer/interceptors/transform.interceptor';
import { UPDATE_USER } from '../../common/constants/balance-status';
import { UpdateBalanceStatusDTO } from './models/update-balance-status.dto';
import { validateLoggedUser } from '../../common/validations/validate-logged-user';
import { BaseUserDataDTO } from './models/base-user-data.dto';

@Controller('users')
@ApiUseTags('Users Controller')
export class UsersController {

    constructor(
        private readonly usersService: UsersService,
    ) { }

    @Post()
    @UseInterceptors(new TransformInterceptor(BaseUserDataDTO))
    async registerUser(
        @Body(new ValidationPipe({ whitelist: true, transform: true })) user: CreateUserDTO
    ): Promise<User> {
        return await this.usersService.createUser(user);
    }

    @Get('/:id/full-data')
    @UseGuards(AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(BaseUserDataDTO))
    @ApiBearerAuth()
    async getLoggedUserInfo(
        @Param('id') userId: string,
        @LoggedUser() user: User
    ): Promise<User> {
        validateLoggedUser(user, userId);
        return await this.usersService.getLoggedUserInfo(user);
    }

    @Put('/:id')
    @UseGuards(AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(BaseUserDataDTO))
    @ApiBearerAuth()
    async updateUserProfilePicture(
        @Param('id') userId: string,
        @Body(new ValidationPipe({ transform: true, whitelist: true })) body: { img: string },
        @LoggedUser() user: User
    ): Promise<User> {
        validateLoggedUser(user, userId);
        return await this.usersService.updateUserProfilePicture(body.img, user);
    }

    @Patch('/:id')
    @UseGuards(AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(BaseUserDataDTO))
    @ApiBearerAuth()
    async balanceStatus(
        @Param('id') userId: string,
        @Body(new ValidationPipe({ transform: true, whitelist: true })) body: UpdateBalanceStatusDTO,
        @LoggedUser() user: User,
    ): Promise<User> {
        validateLoggedUser(user, userId);
        return await (this.usersService as any)[UPDATE_USER[body.action]](body.amount, user);
    }

    @Get()
    @UseGuards(AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(BaseUserDataDTO))
    @ApiBearerAuth()
    async getAllUsers(): Promise<User[]> {
        return await this.usersService.getAllUsers();
    }
}
