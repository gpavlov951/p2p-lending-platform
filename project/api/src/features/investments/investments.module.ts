import { Module } from '@nestjs/common';
import { InvestmentsService } from './investments.service';
import { Investment } from '../../database/entities/investment.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Debt } from '../../database/entities/debt.entity';
import { InvestmentsController } from './investments.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([Investment, Debt]),
  ],
  controllers: [InvestmentsController],
  providers: [InvestmentsService],
})
export class InvestmentsModule { }
