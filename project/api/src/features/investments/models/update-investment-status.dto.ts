import { StatusInvestment } from '../../../common/enums/status-investment.enum';
import { IsEnum } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateInvestmentStatusDTO {

    @IsEnum(StatusInvestment)
    @ApiModelProperty({ enum: ['Pending', 'Accepted', 'Rejected'] })
    status: StatusInvestment;
}
