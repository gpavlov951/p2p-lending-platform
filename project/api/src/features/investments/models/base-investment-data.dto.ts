import { Publish } from '../../../middleware/transformer/decorators/publish';
import { StatusInvestment } from '../../../common/enums/status-investment.enum';

export class BaseInvestmentDataDTO {

    @Publish()
    id: string;

    @Publish()
    amount: number;

    @Publish()
    period: number;

    @Publish()
    interest: number;

    @Publish()
    penaltyInterest: number;

    @Publish()
    status: StatusInvestment;
}
