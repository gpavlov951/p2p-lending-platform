import { Publish } from '../../../middleware/transformer/decorators/publish';
import { StatusInvestment } from '../../../common/enums/status-investment.enum';
import { User } from '../../../database/entities/user.entity';
import { BaseUserDataDTO } from '../../users/models/base-user-data.dto';

export class BaseInvestmentDataOnlyUserDTO {

    @Publish()
    id: string;

    @Publish()
    amount: number;

    @Publish()
    period: number;

    @Publish()
    interest: number;

    @Publish()
    penaltyInterest: number;

    @Publish()
    status: StatusInvestment;

    @Publish(BaseUserDataDTO)
    user: User;
}
