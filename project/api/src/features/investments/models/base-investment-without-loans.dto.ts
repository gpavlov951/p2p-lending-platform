import { Publish } from '../../../middleware/transformer/decorators/publish';
import { StatusInvestment } from '../../../common/enums/status-investment.enum';
import { User } from '../../../database/entities/user.entity';
import { Debt } from '../../../database/entities/debt.entity';
import { BaseUserDataDTO } from '../../users/models/base-user-data.dto';
import { BaseDebtDataWithUserDTO } from '../../debts/models/base-debt-data-with-user.dto';

export class BaseInvestmentWithoutLoansDTO {

    @Publish()
    id: string;

    @Publish()
    amount: number;

    @Publish()
    period: number;

    @Publish()
    interest: number;

    @Publish()
    penaltyInterest: number;

    @Publish()
    status: StatusInvestment;

    @Publish(BaseUserDataDTO)
    user: User;

    @Publish(BaseDebtDataWithUserDTO)
    debt: Debt;
}
