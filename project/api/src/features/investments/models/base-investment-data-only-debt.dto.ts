import { Publish } from '../../../middleware/transformer/decorators/publish';
import { StatusInvestment } from '../../../common/enums/status-investment.enum';
import { Debt } from '../../../database/entities/debt.entity';
import { BaseDebtDataWithUserDTO } from '../../debts/models/base-debt-data-with-user.dto';

export class BaseInvestmentDataOnlyDebtDTO {

    @Publish()
    id: string;

    @Publish()
    amount: number;

    @Publish()
    period: number;

    @Publish()
    interest: number;

    @Publish()
    penaltyInterest: number;

    @Publish()
    status: StatusInvestment;

    @Publish(BaseDebtDataWithUserDTO)
    debt: Debt;
}
