import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';

export class CreateInvestmentDTO {

    @ApiModelProperty()
    @IsNotEmpty()
    @IsNumber()
    amount: number;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsNumber()
    period: number;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsNumber()
    interest: number;

    @ApiModelProperty()
    @IsNotEmpty()
    @IsNumber()
    penaltyInterest: number;
}
