import { Investment } from './../../database/entities/investment.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In } from 'typeorm';
import { CreateInvestmentDTO } from './models/create-investment.dto';
import { User } from '../../database/entities/user.entity';
import { Debt } from '../../database/entities/debt.entity';
import { PeerToPeerSystemError } from '../../middleware/exceptions/p2p-system.error';
import { validateEntity } from '../../common/validations/validate-entity';
import { UpdateInvestmentStatusDTO } from './models/update-investment-status.dto';
import { StatusInvestment } from '../../common/enums/status-investment.enum';
import { SocketService } from '../../core/socket/socket.service';
import { BaseInvestmentDataOnlyUserDTO } from './models/base-investment-data-only-user.dto';

@Injectable()
export class InvestmentsService {

    constructor(
        @InjectRepository(Investment) private readonly investmentRepository: Repository<Investment>,
        @InjectRepository(Debt) private readonly debtRepository: Repository<Debt>,
        private readonly socketService: SocketService,
    ) { }

    async getAllInvestmentsPerUser(user: User): Promise<Investment[]> {
        return await this.investmentRepository.find({
            where: { user, status: In([StatusInvestment.Accepted, StatusInvestment.Pending]) },
            relations: ['debt', 'debt.user', 'loans', 'loans.payments'],
            order: { id: 'DESC' },
        });
    }

    async createInvestment(investment: CreateInvestmentDTO, debtId: string, loggedUser: User): Promise<Investment> {
        const createdInvestment: Investment = this.investmentRepository.create(investment);
        const foundDebt: Debt = await this.debtRepository.findOne({ where: { id: debtId }, relations: ['user'] });

        validateEntity(foundDebt, 'Debt');

        if (investment.amount > foundDebt.amount) {
            throw new PeerToPeerSystemError(`You can't give more money then he want!`, 400);
        }

        createdInvestment.user = Promise.resolve(loggedUser);
        createdInvestment.debt = Promise.resolve(foundDebt);

        const borrower: User = await foundDebt.user;

        const savedInvest = await this.investmentRepository.save(createdInvestment);

        const investToEmmit: BaseInvestmentDataOnlyUserDTO = {
            id: savedInvest.id,
            amount: savedInvest.amount,
            period: savedInvest.period,
            interest: savedInvest.interest,
            penaltyInterest: savedInvest.penaltyInterest,
            status: savedInvest.status,
            user: ({
                id: loggedUser.id,
                username: loggedUser.username,
                role: loggedUser.role,
                balance: loggedUser.balance,
                userProfileImgUrl: loggedUser.userProfileImgUrl,
            } as any),
        };

        this.socketService.sendInvestToBorrower(investToEmmit, foundDebt.id, borrower);

        return savedInvest;
    }

    async updateInvestmentStatus(investmentId: string, newStatus: UpdateInvestmentStatusDTO): Promise<Investment> {
        const foundInvestment: Investment = await this.investmentRepository.findOne({
            where: { id: investmentId },
            relations: ['user', 'debt', 'debt.user', 'loans', 'loans.payments']
        });

        foundInvestment.status = newStatus.status;

        this.socketService.declinedInvestments([foundInvestment.id]);

        return await this.investmentRepository.save(foundInvestment);
    }
}
