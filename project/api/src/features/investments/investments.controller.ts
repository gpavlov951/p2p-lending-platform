import { Controller, Post, UseGuards, UseInterceptors, Param, Body, ValidationPipe, Get, Put } from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuardWithBlacklisting } from '../../middleware/guards/blacklist.guard';
import { TransformInterceptor } from '../../middleware/transformer/interceptors/transform.interceptor';
import { AllInvestmentDataDTO } from './models/all-investment-data.dto';
import { CreateInvestmentDTO } from './models/create-investment.dto';
import { LoggedUser } from '../../middleware/decorators/user.decorator';
import { User } from '../../database/entities/user.entity';
import { Investment } from '../../database/entities/investment.entity';
import { InvestmentsService } from './investments.service';
import { validateLoggedUser } from '../../common/validations/validate-logged-user';
import { BaseInvestmentWithoutLoansDTO } from './models/base-investment-without-loans.dto';
import { UpdateInvestmentStatusDTO } from './models/update-investment-status.dto';

@Controller()
@ApiUseTags('Investments Controller')
export class InvestmentsController {

    constructor(
        private readonly investmentsService: InvestmentsService,
    ) { }

    @Get('users/:userId/investments')
    @UseGuards(AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(AllInvestmentDataDTO))
    @ApiBearerAuth()
    async getAllInvestmentsPerUser(
        @Param('userId') userId: string,
        @LoggedUser() user: User,
    ): Promise<Investment[]> {
        validateLoggedUser(user, userId);
        return this.investmentsService.getAllInvestmentsPerUser(user);
    }

    @Post('users/:userId/debts/:debtId/investments')
    @UseGuards(AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(BaseInvestmentWithoutLoansDTO))
    @ApiBearerAuth()
    async createInvestment(
        @Param('debtId') debtId: string,
        @Body(new ValidationPipe({ whitelist: true, transform: true })) newInvestment: CreateInvestmentDTO,
        @LoggedUser() user: User,
    ): Promise<Investment> {
        return await this.investmentsService.createInvestment(newInvestment, debtId, user);
    }

    @Put('investments/:investmentId')
    @UseGuards(AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(AllInvestmentDataDTO))
    @ApiBearerAuth()
    async  updateInvestmentStatus(
        @Param('investmentId') investmentId: string,
        @Body(new ValidationPipe({ whitelist: true, transform: true })) newStatus: UpdateInvestmentStatusDTO,
    ): Promise<Investment> {
        return this.investmentsService.updateInvestmentStatus(investmentId, newStatus);
    }
}
