-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: goldendb
-- ------------------------------------------------------
-- Server version	5.7.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `debts`
--

DROP TABLE IF EXISTS `debts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `debts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `amount` int(11) NOT NULL,
  `period` int(11) NOT NULL,
  `status` enum('Cancel','Pending','PartialFunded','FullyFunded') NOT NULL DEFAULT 'Pending',
  `isPartial` tinyint(4) NOT NULL DEFAULT '0',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `userId` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_834960a509c776eb841644a9bac` (`userId`),
  CONSTRAINT `FK_834960a509c776eb841644a9bac` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `debts`
--

LOCK TABLES `debts` WRITE;
/*!40000 ALTER TABLE `debts` DISABLE KEYS */;
INSERT INTO `debts` VALUES (1,'2019-12-17 15:18:30.424826',300,3,'FullyFunded',0,0,'f09b371f-c25e-4e7a-b658-f12e461f83dc'),(2,'2019-12-17 15:19:20.524119',500,5,'PartialFunded',1,0,'f09b371f-c25e-4e7a-b658-f12e461f83dc'),(3,'2019-12-17 15:21:06.474006',1500,10,'Pending',1,0,'f09b371f-c25e-4e7a-b658-f12e461f83dc');
/*!40000 ALTER TABLE `debts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `investments`
--

DROP TABLE IF EXISTS `investments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `investments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` int(11) NOT NULL,
  `period` int(11) NOT NULL,
  `interest` float NOT NULL,
  `penaltyInterest` float NOT NULL DEFAULT '0',
  `status` enum('Pending','Accepted','Rejected') NOT NULL DEFAULT 'Pending',
  `userId` varchar(36) DEFAULT NULL,
  `debtId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_1ee4fc01d07959ee6cf7926fe3c` (`userId`),
  KEY `FK_43dad118c601f791573d2ae4d05` (`debtId`),
  CONSTRAINT `FK_1ee4fc01d07959ee6cf7926fe3c` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_43dad118c601f791573d2ae4d05` FOREIGN KEY (`debtId`) REFERENCES `debts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `investments`
--

LOCK TABLES `investments` WRITE;
/*!40000 ALTER TABLE `investments` DISABLE KEYS */;
INSERT INTO `investments` VALUES (1,300,3,3,3,'Accepted','fbb2ca8d-6597-4e63-9519-7ca06127b89e',1),(2,200,2,2,2,'Accepted','fbb2ca8d-6597-4e63-9519-7ca06127b89e',2),(3,350,3,3,3,'Rejected','fbb2ca8d-6597-4e63-9519-7ca06127b89e',2),(4,100,3,3,3,'Pending','fbb2ca8d-6597-4e63-9519-7ca06127b89e',2);
/*!40000 ALTER TABLE `investments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loans`
--

DROP TABLE IF EXISTS `loans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `totalAmount` float NOT NULL,
  `monthlyInstalmet` float NOT NULL,
  `status` enum('InProcess','Completed') NOT NULL DEFAULT 'InProcess',
  `userId` varchar(36) DEFAULT NULL,
  `investmentId` int(11) DEFAULT NULL,
  `debtId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_4c2ab4e556520045a2285916d45` (`userId`),
  KEY `FK_520cdb0f5d5fc36870ecfa253e7` (`investmentId`),
  KEY `FK_90d92d94ab31b887216d7fe335c` (`debtId`),
  CONSTRAINT `FK_4c2ab4e556520045a2285916d45` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_520cdb0f5d5fc36870ecfa253e7` FOREIGN KEY (`investmentId`) REFERENCES `investments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_90d92d94ab31b887216d7fe335c` FOREIGN KEY (`debtId`) REFERENCES `debts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loans`
--

LOCK TABLES `loans` WRITE;
/*!40000 ALTER TABLE `loans` DISABLE KEYS */;
INSERT INTO `loans` VALUES (1,'2019-12-17 15:18:50.014245',302.25,100.75,'InProcess','f09b371f-c25e-4e7a-b658-f12e461f83dc',1,1),(2,'2019-12-17 15:20:14.340769',200.667,100.333,'InProcess','f09b371f-c25e-4e7a-b658-f12e461f83dc',2,2);
/*!40000 ALTER TABLE `loans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `amount` float NOT NULL,
  `overDue` int(11) NOT NULL DEFAULT '0',
  `penaltyAmount` float NOT NULL DEFAULT '0',
  `loanId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_138820eff6d03b08b4a6614a5e6` (`loanId`),
  CONSTRAINT `FK_138820eff6d03b08b4a6614a5e6` FOREIGN KEY (`loanId`) REFERENCES `loans` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` varchar(36) NOT NULL,
  `username` varchar(16) NOT NULL,
  `password` varchar(255) NOT NULL,
  `balance` float NOT NULL DEFAULT '1000',
  `role` enum('User','Admin') NOT NULL DEFAULT 'User',
  `userProfileImgUrl` varchar(255) NOT NULL DEFAULT 'https://i.imgur.com/lXmfcoi.png',
  `isBanned` tinyint(4) NOT NULL DEFAULT '0',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_fe0bb3f6520ee0469504521e71` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('f09b371f-c25e-4e7a-b658-f12e461f83dc','borrower123','$2b$10$Qfut/BEGkHMJ2r.AnMfv4ut9tiPlmGqoM7bPUUYubeFIEtqEICip6',1500,'User','https://i.imgur.com/lXmfcoi.png',0,0),('fbb2ca8d-6597-4e63-9519-7ca06127b89e','investor123','$2b$10$L9KsAvk.PAyKhgRD.QereegP2e5W3E0nitwyc.4FvqSJK7Kfe/Cd.',500,'User','https://i.imgur.com/lXmfcoi.png',0,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-17 15:22:25
