<img src="https://i.imgur.com/nxJNTcQ.png" alt="logo" width="500px" style="margin: 20px auto;"/>

# P2P Lending Platform - Telerik Final Project Assignment

## Description

Welcome to our **P2P Lending Platform**. The application connects people who need money with people willing to lend money. One user can be borrower and investor at the same time. He can request a loan with amount(bigger then 100\$), period and user can choice if he want partial investments or not.
User as borrower can see his loan requests(still pending) and his debts. On every loan request user is able to see all loan's investments. He is able to make payments on debt(s). 
User as investor can see all loan requests(still pending) and his investments. He can make a investment request with amount, period, annual interest rate, penalty annual interest rate. He is able to see on every investment when is next due date to get money.

## Project Instaling

1. Go to project/api folder then in terminal and run:

   - `npm install`

2. Setup MySQL Database with new Schema.

3. Setup `.env` and `оrmconfig.json` files. They need to be on root level in api folder where is `package.json` and other config files.

   - `.env` file with your settings:

   ```typescript
   PORT=3000
   DB_TYPE=mysql
   DB_HOST=localhost
   DB_PORT=3306
   DB_USERNAME=root
   DB_PASSWORD=root
   DB_DATABASE_NAME=p2pdb
   JWT_SECRET=awesomekey
   ```

   - `ormconfig.json` file with your settings:

   ```typescript
    {
    "type": "mysql",
    "host": "localhost",
    "port": 3306,
    "username": "root",
    "password": "root",
    "database": "p2pdb",
    "entities": [
        "src/database/entities/**/*.ts"
    ],
    "migrations": [
        "src/database/migration/**/*.ts"
    ],
    "cli": {
        "entitiesDir": "src/database/entities",
        "migrationsDir": "src/database/migration"
    }
   }
   ```

4. After files are setup go in terminal and run:

   - `npm run start:dev` or `npm run start`

**Now you will have working Api server with empty database. If you want to load information in database you should load in MySQL Workbench `db.sql` file. You can find this file at root level in api folder. Our Api have setup for swagger and you can use it [here](http://localhost:3000/api).**

5. Go to client folder then in terminal and run:

   - `npm install`

6. Now you can start angular server from terminal with:

   - `ng serve --o`

You should see a message on the console **"Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/. Compiled successfully"** and browser should be open.

## **You can enter [here](https://p2p-ld-client.herokuapp.com/) and enjoy our app.**

<img src="https://media.giphy.com/media/XAyOf7EpPHf1D9m9AZ/giphy.gif" alt="coin" width="150px" style="margin-left: 175px;"/>

## Tests

1. You can run tests from client folder with:

   - `npm run test`

**Project have only front-end(Angular) tests on a few components and many services. All tests files are in project/client/src/tests folder.**
